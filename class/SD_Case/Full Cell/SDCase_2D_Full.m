classdef (Abstract) SDCase_2D_Full
    %SDCase_2D_Full Abstract class wich describes a computational case for 
    %the Spectral Difference (SD) method.
    % Uses a SDCell which inherits from SDCell_2D_Full.
    %Abstract methods and properties are equation or dimension dependant
    %(e.g. Euler 1D, Burgers 2D, etc...).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract methods/properties
% (must be implemented by each subclass.)


    properties (Abstract, SetAccess = protected)
        d; % (1x1) Dimension (1D,2D,...)
        Nq; % (1x1) Number of conservative variables
        EqnName; % (string) Name of the equation(s) solved.
            
            % 'Normal' indexes.
            % Usage: normal projection (Riemann Solver).
            % Example: Euler 2D.
            % state vector is Q = [rho; rho*u; rho*v; rho*E]
            %       x_idx is 1,2,4 (Q_nx = [rho; rho*u; rho*E])
            %       y_idx is 1,3,4 (Q_ny = [rho; rho*v; rho*E])
            
        
        x_idx; % (1x?) indexes of variables which belong to the 'x' state.
        y_idx; % (1x?) indexes of variables which belong to the 'y' state.
    end
    
    methods (Abstract, Access=public)

    end

    
    methods (Abstract, Access=protected)
        % From Qxs, compute the maximum physical speed in every direction.
        % Usage: deduce time step from the CFL number
        % Input:
        %   Conservative variable
        % Output:
        %   Absolute Maximumn speed(s) (dx1) (d: dimension. 1D, 2D, ...)
        % (1D: U_max = [ux_max], 2D: U_max=[ux_max; uy_max]
        U_max = GetMaxSpeed(obj);

        % Flux Compute the x-flux and y-flux vectors : Fx(Q) & Fy(Q)
        % Input:
        %   Q (NxNq) State vector (conservative variables)
        % Output:
        %   Fx (NxNq) x-flux vector Fx(Q)
        %   Fy (NxNq) y-flux vector Fy(Q)
        [Fx, Fy] = Flux(obj, Q)

        % RiemannSolver1D Compute the Riemann flux F(Q*). Q* may be the
        % exact or an approximate solution to the Riemann problem.
        % Input:
        %   Ql (NxP) left state vector.
        %   Qr (NxP) right state vector.
        %   n (2x1) [nx;ny] normal vector (needed in some cases)
        % Output:
        %   F (NxP) flux vector.
        % Storage format: storage by line: [Q1; Q2; Q3; ..; QN]
        F = RiemannSolver1D(obj,Ql,Qr,n)
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (SetAccess = protected)
        
        CFL; % (1x1) CFL number
        dt; % (1x1) time-step (deduced from <CFL>)
        
        CaseName; % (string) Name of the computational case.
        
        Mesh; % (Mesh_2D_Rect_Uniform) Computational mesh.
        SDCell; % (SDCell_2D_Full) SD cell.
           
            % (1x(niter+1)) time corresponding to each iteration
            % 1st value : 0s (iteration n°0)
            % niter : number of iterations performed (>=0)
        t;
            % (Ncell*Ns x Nq x P) P>= (niter+1) due to pre-allocation
            % Polynomial coefficients, conservative variables 
            % @ solution points
            % Rmq: 1D -> Ncell = Nx
            %      2D -> Ncell = Nx*Ny
            % Rmq: All computed values are stored here.
            % Storage convention:
            %   - variables (1 to Nq) on each column
            %   - values over cell follows the same convention as 
            %<SDCell.xs_c> (this is transparent).
            %   - access through 'index functions'.
        CQ;
            % (Ncell*Ns x 2) global coordinates of the solution points
            % Format : [xs,ys]
        xs_c;
            % (Ny*Nsd(2) x Nx*Nsd(1) x 2)
            % Solution points display grid
            % Global coordinates, grid format.
            % xsd_g(:,:,1) = x, xsd_g(:,:,2)=y
            % (top-left) -> (bottom-right) storage convention
        xsd_g;
                
        % Face index table (for Riemann)
        % For each cell face, indexes to access the left and right 
        % flux points.
        % The format of each of these tables is [iL, iR] (left and right).
        % Usage : Qxf(iL) or Qxf(iR).
        
        BoundT; % (?x2) Top domain boundary
        BoundR; % (?x2) Right domain boundary
        IntV; % (?x2) Vertical interior faces (excl. top domain boundary)
        IntH; % Horizontal interior faces (excl. right domain boundary)
        
                %-- Global matrices
            % Polynomial operations
            % (usage example: Q(xs) = Ls(xs)*CQ)
            
        Ls_xs; % (Ncell*Ns x Ncell*Ns) Ls(xs)
        Ls_xsd; % (Ncell*prod(Nsd) x Ncell*Ns) Ls(xsd)
        iLs_xs; % inverse of <Ls_xs>
        Ls_xf; % (Ncell*Nf x Ncell*Ns) Ls(xf)
        
        Lf_xs ; % (Ncell*Ns x Ncell*Nf) Lf(xs)
        Lf_xf; % (Ncell*Nf x Ncell*Nf) Lf(xf)
        iLf_xf; % inverse of <Lf_xf>
        Dx1Lf_xs; % (Ncell*Ns x Ncell*Nf) Dx1Lf(xs)
        Dx2Lf_xs; % (Ncell*Ns x Ncell*Nf) Dx2Lf(xs)
        
        
            % -- Temp variables
            % Hold last computed values
    
        Qxs; % (Ncell*Ns x Nq x 1) Cons. var. @ solution points
        Qxf; % (Ncell*Nf x Nq x1) Cons. var @ flux points (on one cell)

        FXxf; % (Ncell*Nf x Nq x1) x-flux @ flux points
        FYxf; % (Ncell*Nf x Nq x1) y-flux @ flux points
        
        CFX % (Ncell*Nf x Nq x1) x-flux polynomial coefficients
        CFY; % (Ncell*Nf x Nq x1) y-flux polynomial coefficients
        divF; % (Ncell*Ns x Nq x 1)  div(F(Q)) @ solution points
         
    end
    
    methods (Access = protected)
        
        function Case = SDCase_2D_Full(q0,CFL,Mesh,SDCell,CaseName)
        %SDCas_2D_Full Class constructor.
        %   q0 (Nxd (space) -> NxNq (cons. variables)) function, initial
        %fields for the conservative variables.
        % ex: (N=2, d=3, Nq=1) 
        % q0([x1,y1,z1;x2,y2,z2]) = [4;5]
        %   CFL (1x1) CFL number to use for the computation.   
        %   Mesh (Mesh_2D_RectUniform) Computational mesh. TODO
        %   SDCell (SDCell_2D_Full) SD cell.
        %   CaseName (string) Name of the computational case, for
        %convenience purposes.
        % ex: CaseName='Stability_Nx_10'.
        
        % TODO: validate abstract methods.
        
                % Arguments check
            validateattributes(CFL,{'numeric'},{'scalar','>',0});
            validateattributes(CaseName,{'char'},{'nonempty'});
            validateattributes(Mesh,{'Mesh_2D_RectUniform'},{});
            validateattributes(SDCell,{'SDCell_2D_Full'},{});        
                % Validate q0.
            N = 10;
            if Case.d==2 % 2D Case
                x=linspace(0,1,N)';
                validateattributes(q0([x,x]),{'numeric'},{'2d','size',[N,Case.Nq]});
            end
            clear N x;

            Case.CFL = CFL;
            Case.Mesh = Mesh;
            Case.SDCell = SDCell;
            Case.CaseName = CaseName;

                % Build faces index table (for Riemann)
            Case = Case.buildFacesIndexTable();
                % Build global polynomial interpolation matrices
            Case = Case.buildGlobalInterpolationMatrix();
                % Compute global coordinates of sol. points
            Case.xs_c = Case.GetGlobalCoords_Sol();
                % Initializes Qxs and CQ with q0
            Case = Case.Initialize(q0);
                % Generate the display grid
            Case.xsd_g = Case.GetInterpolationGrid_s();
        end
        
        function obj = buildFacesIndexTable(obj)
        %buildFacesIndexTable Build tables which give, for each cell face,
        %indexes to access the left and right flux points.
        % The format of each of these tables is [iL, iR] (left and right).
        % Output :
        %   BoundT (?x2) Top domain boundary
        %   BoundR (?x2) Right domain boundary
        %   IntV (?x2) Vertical interior faces (excluding the top domain
        %   boundary faces)
        %   IntH (?x2) Horizontal interior faces (excluding the right 
        % domain boundary faces)
        % Usage : Qxf(iL) or Qxf(iR).
                % Temp. variables
            Nx=obj.Mesh.N(1);
            Ny=obj.Mesh.N(2);
            % Top boundary (Left = Bottom, Right = Top)
            obj.BoundT = zeros(0);
            for i=1:size(obj.Mesh.Face_top_bound,1)
                    % Get top and bottom cells (k,l) coordinates
                kb = obj.Mesh.Face_top_bound(i,1);
                lb = obj.Mesh.Face_top_bound(i,2);
                kt=kb; lt=Ny;
                obj.BoundT = vertcat(obj.BoundT,[obj.mf_t(kb,lb), obj.mf_b(kt,lt)]);
            end
            % Right boundary (Left = Left, Right = Right)
            obj.BoundR = zeros(0);
            for i=1:size(obj.Mesh.Face_right_bound,1)
                kl=obj.Mesh.Face_right_bound(i,1);
                ll=obj.Mesh.Face_right_bound(i,2);
                kr=1; lr=ll;
                obj.BoundR = vertcat(obj.BoundR,[obj.mf_r(kl,ll),obj.mf_l(kr,lr)]);
            end
            % Interior vertical faces (Left = Left, Right = Right)
            obj.IntV=zeros(0);
            for i=1:size(obj.Mesh.Face_vert_int,1) % loop over every vertical interior face
                kl=obj.Mesh.Face_vert_int(i,1);
                ll=obj.Mesh.Face_vert_int(i,2);
                kr=obj.Mesh.Face_vert_int(i,3);
                lr=obj.Mesh.Face_vert_int(i,4);
                obj.IntV=vertcat(obj.IntV,[obj.mf_r(kl,ll),obj.mf_l(kr,lr)]);
            end
            % Interior horizontal faces (Left = Left, Right = Right)
            obj.IntH=zeros(0);
            for i=1:size(obj.Mesh.Face_hor_int,1) % loop over every horizontal interior face
                kb=obj.Mesh.Face_hor_int(i,1);
                lb=obj.Mesh.Face_hor_int(i,2);
                kt=obj.Mesh.Face_hor_int(i,3);
                lt=obj.Mesh.Face_hor_int(i,4);
                obj.IntH=vertcat(obj.IntH,[obj.mf_t(kb,lb),obj.mf_b(kt,lt)]);
            end
        end
        
        function divF = ComputeDivF(obj,CQ)
        %ComputeDivF Computes an approximation of div(F(Q)), using a 
        %Spectral Difference method.
        % Input:
        %   CQ (Ncell*NsxNq) polynomial coefficients of conservative
        % variables
        % Output:
        %   divF (Ncell*NsxNq) div(F(Q)) @ solution points.
        % Temp. variable used:
        %   Qxf (Ncell*Nf x Nq x1)
        %   FXxf (//)
        %   FYxf (//)
        %   CFX (//)
        %   CFY (//)
        
                % Temp. variables
            Nx=obj.Mesh.N(1);
            Ny=obj.Mesh.N(2);
            dx=obj.Mesh.cell_size(1);
            dy=obj.Mesh.cell_size(2);
            Nf=obj.SDCell.Nf;
            Ns=obj.SDCell.Ns;
            Nq = obj.Nq;

                % Interpolation: Cons. var. @ sol. points -> @ flux points
            obj.Qxf = obj.Ls_xf*CQ;
                % x & y-flux @ flux points
            [obj.FXxf,obj.FYxf] = obj.Flux(obj.Qxf);

            if nargin>2 % <it> given
                obj.CQ(:,:,it) = CQ;
            end

            QnL = zeros(0); % left normal state (used for clarity)
            QnR = zeros(0); % right normal state (used for clarity)
            nx = [1;0]; % normal vector, vertical face
            ny = [0;1]; % normal vector, horizontal face
                %-- Boundary Conditions
                %(here, spatial periodicity is enforced)

                % Top boundary: Riemann with bottom boundary
                % Get left and right normal state Qn
            QnL = obj.Qxf(obj.BoundT(:,1),obj.y_idx);
            QnR = obj.Qxf(obj.BoundT(:,2),obj.y_idx);
            obj.FYxf(obj.BoundT(:,1),obj.y_idx)=obj.RiemannSolver1D(QnL,QnR,ny);
            obj.FYxf(obj.BoundT(:,2),obj.y_idx)=obj.FYxf(obj.BoundT(:,1),obj.y_idx);
                % Right boundary: Riemann with left boundary
            QnL = obj.Qxf(obj.BoundR(:,1),obj.x_idx);
            QnR = obj.Qxf(obj.BoundR(:,2),obj.x_idx);
            obj.FXxf(obj.BoundR(:,1),obj.x_idx) = obj.RiemannSolver1D(QnL,QnR,nx);
            obj.FXxf(obj.BoundR(:,2),obj.x_idx) = obj.FXxf(obj.BoundR(:,1),obj.x_idx);
                %-- Riemann Solver on interior faces
                % Vertical faces
            QnL = obj.Qxf(obj.IntV(:,1),obj.x_idx);
            QnR = obj.Qxf(obj.IntV(:,2),obj.x_idx);
            obj.FXxf(obj.IntV(:,1),obj.x_idx) = obj.RiemannSolver1D(QnL,QnR,nx);
            obj.FXxf(obj.IntV(:,2),obj.x_idx) = obj.FXxf(obj.IntV(:,1),obj.x_idx);
                % Horizontal faces
            QnL = obj.Qxf(obj.IntH(:,1),obj.y_idx);
            QnR = obj.Qxf(obj.IntH(:,2),obj.y_idx);
            obj.FYxf(obj.IntH(:,1),obj.y_idx) = obj.RiemannSolver1D(QnL,QnR,ny);
            obj.FYxf(obj.IntH(:,2),obj.y_idx) = obj.FYxf(obj.IntH(:,1),obj.y_idx);
            clear QnR QnL
                %-- div(F(Q)) @ sol. points from fluxes @ flux points
                % flux @ flux points -> polynomial coefficients
            obj.CFX = obj.iLf_xf*obj.FXxf;
            obj.CFY = obj.iLf_xf*obj.FYxf;
                % flux derivative @ solution points -> div(F)
                % ! local != global coordinates (hence metric terms)
            divF= (2/dx)*(obj.Dx1Lf_xs)*obj.CFX+...
                      (2/dy)*(obj.Dx2Lf_xs)*obj.CFY;
        
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Index functions.
    % They are necessary to browse the data structures in column
    %format.
    % There are 4 different storage conventions:
    %   1 - quantity @ solution points (Ns points per cell).
    %       -> functions: ms_1st, ms
    %   2 - quantity @ flux points (Nf points per cell)
    %       -> functions: mf_1st, mf
    %   3 - quantity @ display solution points (prod(Nsd) points
    %   per cell)
    %       -> functions: msi_1st, msi
    %   4 - quantity @ display flux solution points (prod(Nfd)
    %   points per cell)
    %       -> functions: mfi_1st, mfi
    % Usage example: Q(ms_1st(k,l),:).
    % (k,l) range from (1,1) to (Nx,Ny).
          
        function mcidx = mc(k,l)
        %mc Index number of cell (k,l).
            mcidx = 1+(l-1)*Nx*1+(k-1)*1;
        end
                % -- First index functions
        function msidx = ms_1st(obj,k,l)
        %ms_1st ms_1st(k,l) is the FIRST data index wich belongs to the 
        %cell (k,l).
            Nx = obj.Mesh.N(1);
            N = obj.SDCell.Ns;
            msidx = 1+(l-1)*Nx*N+(k-1)*N;
        end
        
        function msidx = mf_1st(obj,k,l)
        %mf_1st mf_1st(k,l) is the FIRST data index wich belongs to the 
        %cell (k,l).
            Nx = obj.Mesh.N(1);
            N = obj.SDCell.Nf;
            msidx = 1+(l-1)*Nx*N+(k-1)*N;
        end
        
        function msidx = msi_1st(obj,k,l)
        %msi_1st msi_1st(k,l) is the FIRST data index wich belongs to the 
        %cell (k,l).
            Nx = obj.Mesh.N(1);
            N = prod(obj.SDCell.Nsd);
            msidx = 1+(l-1)*Nx*N+(k-1)*N;
        end
        
        function msidx = mfi_1st(obj,k,l)
        %mfi_1st mfi_1st(k,l) is the FIRST data index wich belongs to the 
        %cell (k,l).
            Nx = obj.Mesh.N(1);
            N = prod(obj.SDCell.Nfd);
            msidx = 1+(l-1)*Nx*N+(k-1)*N;
        end
                % -- Index range functions
        function msidx = ms(obj,k,l)
        %ms Indexes of all the points at the cell(k,l).
            N = obj.SDCell.Ns;
            msidx = obj.ms_1st(k,l) + (0:(N-1));
        end
        
        function mfidx = mf(obj,k,l)
        %mf Indexes of all the points at the cell(k,l).
            N = obj.SDCell.Nf;
            mfidx = obj.mf_1st(k,l)+(0:(N-1));
        end
        
        function msidx = msi(obj,k,l)
        %msi Indexes of all the points at the cell(k,l).
            N = prod(obj.SDCell.Nsd);
            msidx = obj.ms_1st(k,l) + (0:(N-1));
        end
        
        function mfidx = mfi(obj,k,l)
        %mfi Indexes of all the points at the cell(k,l).
            N = prod(obj.SDCell.Nfd);
            mfidx = obj.mf_1st(k,l)+(0:(N-1));
        end
        
                % -- Index range functions to segregate flux points
        function mfidx = mf_t(obj,k,l)
        %mf_t Top flux points indexes at the cell (k,l).
            mfidx = obj.mf_1st(k,l) + (obj.SDCell.idx_xf_t-1);
        end
        
        function mfidx = mf_b(obj,k,l)
        %mf_b Bottom flux points indexes at the cell (k,l).
            mfidx = obj.mf_1st(k,l) + (obj.SDCell.idx_xf_b-1);
        end
        
        function mfidx = mf_l(obj,k,l)
        %mf_l Left flux points indexes at the cell (k,l).
            mfidx = obj.mf_1st(k,l) + (obj.SDCell.idx_xf_l-1);
        end
        
        function mfidx = mf_r(obj,k,l)
        %mf_r Right flux points indexes at the cell (k,l).
            mfidx = obj.mf_1st(k,l) + (obj.SDCell.idx_xf_r-1);
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Polynomial Interpolation method(s).
% (Creation of polynomial interpolation matrices.)
        function obj=buildGlobalInterpolationMatrix(obj)
        %buildGlobalInterpolationMatrix Build (using the matrices in the
        %SDCell) all the global polynomial interpolation matrices.
        % All the matrices are created as sparse ones.
        % (usage example: Q(xs) = Ls_xs*CQ )

                % Necessary constants
            Ncell = prod(obj.Mesh.N);
            Ns = obj.SDCell.Ns;
            Nf = obj.SDCell.Nf;
            Nsd = prod(obj.SDCell.Nsd);
                % Elementary matrices (defined at cell-level)
            iLs_xs = inv(obj.SDCell.Ls_xs);
            iLf_xf = inv(obj.SDCell.Lf_xf);

                % Initialization as sparse
            obj.Ls_xs = sparse(1,1,1);
            obj.Ls_xsd = sparse(1,1,1);
            obj.iLs_xs = sparse(1,1,1);
            obj.Ls_xf = sparse(1,1,1);
            obj.Lf_xs = sparse(1,1,1);
            obj.Lf_xf = sparse(1,1,1);
            obj.iLf_xf = sparse(1,1,1);
            obj.Dx1Lf_xs = sparse(1,1,1);
            obj.Dx2Lf_xs = sparse(1,1,1);

            I_Ncell = speye(Ncell); 
            obj.Ls_xs = kron(I_Ncell,sparse(obj.SDCell.Ls_xs));
            obj.Ls_xsd = kron(I_Ncell,sparse(obj.SDCell.Ls_xsd));
            obj.iLs_xs = kron(I_Ncell,sparse(iLs_xs));
            obj.Ls_xf = kron(I_Ncell,sparse(obj.SDCell.Ls_xf));
            obj.Lf_xf = kron(I_Ncell,sparse(obj.SDCell.Lf_xf));
            obj.iLf_xf = kron(I_Ncell,sparse(iLf_xf));
            obj.Lf_xs  = kron(I_Ncell,sparse(obj.SDCell.Lf_xs));
            obj.Dx1Lf_xs = kron(I_Ncell,sparse(obj.SDCell.Dx1Lf_xs));
            obj.Dx2Lf_xs= kron(I_Ncell,sparse(obj.SDCell.Dx2Lf_xs));


            clear iLs_xs iLf_xf I_Ncell
            validateattributes(obj.Ls_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Ns]});
            validateattributes(obj.Ls_xsd,{'numeric'},{'2d','size',[Ncell*Nsd,Ncell*Ns]});
            validateattributes(obj.iLs_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Ns]});
            validateattributes(obj.Ls_xf,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Ns]});
            validateattributes(obj.Lf_xf,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Nf]}); 
            validateattributes(obj.iLf_xf,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Nf]});
            validateattributes(obj.Lf_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Nf]});
            validateattributes(obj.Dx1Lf_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Nf]});
            validateattributes(obj.Dx2Lf_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Nf]});
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Methods related to display.
% To enable the displaying of quantities, they must be stored in a grid
% format : hence the use of a "display" grid.

        function g = Reshape_xsd_ctg(obj,c)
        %Reshape_ctg Column to grid reshaping, at a global level.
        % Input:
        %   c (Ncell*prod(Nsd)xM) quantity in column format, following the same
        %   storage convention as <Qxs>.
        % Output:
        %   g (Ny*Nsd(2) x Nx*Nsd(1)xM) quantities in grid format, following
        %   the same convention as <GetInterpolationGrid>.
        
          P = obj.SDCell.Nsd;
          Nx = obj.Mesh.N(1); Ny = obj.Mesh.N(2);
          g = zeros(Ny*P(2),Nx*P(1),size(c,2));

          for n=1:size(g,3)
          for k=0:(Nx-1)
          for l=0:(Ny-1)
              g(l*P(2)+(1:P(2)),k*P(1)+(1:P(1)),:) = obj.SDCell.reshape_xsd_ctg(c(obj.msi(k+1,l+1),:));
          end
          end
          end
          clear Nx Ny P
        end
        
        function xs_c = GetGlobalCoords_Sol(obj)
        %GetGlobalCoords_Sol Return the global coordinates of every
        %solution points in the domain.
        % Output:
        %   xs_c (Ncell*Ns,2) [xs,ys]
            dx = obj.Mesh.cell_size(1);
            dy = obj.Mesh.cell_size(2);
            xs_c = zeros(prod(obj.Mesh.N)*obj.SDCell.Ns,2);
            xs = obj.SDCell.xs_c(:,1);
            ys = obj.SDCell.xs_c(:,2);
            for k=1:obj.Mesh.N(1)
            for l=1:obj.Mesh.N(2)
                tmp=obj.ms(k,l);
                    % local->global
                xs_c(tmp,1) = obj.Mesh.x(k)+ (dx/2)*(xs+1);
                xs_c(tmp,2) = obj.Mesh.y(l)+(dy/2)*(ys-1);
            end
            end 
        end
%----- Definition of the display grids
% These display grids are defined as a concatenation of the
% display grids locally defined on each SDCell.

        function grid = GetInterpolationGrid_f(obj)
        %GetInterpolationGrid_s Generate the flux display grid.
        % Output:
        %   grid (Ny*Nfd(2) x Nx*Nfd(1) x 2)
            grid = GetInterpolationGrid(obj,'f');
        end
        
        function grid = GetInterpolationGrid_s(obj)
        %GetInterpolationGrid_s Generate the solution display grid.
        % Output:
        %   grid (Ny*Nsd(2) x Nx*Nsd(1) x 2)
            grid = GetInterpolationGrid(obj,'s');
        end

        function grid = GetInterpolationGrid(obj,var)
        %GetInterpolationGrid Build a display grid (in global
        %coordinates), from the local SDCell & the mesh.
        % Input:
        %   N (1x2) [N1,N2] Number of points along each axis
        %   var (string)
        %       's' : solution (N=Nsd)
        %       'f' : flux (N=Nfd)
        % Output:
        %   grid (Ny*N2 x Nx*N1 x 2)
        
            validateattributes(var,{'char'},{'nonempty'});

            dx = obj.Mesh.cell_size(1);
            dy = obj.Mesh.cell_size(2);
            Nx = obj.Mesh.N(1);
            Ny = obj.Mesh.N(2);

            if strcmp(var,'s')==1
              N = obj.SDCell.Nsd;
              SDCell_xi_g(:,:,1) = obj.SDCell.xsd_g(:,:,1);
              SDCell_xi_g(:,:,2) = obj.SDCell.xsd_g(:,:,2);
            elseif strcmp(var,'f')==1
              N = obj.SDCell.Nfd;
              SDCell_xi_g(:,:,1) = obj.SDCell.xfd_g(:,:,1);
              SDCell_xi_g(:,:,2) = obj.SDCell.xfd_g(:,:,2);
            else
              error('Unknown <var>.');
            end

            grid = zeros(Ny*N(2),Nx*N(1),2);

            xs = zeros(N(2),N(1)); % sol. pts. global coordinates at cell (k,l)
            ys = zeros(N(2),N(1)); % sol. pts. global coordinates at cell (k,l)
              % (local -> global)

            for k=0:(Nx-1)
              xs = obj.Mesh.x(1+k)+(dx/2)*(SDCell_xi_g(:,:,1)+1);
              for l=0:(Ny-1)
                ys = obj.Mesh.y(1+l)+(dy/2)*(SDCell_xi_g(:,:,2)-1);
                grid(l*N(2)+(1:N(2)),k*N(1)+(1:N(1)),1)=xs;
                grid(l*N(2)+(1:N(2)),k*N(1)+(1:N(1)),2)=ys;
              end
            end
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function obj=Initialize(obj,q0)
        %Initialize Initializes <Qxs> and <CQ> with the function <q0>.
        % Inputs:
        %   q0: ref. constructor description.
        
                % Temp variables
            Nx = obj.Mesh.N(1);
            Ny = obj.Mesh.N(2);
            Ncell = Nx*Ny;
            Ns = obj.SDCell.Ns;
            
            obj.t(1) = 0; % iteration n°0
                % Initialization of Qxs
            obj.Qxs = zeros(Ncell*Ns,obj.Nq,1);
            obj.Qxs = q0(obj.xs_c);
                % Initializes CQ (projection of Q)
            obj.CQ = zeros(Nx*Ny*Ns,obj.Nq,1);
                % Projection: Cons. var. @ sol. points -> polynomial coeff.
            obj.CQ = obj.iLs_xs*obj.Qxs;
            clear Nx Ny dx dy Ncell Ns x y tmp
        end
        
        function dt = GetTimeStep(obj)
        %GetTimeStep Compute the time step for the explicit time
        %integration.
        % Input:
        %   CFL
        %   Mesh size
        %   Maximum speed
        % Output:
        %   dt (1x1) time step
        
            a = dot(obj.GetMaxSpeed(),1./obj.Mesh.cell_size);
            if(a~=0)
                dt = obj.CFL/(a);
            else
                error('Maximum absolute propagation speed is null.\n');
            end
        end
        
    end
    
    methods (Access = public)
        
        
        function obj = Iterate(obj,Deltat)
         %Iterate Iterates to advance time by Deltat.
         % Inputs:
         %  Deltat (1x1) time advance
         %  CQ, Qxs
         % Outputs:
         %  CQ (N new arrays) (more arrays can be added due to
         %  pre-allocation)
         %  t (N new values)
         %  Qxs
         
            validateattributes(Deltat,{'numeric'},{'positive','nonzero'});

            % temp values for the R-K scheme
            % (two temporary values are needed)
            Qt = zeros(size(obj.Qxs,1), size(obj.Qxs,2),2);

            Nx=obj.Mesh.N(1);
            Ny=obj.Mesh.N(2);
            Nf=obj.SDCell.Nf;
            Ns=obj.SDCell.Ns;
                % Get estimate of niter
            obj.dt = obj.GetTimeStep();
            niter = ceil(Deltat/obj.dt);

                % pre-allocate niter matrix
            obj.CQ(:,:,end+niter) = zeros(Nx*Ny*Ns,obj.Nq);

            % Maximum time-step computed from:
            %   CFL number (given by the user)
            %   Maximum propagation speed (computed from the current iter.)
            siter = obj.getIterationNumber()+1;
            n=-1;
            while((obj.t(end)-obj.t(siter))<=Deltat)
                n=n+1;
                    %--  Update time step
                obj.dt = obj.GetTimeStep();
                fprintf('[Iterate] Starting iteration n°%d. Remaining (estimate): %d...\n',n+siter,ceil(((Deltat+obj.t(siter))-obj.t(end))/obj.dt));
                
                    %-- Compute new Qxs
                        %Euler 1st order
%                 obj=obj.ComputeDivF(obj.Qxs,siter+n);
%                 obj.Qxs = obj.Qxs -obj.dt*obj.divF;
                % TVD 3-rd order R-K time integration
                obj.divF=obj.ComputeDivF(obj.CQ(:,:,siter+n));
                Qt(:,:,1) = obj.Qxs - obj.dt*obj.divF;
                obj.divF=obj.ComputeDivF(obj.iLs_xs*Qt(:,:,1));
                Qt(:,:,2) = (3/4)*obj.Qxs+(1/4)*Qt(:,:,1)-(1/4)*obj.dt*obj.divF;
                obj.divF=obj.ComputeDivF(obj.iLs_xs*Qt(:,:,2));
                obj.Qxs=(1/3)*obj.Qxs+(2/3)*Qt(:,:,2)-(2/3)*obj.dt*obj.divF;
                
                    % -- Sync <t> and <CQ> with Q(xs)
                obj.CQ(:,:,siter+n+1)=obj.iLs_xs*obj.Qxs;
                obj.t(end+1) = obj.t(end) + obj.dt; 
            end
            clear Qt
        end
        
       function DisplaySol(obj, iq, iter)
        %DisplayIter Display variable n°<iq> at the solution display 
        %grid.
        % Opens a new plot window.
        % Inputs:
        %   iq (1x1) index of the conservative variable to display
        % Optional input:
        %   iter (1x1) iteration to display (>=0) (default: last computed
        %   iteration)
            
                % Check arguments
            validateattributes(iq,{'numeric'},{'integer','positive','>=',1,'<=',obj.Nq});
            if nargin<3 % if an iteration number has not been given
                iter = obj.getIterationNumber();
            else
                validateattributes(iter,{'numeric'},{'integer','>=',0,'<=',obj.getIterationNumber()});
            end
            
                % Global grid
            x = obj.xsd_g(:,:,1); y=obj.xsd_g(:,:,2);
                % Interpolation : compute values at the solution
                % display grid points
            Qxsd = obj.Ls_xsd*obj.CQ(:,iq,iter+1);
                % Reshape of Q
            z = obj.Reshape_xsd_ctg(Qxsd);
            %[x_node,y_node] = meshgrid(obj.Mesh.x,obj.Mesh.y);
            hold off
            surf(x,y,z,'EdgeAlpha',0);
            colorbar('eastoutside');
            xlabel('x');
            ylabel('y');
            title(sprintf('%s - %s - order: %d\n (Nx=%d,Ny=%d,CFL=%1.1g) it:%d/%d dt=%3.2g/%3.2g',obj.EqnName,obj.SDCell.name,obj.SDCell.order,obj.Mesh.N(1),obj.Mesh.N(2),obj.CFL,iter,obj.getIterationNumber()+1,obj.t(iter+1),obj.t(end)));
            hold on
            %mesh(x_node,y_node,max(Qxsd)*ones(size(x_node)));
            hidden off
            view(0,90);
            set(gca, 'DataAspectRatio', [1 1 1]);
        end
        
        function SaveVideo(obj,iq,viewangle,zaxis,filename,duration)
        %SaveVideo Display a video of all the iterations, and, optionally,
        %save them into a video file.
        % Inputs:
        %   viewangle (1x2) [azimut, elevation] (°)
        %   iq (1x1) conservative variable to display
        %   zaxis (1x2) [zmin zmax]
        % Optional input:
        %   filename (string) video file name
        %   duration (1x1) video duration (default: same as computed
        %   duration)
        % Output : Plots will be displayed and saved.
            niter = obj.getIterationNumber();
            if(niter<1)
                error('No iterations avalaible.\n');
            end
            figure
            if nargin<6
                duration = obj.getElapsedTime();
            else
                validateattributes(duration,{'numeric'},{'<=',obj.getElapsedTime()});
            end
            fps = 15; % frame per second
            nframe = min(ceil(duration*fps),niter); % number of frame to display
            idx_int = ceil(linspace(0,niter,nframe));
            for i=1:length(idx_int)
                obj.DisplaySol(iq,idx_int(i));
                view(viewangle);
                axis([obj.Mesh.bound(1,:),obj.Mesh.bound(2,:),zaxis])
                drawnow % force plot to show
                Vi(i) = getframe(gcf);
            end
            if nargin>=5
                  fprintf('Now generating video file...');
                  movie(Vi);
                  movie2avi(Vi,filename,'fps',fps);
                  fprintf('Done\n');
                  clear Vi
            end
        end
        
        
        
        function Flush()
            %TODO
        end
        

        
        function err = ComputeL2Error(obj,qe)
        %ComputeL2Error
        % Input:
        %   qe (function handle: Nxd -> NxNq) exact solution
        % Output:
        %   err (1xNq) ||Q(xs)-qe(xs)|| (L2 norm)
       
            % Validate qe.
            N = 10;
            if obj.d==2 % 2D Case
                x=linspace(0,1,N)';
                validateattributes(qe([x,x]),{'numeric'},{'2d','size',[N,obj.Nq]});
            end
            clear N x;
            % Check that Qxs is non-empty
            validateattributes(obj.Qxs,{'numeric'},{'2d','size',[prod(obj.Mesh.N)*obj.SDCell.Ns,obj.Nq]});
            % compute L2 error
            Nx = obj.Mesh.N(1);
            Ny = obj.Mesh.N(2);
            dx = obj.Mesh.cell_size(1);
            dy = obj.Mesh.cell_size(2);
            err = zeros(obj.Nq);
            for k=1:Nx
                x = obj.Mesh.x(k)+ (dx/2)*(obj.SDCell.xs_c(:,1)+1); % local->global
            for l=1:Ny
                    % local->global
                y = obj.Mesh.y(l)+(dy/2)*(obj.SDCell.xs_c(:,2)-1);
                Qe = qe([x,y]); % (NsxNq)
                for i=1:obj.Nq % compute L2 error for each variable
                    err(i)=err(i)+sum((obj.Qxs(obj.ms(k,l),i)-Qe(:,i)).^2);
                end
            end
            end
            err = sqrt(err./(obj.SDCell.Ns*prod(obj.Mesh.N))); 
        end
        
        function PrintSummary(obj)
        %PrintSummary Display a summary of the case on the standard input.

            fprintf('---- 2D SD Computational Case (Standard cell) summary:\n');
            fprintf('Equation: %s\n',obj.EqnName);
            fprintf('Computational case: %s\n',obj.CaseName);
            fprintf('Dimension d=%d\n',obj.d);
            fprintf('No. of conservative var. Nq=%d\n',obj.Nq);
            fprintf('No. of iter done %d, t=%1.2g\n',obj.getIterationNumber(),obj.t(end))
            fprintf('--------------------------------\n');
        end
        
                
        function nit=getIterationNumber(obj)
        %getIterationNumber Return the number of iterations performed.
        % Output:
        %   nit (1x1) number of iterations performed
        
            nit = length(obj.t)-1;
            
        end
        
        function DeltaT=getElapsedTime(obj)
        %getElapsedTime Return the elapsed time.
        % Output:
        %   DeltaT (1x1) elapsed time

            DeltaT = obj.t(end)-obj.t(1);
        end
        
        function CheckNormalFluxContinuity(obj)
        % Check normal flux continuit at face flux points.
        % (This is enough to ensure conservativity is the cell is
        % appropriately designed.)
        % (Debugging purposes mainly.)
        
            iscont = true;
            Ny = obj.Mesh.N(2);
            for i=1:size(obj.Mesh.Face_top_bound,1)
                    % Get top and bottom cells (k,l) coordinates
                kb = obj.Mesh.Face_top_bound(i,1);
                lb = obj.Mesh.Face_top_bound(i,2);
                kt=kb; lt=Ny;
                iscont = iscont && (obj.FYxf(obj.mf_t(kb,lb),obj.y_idx)==obj.FYxf(obj.mf_b(kt,lt),obj.y_idx));
            end
            for i=1:size(obj.Mesh.Face_right_bound,1)
                kl=obj.Mesh.Face_right_bound(i,1);
                ll=obj.Mesh.Face_right_bound(i,2);
                kr=1; lr=ll;
                iscont = iscont && (obj.FXxf(obj.mf_l(kr,lr),obj.x_idx) == obj.FXxf(obj.mf_r(kl,ll),obj.x_idx));
            end
            for i=1:size(obj.Mesh.Face_vert_int,1) % loop over every vertical interior face
                kl=obj.Mesh.Face_vert_int(i,1);
                ll=obj.Mesh.Face_vert_int(i,2);
                kr=obj.Mesh.Face_vert_int(i,3);
                lr=obj.Mesh.Face_vert_int(i,4);
                iscont = iscont && (obj.FXxf(obj.mf_l(kr,lr),obj.x_idx) == obj.FXxf(obj.mf_r(kl,ll),obj.x_idx));
            end
                % Horizontal faces
                % ('normal flux' = y-flux)
            for i=1:size(obj.Mesh.Face_hor_int,1) % loop over every horizontal interior face
                kb=obj.Mesh.Face_hor_int(i,1);
                lb=obj.Mesh.Face_hor_int(i,2);
                kt=obj.Mesh.Face_hor_int(i,3);
                lt=obj.Mesh.Face_hor_int(i,4);
                iscont = iscont && (obj.FYxf(obj.mf_b(kt,lt),obj.y_idx) == obj.FYxf(obj.mf_t(kb,lb),obj.y_idx));
            end

            fprintf('Is the normal flux continuous at face flux points ? <isCont>=%b',iscont);
        end
    end
end

