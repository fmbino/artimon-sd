classdef SDCase_2D_Full_Transport < SDCase_2D_Full
    %SDCase_2D_Full_Transport Implements the 2D transport of a scalar
    %quantity.
    % This is simply an implementation of the abstract methods defined in
    % SDCase_2D_Full.
    
    properties (SetAccess = protected)
        d=2;
        Nq=1;
        EqnName='Transport 2D';
        V; % (2x1) [Vx; Vy] propagation speed
        
        x_idx = [1];
        y_idx = [1];
    end
    
    methods (Access = public)
	function Case = SDCase_2D_Full_Transport(V,q0,CFL,Mesh,SDCell,CaseName)
	% (ref. constructor of SDCase_2D_Standard.)
        % Additional input :
        % V (2x1) propagation speed
	    Case = Case@SDCase_2D_Full(q0,CFL,Mesh,SDCell,CaseName);

	    validateattributes(V,{'numeric'},{'2d','size',[2,1]});    
	    Case.V = zeros(2,1);
	    Case.V(1) = V(1);
	    Case.V(2) = V(2);
        end
    end
    
    methods (Access = protected)
            
        function U_max = GetMaxSpeed(obj)
        
            U_max = obj.V;
        end
        
        
        function [Fx,Fy] = Flux(obj,Q)
            
            Fx = obj.V(1) .* Q;
            Fy = obj.V(2) .* Q;
            
        end
        
        function F = RiemannSolver1D(obj,Ql,Qr,n)
            Vn = dot(obj.V,n);
            Q = (Vn>0).*Ql+(Vn<0).*Qr;
            [Fx,Fy] = obj.Flux(Q);
            F = [Fx,Fy]*n;
        end 
    end
    
    methods (Access = public)
        
    end
end

