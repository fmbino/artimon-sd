classdef (Abstract) SDCase_2D_Standard
    %SDCas_2D_Full Abstract class wich describes a computational case for the 
    %Spectral Difference (SD) method.
    % Uses a SDCell which inherits from SDCell_2D_Standard.
    %Abstract methods and properties are equation or dimension dependant
    %(e.g. Euler 1D, Burgers 2D, etc...).
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract methods/properties
% (must be implemented by each subclass.)

    properties (Abstract, SetAccess = protected)
        d; % (1x1) Dimension (1D,2D,...)
        Nq; % (1x1) Number of conservative variables
        EqnName; % (string) Name of the equation(s) solved.
            
            % 'Normal' indexes.
            % Usage: normal projection (Riemann Solver). This is a quick
            % way of a doing a normal projection, since we work with
            % rectangular cell.
            % Example: Euler 2D,
            % state vector is Q = [rho; rho*u; rho*v; rho*E]
            %       x_idx is 1,2,4 (Q_nx = [rho; rho*u; rho*E])
            %       y_idx is 1,3,4 (Q_ny = [rho; rho*v; rho*E])
            
        x_idx; % (1x?) indexes of variables which belong to the 'x' state.
        y_idx; % (1x?) indexes of variables which belong to the 'y' state.
    end

    methods (Abstract, Access=protected)
        % From Qxs, compute the maximum physical speed in every direction.
        % Usage: deduce time step from the CFL number
        % Input:
        %   Qxs Conservative variable 
        % Output:
        %   Absolute Maximumn speed(s) (dx1) (d: dimension. 1D, 2D, ...)
        % (1D: U_max = [ux_max], 2D: U_max=[ux_max; uy_max]
        U_max = GetMaxSpeed(obj);

        % Flux Compute the x-flux and y-flux vectors : Fx(Q) & Fy(Q)
        % Input:
        %   Q (NxNq) State vector (conservative variables)
        % Output:
        %   Fx (NxNq) x-flux vector Fx(Q)
        %   Fy (NxNq) y-flux vector Fy(Q)
        Fx = xFlux(obj, Q)
        Fy = yFlux(obj, Q)

        % RiemannSolver1D Compute the Riemann flux F(Q*). Q* may be the
        % exact or an approximate solution to the Riemann problem.
        % Input:
        %   Ql (NxP) left state vector.
        %   Qr (NxP) right state vector.
        %   n (2x1) [nx;ny] normal vector (needed in some cases)
        % Output:
        %   F (NxP) flux vector.
        % Storage format: storage by line: [Q1; Q2; Q3; ..; QN]
        F = RiemannSolver1D(obj,Ql,Qr,n)
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    properties (SetAccess = protected)
        
        CFL; % (1x1) CFL number
        dt; % (1x1) time-step (deduced from <CFL>)
        CaseName; % (string) Name of the computational case.
        Mesh; % (Mesh_2D_Rect_Uniform) Computational mesh.
        SDCell; % (SDCell_2D_Standard) SD cell.
           % (1x(niter+1)) time corresponding to each iteration
           % 1st value : 0s (iteration n°0)
           % niter : number of iterations performed (>=0)
        t; 
        
        % Polynomial coefficients, conservative variables 
        % @ solution points
        % Rmq: 1D -> Ncell = Nx
        %      2D -> Ncell = Nx*Ny
        % Rmq: All computed values are stored here.
        % Storage convention:
        %   - variables (1 to Nq) on each column
        %   - values over cell follows the same convention as 
        %<SDCell.xs_c> (this is transparent).
        %   - access through 'index functions'.
        % (Ncell*Ns x Nq x P) P>= (niter+1) due to pre-allocation
        CQ; 
        
        xs_c; % (Ncell*Ns x 2) global coordinates of the solution points
              % Format : [xs,ys]
        % Solution points display grid
        % Global coordinates, grid format.
        % xsd_g(:,:,1) = x, xsd_g(:,:,2)=y
        % (top-left) -> (bottom-right) storage convention.
        xsd_g; % (Ny*Nsd(2) x Nx*Nsd(1) x 2)

            % Face index table (for Riemann)
            % For each cell face, indexes to access the left and right 
            % flux points.
            % The format of each of these tables is [iL, iR] (left and right).
            % Usage : Qxf(iL) or Qxf(iR).
        BoundT; % (?x2) Top domain boundary
        BoundR; % (?x2) Right domain boundary
        IntV; % (?x2) Vertical interior faces (excl. top domain boundary)
        IntH; % Horizontal interior faces (excl. right domain boundary)

        %-- Global matrices
            % Polynomial operations
            % (usage example: Q(xs) = Ls(xs)*CQ)
        Ls_xs; % (Ncell*Ns x Ncell*Ns) Ls(xs)
        Ls_xsd; % (Ncell*prod(Nsd) x Ncell*Ns) Ls(xsd)
        iLs_xs; % inverse of <Ls_xs>
        Ls_xfx; % (Ncell*Nf x Ncell*Ns) Ls(xfx)
        Ls_xfy; % (Ncell*Nf x Ncell*Ns) Ls(xfy)
        
        Lfx_xs ; % (Ncell*Ns x Ncell*Nf) Lfx(xs)
        Lfx_xfx; % (Ncell*Nf x Ncell*Nf) Lfx(xfx)
        iLfx_xfx; % inverse of <Lfx_xfx>
        Dx1Lfx_xs; % (Ncell*Ns x Ncell*Nf) derivative (local coord)
        
        Lfy_xs ; % (Ncell*Ns x Ncell*Nf) Lfy(xs)
        Lfy_xfy; % (Ncell*Nf x Ncell*Nf) Lfy(xfy)
        iLfy_xfy; % inverse of <Lfy_xfy>
        Dx2Lfy_xs; % (Ncell*Ns x Ncell*Nf) derivative (local coord)
        
        %-- Marker & Limiter
        Marker; % (1x1) use of a marker
        Limiter; % (1x1) use of a limiter
        alpha=1; % (1x1) AP-TVB marker constant
        MLs; % (NcellxNcell*Ns) MLs*CQ is the mean value over each cell
        MDxLs; % (//) mean value of d/dx (global coord) over each cell
        MDyLs; % (//) mean value of d/dy (global coord) over each cell
        MD2xLs; % (//) // d^2/dx^2
        MD2yLs; % (//) // d^2/dy^2
        k_tr; % (cell) (1xniter) (k,l) of troubled cells 
        l_tr; % (cell) (1xniter)
        
        % -- Temp variables
        % Hold last computed values only
        
        Qxs; % (Ncell*Ns x Nq x 1) Cons. var. @ solution points
        Qxfx; % (Ncell*Nf x Nq x1) Cons. var @ x-flux points
        Qxfy; % (Ncell*Nf x Nq x 1) Cons. var @ y-flux points
        
        FXxfx; % (Ncell*Nf x Nq x1) x-flux @ x-flux points
        FYxfy; % (Ncell*Nf x Nq x1) y-flux @ y-flux points
        
        CFX % (Ncell*Nf x Nq x1) x-flux polynomial coefficients
        CFY; % (Ncell*Nf x Nq x1) y-flux polynomial coefficients
        divF; % (Ncell*Ns x Nq x 1)  div(F(Q)) @ solution points

    end
    
    methods (Access = protected)
       
        function Case = SDCase_2D_Standard(q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter)
        %SDCas_2D_Standard Class constructor.
        %   q0 (Nxd (space) -> NxNq (cons. variables)) function handle, 
        %initial fields for the conservative variables.
        % ex: for (N=2, d=2, Nq=1), 
        %   q0([x1,y1;x2,y2]) = [4;5]
        %   CFL (1x1) CFL number to use for the computation.   
        %   Mesh (Mesh_2D_RectUniform) Computational mesh.
        %   SDCell (SDCell_2D_Standard) SD cell.
        %   CaseName (string) Name of the computational case, for
        %convenience purposes.
        %   Marker (string) Name of the marker to use (mark of troubled
        %   cell)
        %       'AP-TVB' adapted from "A Parameter-Free [...]" (Yang,2009)
        %   Limiter (string) Name of the limiter to use. '1-th order'
        % ex: CaseName='Stability_Nx_10'.
        
                % TODO: validate abstract methods.
        
                % Arguments check
            validateattributes(CFL,{'numeric'},{'scalar','>',0});
            validateattributes(CaseName,{'char'},{'nonempty'});
            validateattributes(Mesh,{'Mesh_2D_RectUniform'},{});
            validateattributes(SDCell,{'SDCell_2D_Standard'},{});
                % Validate q0.
            N = 10;
            if Case.d==2 % 2D Case
                x=linspace(0,1,N)';
                validateattributes(q0([x,x]),{'numeric'},{'2d','size',[N,Case.Nq]});
            end
            clear N x;

            Case.CFL = CFL;
            Case.Mesh = Mesh;
            Case.SDCell = SDCell;
            Case.CaseName = CaseName;
                % Build faces index table (for Riemann)
            Case = Case.buildFacesIndexTable();
                % Build global polynomial interpolation matrices
            Case = Case.buildGlobalInterpolationMatrix();
                % Compute global coordinates of sol. points
            Case.xs_c = Case.GetGlobalCoords_Sol();
                % Initializes Qxs and CQ with q0
            Case = Case.Initialize(q0);
                % Generate the display grid
            Case.xsd_g = Case.GetInterpolationGrid_s();
                % Initialize marker fields
            if(strcmp(Marker,'AP-TVB'))
                Case.Marker=1;
                Case = Case.InitializeMarker();
            else
                Case.Marker=0;
            end
            if(strcmp(Limiter,'1-th order'))
                Case.Limiter=1;
            else
                Case.Limiter=0;
            end
            
        end
        
        function obj = buildFacesIndexTable(obj)
        %buildFacesIndexTable Build tables which give, for each cell face,
        %indexes to access the left and right flux points.
        % The format of each of these tables is [iL, iR] (left and right).
        % Output :
        %   BoundT (?x2) Top domain boundary
        %   BoundR (?x2) Right domain boundary
        %   IntV (?x2) Vertical interior faces (excluding the top domain
        %   boundary faces)
        %   IntH (?x2) Horizontal interior faces (excluding the right 
        % domain boundary faces)
        % Usage : Qxf(iL) or Qxf(iR).
                % Temp. variables
            Nx=obj.Mesh.N(1);
            Ny=obj.Mesh.N(2);
            % Top boundary (Left = Bottom, Right = Top)
            obj.BoundT = zeros(0);
            for i=1:size(obj.Mesh.Face_top_bound,1)
                    % Get top and bottom cells (k,l) coordinates
                kb = obj.Mesh.Face_top_bound(i,1);
                lb = obj.Mesh.Face_top_bound(i,2);
                kt=kb; lt=Ny;
                obj.BoundT = vertcat(obj.BoundT,[obj.mfy_t(kb,lb), obj.mfy_b(kt,lt)]);
            end
            % Right boundary (Left = Left, Right = Right)
            obj.BoundR = zeros(0);
            for i=1:size(obj.Mesh.Face_right_bound,1)
                kl=obj.Mesh.Face_right_bound(i,1);
                ll=obj.Mesh.Face_right_bound(i,2);
                kr=1; lr=ll;
                obj.BoundR = vertcat(obj.BoundR,[obj.mfx_r(kl,ll),obj.mfx_l(kr,lr)]);
            end
            % Interior vertical faces (Left = Left, Right = Right)
            obj.IntV=zeros(0);
            for i=1:size(obj.Mesh.Face_vert_int,1) % loop over every vertical interior face
                kl=obj.Mesh.Face_vert_int(i,1);
                ll=obj.Mesh.Face_vert_int(i,2);
                kr=obj.Mesh.Face_vert_int(i,3);
                lr=obj.Mesh.Face_vert_int(i,4);
                obj.IntV=vertcat(obj.IntV,[obj.mfx_r(kl,ll),obj.mfx_l(kr,lr)]);
            end
            % Interior horizontal faces (Left = Left, Right = Right)
            obj.IntH=zeros(0);
            for i=1:size(obj.Mesh.Face_hor_int,1) % loop over every horizontal interior face
                kb=obj.Mesh.Face_hor_int(i,1);
                lb=obj.Mesh.Face_hor_int(i,2);
                kt=obj.Mesh.Face_hor_int(i,3);
                lt=obj.Mesh.Face_hor_int(i,4);
                obj.IntH=vertcat(obj.IntH,[obj.mfy_t(kb,lb),obj.mfy_b(kt,lt)]);
            end
        end
        function divF = ComputeDivF(obj,CQ)
        %GomputeDivF Computes an approximation of div(F(Q)), using a 
        %Spectral Difference (SD) method.
        % Input:
        %   CQ (Ncell*NsxNq) polynomial coefficients of conservative
        % variables
        % Output:
        %   divF (Ncell*NsxNq) div(F(Q)) @ solution points.
        % Temp. variable used:
        %   Qxfx & Qxfy (Ncell*Nf x Nq x1)
        %   FXxfx (//)
        %   FYxfy (//)
        %   CFX (//)
        %   CFY (//)

        
                % Temp. variables
            Nx=obj.Mesh.N(1);
            Ny=obj.Mesh.N(2);
            dx=obj.Mesh.cell_size(1);
            dy=obj.Mesh.cell_size(2);
            Nf=obj.SDCell.Nf;
            Ns=obj.SDCell.Ns;
            Nq = obj.Nq;
            
                % Interpolation: Cons. var. @ sol. points -> @ flux points
            obj.Qxfx = obj.Ls_xfx*CQ;
            obj.Qxfy = obj.Ls_xfy*CQ;
                % x & y-flux @ flux points
            obj.FXxfx = obj.xFlux(obj.Qxfx);
            obj.FYxfy = obj.yFlux(obj.Qxfy);

            QnL = zeros(0); % left normal state (used for clarity)
            QnR = zeros(0); % right normal state (used for clarity)
            nx = [1;0]; % normal vector, vertical face
            ny = [0;1]; % normal vector, horizontal face
                %-- Boundary Conditions
                %(here, spatial periodicity is enforced)

                % Top boundary: Riemann with bottom boundary
                % ('normal flux' = y-flux)
                % Get left and right normal state Qn
            QnL = obj.Qxfy(obj.BoundT(:,1),obj.y_idx);
            QnR = obj.Qxfy(obj.BoundT(:,2),obj.y_idx);
            obj.FYxfy(obj.BoundT(:,1),obj.y_idx)=obj.RiemannSolver1D(QnL,QnR,ny);
            obj.FYxfy(obj.BoundT(:,2),obj.y_idx)=obj.FYxfy(obj.BoundT(:,1),obj.y_idx);
                % Right boundary: Riemann with left boundary
                % ('normal flux' = x-flux)
            QnL = obj.Qxfx(obj.BoundR(:,1),obj.x_idx);
            QnR = obj.Qxfx(obj.BoundR(:,2),obj.x_idx);
            obj.FXxfx(obj.BoundR(:,1),obj.x_idx) = obj.RiemannSolver1D(QnL,QnR,nx);
            obj.FXxfx(obj.BoundR(:,2),obj.x_idx) = obj.FXxfx(obj.BoundR(:,1),obj.x_idx);
                %-- Riemann Solver on interior faces
                % Vertical faces
                % ('normal flux' = x-flux)
            QnL = obj.Qxfx(obj.IntV(:,1),obj.x_idx);
            QnR = obj.Qxfx(obj.IntV(:,2),obj.x_idx);
            obj.FXxfx(obj.IntV(:,1),obj.x_idx) = obj.RiemannSolver1D(QnL,QnR,nx);
            obj.FXxfx(obj.IntV(:,2),obj.x_idx) = obj.FXxfx(obj.IntV(:,1),obj.x_idx);
                % Horizontal faces
                % ('normal flux' = y-flux)
            QnL = obj.Qxfy(obj.IntH(:,1),obj.y_idx);
            QnR = obj.Qxfy(obj.IntH(:,2),obj.y_idx);
            obj.FYxfy(obj.IntH(:,1),obj.y_idx) = obj.RiemannSolver1D(QnL,QnR,ny);
            obj.FYxfy(obj.IntH(:,2),obj.y_idx) = obj.FYxfy(obj.IntH(:,1),obj.y_idx);
            clear QnR QnL
                %-- div(F(Q)) @ sol. points from fluxes @ flux points
                % flux @ flux points -> polynomial coefficients
            obj.CFX = obj.iLfx_xfx*obj.FXxfx;
            obj.CFY = obj.iLfy_xfy*obj.FYxfy;
                % flux derivative @ solution points -> div(F)
                % ! local != global coordinates (hence metric terms)
            divF = (2/dx)*(obj.Dx1Lfx_xs)*obj.CFX+...
                      (2/dy)*(obj.Dx2Lfy_xs)*obj.CFY;
        end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Marker & Limiter methods.
% Note: should eventually be put in a separate class.
    function obj = InitializeMarker(obj)
    %InitializeMarker
                % Necessary constants
            Ncell = prod(obj.Mesh.N);
            Ns = obj.SDCell.Ns;
            I_Ncell = speye(Ncell);
            dx = obj.Mesh.cell_size(1);
            dy = obj.Mesh.cell_size(2);
                % Initialization as sparse
            obj.MLs = sparse(1,1,1);
            obj.MDxLs = sparse(1,1,1);
            obj.MDyLs = sparse(1,1,1);
            obj.MD2xLs = sparse(1,1,1);
            obj.MD2yLs = sparse(1,1,1);
            
            obj.MLs = kron(I_Ncell,sparse(obj.SDCell.MLs));
                % local->global
            obj.MDxLs = kron(I_Ncell,(2/dx)*sparse(obj.SDCell.MDx1Ls));
            obj.MDyLs = kron(I_Ncell,(2/dy)*sparse(obj.SDCell.MDx2Ls));
            obj.MD2xLs = kron(I_Ncell,(2/dx)^2*sparse(obj.SDCell.MD2x1Ls));
            obj.MD2yLs = kron(I_Ncell,(2/dy)^2*sparse(obj.SDCell.MD2x2Ls));
            clear I_Ncell
            validateattributes(obj.MLs,{'numeric'},{'2d','size',[Ncell,Ncell*Ns]});
            validateattributes(obj.MDxLs,{'numeric'},{'2d','size',[Ncell,Ncell*Ns]});
            validateattributes(obj.MDyLs,{'numeric'},{'2d','size',[Ncell,Ncell*Ns]});
            validateattributes(obj.MD2xLs,{'numeric'},{'2d','size',[Ncell,Ncell*Ns]});
            validateattributes(obj.MD2yLs,{'numeric'},{'2d','size',[Ncell,Ncell*Ns]});
            
    end
    
    function [ktr,ltr] = PerformMarking(obj,Qxs)
    %PerformMarking
    % Output:
    %   ktr (?x1)
    %   ltr (?x1)

        Nx = obj.Mesh.N(1);
        Ny = obj.Mesh.N(2);
        dx = obj.Mesh.cell_size(1);
        dy = obj.Mesh.cell_size(2);
        obj.Qxfx = obj.Ls_xfx*Qxs;
        obj.Qxfy = obj.Ls_xfy*Qxs;
        CQ = obj.iLs_xs*Qxs;
            % Mean values
        MQ = obj.MLs*CQ;
        MDxQ = obj.MDxLs*CQ;
        MDyQ = obj.MDyLs*CQ;
        MD2xQ = obj.MD2xLs*CQ;
        MD2yQ = obj.MD2yLs*CQ;
        DQmax = max(Qxs)-min(Qxs);
        fprintf('Max value of D2xQ: %2.1g, D2xQ: %2.1g, Qmax-Qmin=%2.1g, DQ/dx^2=%2.1g DQ/dx^2=%2.1g\n',max(MD2xQ),max(MD2yQ),max(Qxs)-min(Qxs),(DQmax/(dx^2)),(DQmax/(dx^2)));
        MQmax_x = zeros(Nx*Ny,1); MQmin_x = zeros(Nx*Ny,1);
        MQmax_y = zeros(Nx*Ny,1); MQmin_y = zeros(Nx*Ny,1);
        ktr_1st = zeros(0); ltr_1st = zeros(0);
       
            % loop over each cell
            for k=1:Nx
                for l=1:Ny
                        % neighbors
                    lt = (l-1-Ny)*(l>1)+Ny; lb = (l)*(l<Ny)+1;
                    kl = (k-1-Nx)*(k>1)+Nx; kr = (k)*(k<Nx)+1;
                    ic = obj.mc(k,l); % index
                    it = obj.mc(k,lt);
                    ib = obj.mc(k,lb);
                    il = obj.mc(kl,l);
                    ir = obj.mc(kr,l);
                    MQ_x = [MQ(ic),MQ(il),MQ(ir)];
                    MQ_y = [MQ(ic),MQ(it),MQ(ib)];
                    MQmax_x(ic) = max(max(MQ_x),eps);
                    MQmin_x(ic) = max(min(MQ_x),eps);
                    MQmax_y(ic) = max(max(MQ_y),eps);
                    MQmin_y(ic) = max(min(MQ_y),eps);
                    if ( sum(max(obj.Qxfx(obj.mf(k,l)),eps)<0.99*MQmin_x(ic))>0 || ...
                         sum(max(obj.Qxfy(obj.mf(k,l)),eps)<0.99*MQmin_y(ic))>0 || ...
                         sum(max(obj.Qxfx(obj.mf(k,l)),eps)>1.001*MQmax_x(ic))>0 || ...
                         sum(max(obj.Qxfy(obj.mf(k,l)),eps)>1.001*MQmax_y(ic))>0)
                        ktr_1st(end+1) = k;
                        ltr_1st(end+1) = l;
                    end
                end
            end
        fprintf('No. of troubled cells (1st stage): %d/%d.\n',length(ktr_1st),Nx*Ny);
        % loop over each troubled cells
        beta = 2.0;
        ktr = zeros(0); ltr = zeros(0);
        for i=1:length(ktr_1st)
                k = ktr_1st(i); l = ltr_1st(i);
                    % neighbors
                lt = (l-1-Ny)*(l>1)+Ny; lb = (l)*(l<Ny)+1;
                kl = (k-1-Nx)*(k>1)+Nx; kr = (k)*(k<Nx)+1;
                ic = obj.mc(k,l); % index
                it = obj.mc(k,lt);
                ib = obj.mc(k,lb);
                il = obj.mc(kl,l);
                ir = obj.mc(kr,l);
                a=minmod_mod(MD2xQ(ic),beta*(MDxQ(ir)-MDxQ(ic))/(dx),beta*(MDxQ(ic)-MDxQ(il))/(dx),(DQmax/((obj.alpha*dx)^2)));
                b=minmod_mod(MD2yQ(ic),beta*(MDyQ(it)-MDyQ(ic))/(dy),beta*(MDyQ(ic)-MDyQ(ib))/(dy),(DQmax/((obj.alpha*dy)^2)));
                %fprintf('%1.2e=minmod( %1.2e, %1.2e, %1.2e)\n',a,MD2xQ(ic),beta*(MDxQ(ir)-MDxQ(ic))/(dx),beta*(MDxQ(ic)-MDxQ(il))/(dx));
                if(~(a==MD2xQ(ic) && b==MD2yQ(ic)))
                    ktr(end+1)=ktr_1st(i);
                    ltr(end+1)=ltr_1st(i);
                    %fprintf('\t->troubled cell\n');
                else
                   %fprintf('\t->cell OK\n');
                end
        end
        fprintf('No. of troubled cells (2nd stage): %d/%d.\n',length(ktr),length(ktr_1st));
        ktr = ktr';
        ltr=ltr';
    end
    
    function Qxs = PerformLimiting(obj,Qxs_in,tr)
    %PerformLimiting Rough 1-th order conservative limiting.
    % Input:
    %   tr (?x2) troubled cells : [k,l]
        CQ = obj.iLs_xs*Qxs_in;
        MQ = obj.MLs*CQ;

        for i=1:size(tr,1) % loop over each troubled cell
            k=tr(i,1); l=tr(i,2);
            CQ(obj.ms(k,l),:)=0; % constant polynomial, same mass
            CQ(obj.ms_1st(k,l),:) = MQ(obj.mc(k,l),:)./obj.MLs(obj.mc(k,l),obj.ms_1st(k,l));
        end
        Qxs = obj.Ls_xs*CQ;
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Index functions.
    % They are necessary to browse the data structures in column
    %format.
    % There are 3 different storage conventions:
    %   1 - quantity @ solution points (Ns points per cell).
    %       -> functions: ms_1st, ms
    %   2 - quantity @ x or y - flux points (Nf points per cell)
    %       -> functions: mf_1st, mf
    %   3 - quantity @ display solution points (prod(Nsd) points
    %   per cell)
    %       -> functions: msi_1st, msi
    % Usage example: Q(ms_1st(k,l),:).
    % (k,l) range from (1,1) to (Nx,Ny).
        
        function mcidx = mc(obj,k,l)
        %mc Index number of cell (k,l).
            Nx = obj.Mesh.N(1);
            mcidx = 1+(l-1)*Nx*1+(k-1)*1;
        end
                % -- First index functions
        function msidx = ms_1st(obj,k,l)
        %ms_1st ms_1st(k,l) is the FIRST data index wich belongs to the 
        %cell (k,l).
            Nx = obj.Mesh.N(1);
            N = obj.SDCell.Ns;
            msidx = 1+(l-1)*Nx*N+(k-1)*N;
        end
        
        function msidx = mf_1st(obj,k,l)
        %mf_1st mf_1st(k,l) is the FIRST data index wich belongs to the 
        %cell (k,l).
            Nx = obj.Mesh.N(1);
            N = obj.SDCell.Nf;
            msidx = 1+(l-1)*Nx*N+(k-1)*N;
        end
        
        function msidx = msi_1st(obj,k,l)
        %msi_1st msi_1st(k,l) is the FIRST data index wich belongs to the 
        %cell (k,l).
            Nx = obj.Mesh.N(1);
            N = prod(obj.SDCell.Nsd);
            msidx = 1+(l-1)*Nx*N+(k-1)*N;
        end
        
                % -- Index range functions
        function msidx = ms(obj,k,l)
        %ms Indexes of all the points at the cell(k,l).
            N = obj.SDCell.Ns;
            msidx = obj.ms_1st(k,l) + (0:(N-1));
        end
        
        function mfidx = mf(obj,k,l)
        %mf Indexes of all the points at the cell(k,l).
            N = obj.SDCell.Nf;
            mfidx = obj.mf_1st(k,l)+(0:(N-1));
        end
        
        function msidx = msi(obj,k,l)
        %msi Indexes of all the points at the cell(k,l).
            N = prod(obj.SDCell.Nsd);
            msidx = obj.ms_1st(k,l) + (0:(N-1));
        end
        
                % -- Index range functions to segregate flux points
        function mfidx = mfy_t(obj,k,l)
        %mf_t Top flux points indexes at the cell (k,l).
        % (y-Flux point only.)
            mfidx = obj.mf_1st(k,l) + (obj.SDCell.idx_xfy_t-1);
        end
        
        function mfidx = mfy_b(obj,k,l)
        %mf_b Bottom flux points indexes at the cell (k,l).
        % (y-Flux point only.)
            mfidx = obj.mf_1st(k,l) + (obj.SDCell.idx_xfy_b-1);
        end
        
        function mfidx = mfx_l(obj,k,l)
        %mf_l Left flux points indexes at the cell (k,l).
        % (x-Flux point only.)
            mfidx = obj.mf_1st(k,l) + (obj.SDCell.idx_xfx_l-1);
        end
        
        function mfidx = mfx_r(obj,k,l)
        %mf_r Right flux points indexes at the cell (k,l).
        % (x-Flux point only.)
            mfidx = obj.mf_1st(k,l) + (obj.SDCell.idx_xfx_r-1);
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Polynomial Interpolation method(s).
% (Creation of polynomial interpolation matrices.)
        function obj=buildGlobalInterpolationMatrix(obj)
        %buildGlobalInterpolationMatrix Build (using the matrices in the
        %SDCell) all the global polynomial interpolation matrices.
        % All the matrices are created as sparse ones.
        % (usage example: Q(xs) = Ls_xs*CQ )
        
                % Necessary constants
            Ncell = prod(obj.Mesh.N);
            Ns = obj.SDCell.Ns;
            Nf = obj.SDCell.Nf;
            Nsd = prod(obj.SDCell.Nsd);
                % Elementary matrices (defined at cell-level)
            iLs_xs = inv(obj.SDCell.Ls_xs);
            iLfx_xfx = inv(obj.SDCell.Lfx_xfx);
            iLfy_xfy = inv(obj.SDCell.Lfy_xfy);
            
                % Initialization as sparse
            obj.Ls_xs = sparse(1,1,1);
            obj.Ls_xsd = sparse(1,1,1);
            obj.iLs_xs = sparse(1,1,1);
            obj.Ls_xfx = sparse(1,1,1);
            obj.Ls_xfy = sparse(1,1,1);
            obj.Lfx_xs = sparse(1,1,1);
            obj.Lfx_xfx = sparse(1,1,1);
            obj.iLfx_xfx = sparse(1,1,1);
            obj.Dx1Lfx_xs = sparse(1,1,1);
            obj.Lfy_xs = sparse(1,1,1);
            obj.Lfy_xfy = sparse(1,1,1);
            obj.iLfy_xfy = sparse(1,1,1);
            obj.Dx2Lfy_xs = sparse(1,1,1);
            
            I_Ncell = speye(Ncell); 
            obj.Ls_xs = kron(I_Ncell,sparse(obj.SDCell.Ls_xs));
            obj.Ls_xsd = kron(I_Ncell,sparse(obj.SDCell.Ls_xsd));
            obj.iLs_xs = kron(I_Ncell,sparse(iLs_xs));
            obj.Ls_xfx = kron(I_Ncell,sparse(obj.SDCell.Ls_xfx));
            obj.Ls_xfy = kron(I_Ncell,sparse(obj.SDCell.Ls_xfy));
            obj.Lfx_xfx = kron(I_Ncell,sparse(obj.SDCell.Lfx_xfx));
            obj.iLfx_xfx = kron(I_Ncell,sparse(iLfx_xfx));
            obj.Lfx_xs  = kron(I_Ncell,sparse(obj.SDCell.Lfx_xs));
            obj.Dx1Lfx_xs = kron(I_Ncell,sparse(obj.SDCell.Dx1Lfx_xs));
            obj.Lfy_xfy = kron(I_Ncell,sparse(obj.SDCell.Lfy_xfy));
            obj.iLfy_xfy = kron(I_Ncell,sparse(iLfy_xfy));
            obj.Lfy_xs = kron(I_Ncell,sparse(obj.SDCell.Lfy_xs));
            obj.Dx2Lfy_xs= kron(I_Ncell,sparse(obj.SDCell.Dx2Lfy_xs));
            
            
            clear iLs_xs  iLfx_xfx iLfy_xfy I_Ncell
            validateattributes(obj.Ls_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Ns]});
            validateattributes(obj.Ls_xsd,{'numeric'},{'2d','size',[Ncell*Nsd,Ncell*Ns]});
            validateattributes(obj.iLs_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Ns]});
            validateattributes(obj.Ls_xfx,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Ns]});
            validateattributes(obj.Ls_xfy,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Ns]});
            validateattributes(obj.Lfx_xfx,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Nf]}); 
            validateattributes(obj.iLfx_xfx,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Nf]});
            validateattributes(obj.Lfx_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Nf]});
            validateattributes(obj.Dx1Lfx_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Nf]});
            validateattributes(obj.Lfy_xfy,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Nf]}); 
            validateattributes(obj.iLfy_xfy,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Nf]});
            validateattributes(obj.Lfy_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Nf]});
            validateattributes(obj.Dx2Lfy_xs,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Nf]});
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Methods related to display.
% To enable the displaying of quantities, they must be stored in a grid
% format : hence the use of a "display" grid.
        
        function g = Reshape_xsd_ctg(obj,c)
        %Reshape_ctg Column to grid reshaping, at a global level.
        % Input:
        %   c (Ncell*prod(Nsd)xM) quantity in column format, following the same
        %   storage convention as <Qxs>.
        % Output:
        %   g (Ny*Nsd(2) x Nx*Nsd(1)xM) quantities in grid format, following
        %   the same convention as <GetInterpolationGrid>.
        
            P = obj.SDCell.Nsd;
            Nx = obj.Mesh.N(1); Ny = obj.Mesh.N(2);
            g = zeros(Ny*P(2),Nx*P(1),size(c,2));

            for n=1:size(g,3)
            for k=0:(Nx-1)
              for l=0:(Ny-1)
              g(l*P(2)+(1:P(2)),k*P(1)+(1:P(1)),:) = obj.SDCell.reshape_xsd_ctg(c(obj.msi(k+1,l+1),:));
              end
            end
            end
            clear Nx Ny P
        end

        function xs_c = GetGlobalCoords_Sol(obj)
        %GetGlobalCoords_Sol Return the global coordinates of every
        %solution points in the domain.
        % Output:
        %   xs_c (Ncell*Ns,2) [xs,ys]
            dx = obj.Mesh.cell_size(1);
            dy = obj.Mesh.cell_size(2);
            xs_c = zeros(prod(obj.Mesh.N)*obj.SDCell.Ns,2);
            xs = obj.SDCell.xs_c(:,1);
            ys = obj.SDCell.xs_c(:,2);
            for k=1:obj.Mesh.N(1)
            for l=1:obj.Mesh.N(2)
            tmp=obj.ms(k,l);
                % local->global
            xs_c(tmp,1) = obj.Mesh.x(k)+ (dx/2)*(xs+1);
            xs_c(tmp,2) = obj.Mesh.y(l)+(dy/2)*(ys-1);
            end
            end 
        end
%----- Definition of the display grids
% These display grids are defined as a concatenation of the
% display grids locally defined on each SDCell.

        function grid = GetInterpolationGrid_s(obj)
        %GetInterpolationGrid_s Generate the solution display grid.
        % Output:
        %   grid (Ny*Nsd(2) x Nx*Nsd(1) x 2)
            grid = GetInterpolationGrid(obj,'s');
        end

        function grid = GetInterpolationGrid(obj,var)
        %GetInterpolationGrid Build a display grid (in global
        %coordinates), from the local SDCell & the mesh.
        % Input:
        %   N (1x2) [N1,N2] Number of points along each axis
        %   var (string)
        %       's' : solution (N=Nsd)
        %       'f' : flux (N=Nfd) (not used in this class)
        % Output:
        %   grid (Ny*N2 x Nx*N1 x 2)
        
            validateattributes(var,{'char'},{'nonempty'});

            dx = obj.Mesh.cell_size(1);
            dy = obj.Mesh.cell_size(2);
            Nx = obj.Mesh.N(1);
            Ny = obj.Mesh.N(2);

            if strcmp(var,'s')==1
              N = obj.SDCell.Nsd;
              SDCell_xi_g(:,:,1) = obj.SDCell.xsd_g(:,:,1);
              SDCell_xi_g(:,:,2) = obj.SDCell.xsd_g(:,:,2);
            else
              error('Unknown <var>.');
            end

            grid = zeros(Ny*N(2),Nx*N(1),2);

            xs = zeros(N(2),N(1)); % sol. pts. global coordinates at cell (k,l)
            ys = zeros(N(2),N(1)); % sol. pts. global coordinates at cell (k,l)
              % (local -> global)
            for k=0:(Nx-1)
              xs = obj.Mesh.x(1+k)+(dx/2)*(SDCell_xi_g(:,:,1)+1);
              for l=0:(Ny-1)
                    ys = obj.Mesh.y(1+l)+(dy/2)*(SDCell_xi_g(:,:,2)-1);
                    grid(l*N(2)+(1:N(2)),k*N(1)+(1:N(1)),1)=xs;
                    grid(l*N(2)+(1:N(2)),k*N(1)+(1:N(1)),2)=ys;
              end
            end
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        function obj=Initialize(obj,q0)
        %Initialize Initializes <Qxs>, <Qxfx>, <Qxfy> and <CQ> with the 
        %function <q0>.
        % Inputs:
        %   q0 (function handle) ref. constructor description.
        
                % Temp variables
            Nx = obj.Mesh.N(1);
            Ny = obj.Mesh.N(2);
            Ncell = Nx*Ny;
            Ns = obj.SDCell.Ns;
            
            obj.t(1) = 0; % iteration n°0
                % Initialization of Qxs
            obj.Qxs = zeros(Ncell*Ns,obj.Nq,1);
            obj.Qxs = q0(obj.xs_c);
                % Initializes CQ (projection of Q)
            obj.CQ = zeros(Nx*Ny*Ns,obj.Nq,1);
                % Projection: Cons. var. @ sol. points -> polynomial coeff.
            obj.CQ = obj.iLs_xs*obj.Qxs;
            obj.Qxfx = obj.Ls_xfx*obj.CQ;
            obj.Qxfy = obj.Ls_xfy*obj.CQ;
            
            
            clear Nx Ny dx dy Ncell Ns x y tmp
        end
        
        function dt = GetTimeStep(obj)
        %GetTimeStep Compute the time step for the explicit time
        %integration.
        % Input:
        %   CFL
        %   Mesh size
        %   Maximum speed
        % Output:
        %   dt (1x1) time step
        
            a = dot(obj.GetMaxSpeed(),1./obj.Mesh.cell_size);
            if(a~=0)
                dt = obj.CFL/(a);
            else
                error('Maximum absolute propagation speed is null.\n');
            end
        end
    end
    methods (Access = public)
        function obj = Iterate(obj,Deltat)
         %Iterate Iterates to advance time by Deltat.
         % Inputs:
         %  Deltat (1x1) time advance
         %  CQ, Qxs
         % Outputs:
         %  CQ (N new arrays) (more arrays can be added due to
         %  pre-allocation)
         %  t (N new values)
         %  Qxs
        
            validateattributes(Deltat,{'numeric'},{'positive','nonzero'});
                % temp values for the R-K scheme
                % (two temporary values are needed)
            Qt = zeros(size(obj.Qxs,1), size(obj.Qxs,2),2);
            Nx=obj.Mesh.N(1);
            Ny=obj.Mesh.N(2);
            Nf=obj.SDCell.Nf;
            Ns=obj.SDCell.Ns;
                % Get estimate of the nb. of iterations to perform
            obj.dt = obj.GetTimeStep();
            niter = ceil(Deltat/obj.dt);

                % pre-allocate niter matrix
            obj.CQ(:,:,end+niter) = zeros(Nx*Ny*Ns,obj.Nq);
            
                % Maximum time-step computed from:
                %   CFL number (given by the user)
                %   Maximum propagation speed (computed from the current
                %   iter.).
                % first iteration to be performed is n°<siter> (>=1)
            siter = obj.getIterationNumber()+1;
            n=-1;
            while((obj.t(end)-obj.t(siter))<=Deltat)
                n=n+1;
                    % Marker & Limiter on previous iteration
                if(obj.Marker==1)
                    [k_tr,l_tr] = obj.PerformMarking(obj.Qxs);
                    obj.k_tr{siter+n} = k_tr;
                    obj.l_tr{siter+n} = l_tr;
                    %obj.DisplayTroubledCells(1,n);
                else % no troubled cells
                    obj.k_tr{siter+n} = [];
                    obj.l_tr{siter+n} = [];
                end
                if(obj.Limiter==1)
                   obj.Qxs=obj.PerformLimiting(obj.Qxs,[obj.k_tr{siter+n},obj.l_tr{siter+n}]); 
                end
                
                        % Update time step
                obj.dt = obj.GetTimeStep();
                fprintf('[Iterate] Starting iteration n°%d. Remaining (estimate): %d...\n',n+siter,ceil(((Deltat+obj.t(siter))-obj.t(end))/obj.dt));
                        
                    % -- Compute new Qxs
                        % Euler 1st order
%                 obj.divF=obj.ComputeDivF(obj.CQ(:,:,siter+n));
%                 obj.Qxs = obj.Qxs -obj.dt*obj.divF;
%                 
                        % TVD 3-rd order R-K time integration
                obj.divF=obj.ComputeDivF(obj.CQ(:,:,siter+n));
                Qt(:,:,1) = obj.Qxs - obj.dt*obj.divF;
                obj.divF=obj.ComputeDivF(obj.iLs_xs*Qt(:,:,1));
                Qt(:,:,2) = (3/4)*obj.Qxs+(1/4)*Qt(:,:,1)-(1/4)*obj.dt*obj.divF;
                obj.divF=obj.ComputeDivF(obj.iLs_xs*Qt(:,:,2));
                obj.Qxs=(1/3)*obj.Qxs+(2/3)*Qt(:,:,2)-(2/3)*obj.dt*obj.divF;
                
                    % -- Sync <t> and <CQ> with Q(xs)
                obj.CQ(:,:,siter+n+1)=obj.iLs_xs*obj.Qxs;
                obj.t(end+1) = obj.t(end) + obj.dt;
            end
            clear Qt
        end
        
        function DisplaySol(obj, iq, iter)
        %DisplayIter Display variable n°<iq> at the solution display 
        %grid.
        % Opens a new plot window.
        % Inputs:
        %   iq (1x1) index of the conservative variable to display
        % Optional input:
        %   iter (1x1) iteration to display (>=0) (default: last computed
        %   iteration)

                % Check arguments
            validateattributes(iq,{'numeric'},{'integer','positive','>=',1,'<=',obj.Nq});
            if nargin<3 % if an iteration number has not been given
                iter = obj.getIterationNumber();
            else
                validateattributes(iter,{'numeric'},{'integer','>=',0,'<=',obj.getIterationNumber()});
            end
            
                % Global grid
            x = obj.xsd_g(:,:,1); y=obj.xsd_g(:,:,2);
                % Interpolation : compute values at the solution
                % display grid points
            Qxsd = obj.Ls_xsd*obj.CQ(:,iq,iter+1);
                % Reshape of Q
            z = obj.Reshape_xsd_ctg(Qxsd);
            %[x_node,y_node] = meshgrid(obj.Mesh.x,obj.Mesh.y);
            hold off
            surf(x,y,z,'EdgeAlpha',0);
            colorbar('eastoutside');
            xlabel('x');
            ylabel('y');
            title(sprintf('%s - %s - order: %d\n (Nx=%d,Ny=%d,CFL=%1.1g) it:%d/%d dt=%3.2g/%3.2g',obj.EqnName,obj.SDCell.name,obj.SDCell.order,obj.Mesh.N(1),obj.Mesh.N(2),obj.CFL,iter,obj.getIterationNumber(),obj.t(iter+1),obj.t(end)));
            hold on
            %mesh(x_node,y_node,max(Qxsd)*ones(size(x_node)));
            hidden off
            view(0,90);
            set(gca, 'DataAspectRatio', [1 1 1]);
        end
        function DisplayTroubledCells(obj,iq,it)
        %DisplayTroubledCells Display on the current figure.
        % Input:
        %   iq (1x1) conservative variable to display
        %  Optional input:
        %   it (1x1) iteration number (>=0) (default: last marked
        %   iteration)

            validateattributes(iq,{'numeric'},{'integer','positive','>=',1,'<=',obj.Nq});
            if nargin<3 % if an iteration number has not been given
                it = size(obj.k_tr,2)-1;
            else
                validateattributes(it,{'numeric'},{'integer','>=',0,'<=',size(obj.k_tr,2)-1});
            end
            % Global grid
            x = obj.xsd_g(:,:,1); y=obj.xsd_g(:,:,2);
            Qxsd = obj.Ls_xsd*obj.CQ(:,iq,it+1);
            z = obj.Reshape_xsd_ctg(Qxsd);
            tr = [obj.k_tr{it+1},obj.l_tr{it+1}];

            clf
            hold off
            surf(x,y,z,'EdgeAlpha',0);
            colorbar('eastoutside');
            ylabel('y');
            xlabel('x');
            title(sprintf('%s - %s - order: %d\n (Nx=%d,Ny=%d,CFL=%1.1g) it:%d/%d dt=%3.2g/%3.2g\n%d Troubled cells - alpha=%d',obj.EqnName,obj.SDCell.name,obj.SDCell.order,obj.Mesh.N(1),obj.Mesh.N(2),obj.CFL,it,obj.getIterationNumber(),obj.t(it+1),obj.t(end),size(tr,1),obj.alpha));
            hold on
            hidden off
            set(gca, 'DataAspectRatio', [1 1 1]);
            for i=1:size(tr,1)
                k=tr(i,1); l=tr(i,2);
                [X,Y] = meshgrid(obj.Mesh.x(k:(k+1)),obj.Mesh.y(l:(l+1)));
                surf(X,Y,1.00*max(Qxsd)*ones(size(X)),'FaceAlpha',0);
            end
        end
        
        function SaveVideoWithTroubledCells(obj,iq,viewangle,zaxis,filename,duration)
        %SaveVideoWithTroubledCells Same as <SaveVideo>, but with troubled
        %cells.
            validateattributes(iq,{'numeric'},{'integer','positive','>=',1,'<=',obj.Nq})
            niter = size(obj.k_tr,2)-1;
            if(niter<0)
                error('No marked iterations avalaible.\n');
            end
            figure
            if nargin<6
                duration = obj.getElapsedTime();
            else
                validateattributes(duration,{'numeric'},{'<=',obj.getElapsedTime()});
            end
            fps = 15; % frame per second
            nframe = min(ceil(duration*fps),niter); % number of frame to display
            idx_int = ceil(linspace(0,niter-1,nframe));
            for i=1:length(idx_int)
                obj.DisplayTroubledCells(iq,idx_int(i)+1);
                view(viewangle);
                axis([obj.Mesh.bound(1,:),obj.Mesh.bound(2,:),zaxis])
                Vi(i) = getframe(gcf);
            end
            if nargin>=5
                fprintf('Now generating video file...');
                movie(Vi);
                movie2avi(Vi,filename,'fps',fps);
                fprintf('Done\n');
                clear Vi
            end
        end
        
        function SaveVideo(obj,iq,viewangle,zaxis,filename,duration)
        %SaveVideo Display a video of all the iterations, and, optionally,
        %save them into a video file.
        % Inputs:
        %   viewangle (1x2) [azimut, elevation] (°)
        %   iq (1x1) index of conservative variable to display.
        %   zaxis (1x2) [zmin zmax]
        % Optional input:
        %   filename (string) video file name
        %   duration (1x1) video duration (default: same as computed
        %   duration)
        % Output : Plots will be displayed and saved.
            validateattributes(iq,{'numeric'},{'integer','positive','>=',1,'<=',obj.Nq});
            niter = obj.getIterationNumber();
            if(niter<1)
                error('No iterations avalaible.\n');
            end
            figure
            if nargin<6
                duration = obj.getElapsedTime();
            else
                validateattributes(duration,{'numeric'},{'<=',obj.getElapsedTime()});
            end
            fps = 15; % frame per second
            nframe = min(ceil(duration*fps),niter); % number of frame to display
            idx_int = ceil(linspace(0,niter,nframe));
            for i=1:length(idx_int)
                obj.DisplaySol(iq,idx_int(i));
                view(viewangle);
                axis([obj.Mesh.bound(1,:),obj.Mesh.bound(2,:),zaxis])
                drawnow % force plot to show
                Vi(i) = getframe(gcf);
            end
            if nargin>=5
                  fprintf('Now generating video file...');
                  movie(Vi);
                  movie2avi(Vi,filename,'fps',fps);
                  fprintf('Done\n');
                  clear Vi
            end
        end
        
        
        function Flush()
            % TODO
        end
        
        function err = ComputeL2Error(obj,qe)
        %ComputeL2Error
        % Input:
        %   qe (function handle: Nxd -> NxNq) exact solution
        % Output:
        %   err (1xNq) ||Q(xs)-qe(xs)|| (L2 norm)
        
                % Validate qe.
            N = 10;
            if obj.d==2 % 2D Case
                x=linspace(0,1,N)';
                validateattributes(qe([x,x]),{'numeric'},{'2d','size',[N,obj.Nq]});
            end
            clear N x;
                % Check that Qxs is non-empty
            validateattributes(obj.Qxs,{'numeric'},{'2d','size',[prod(obj.Mesh.N)*obj.SDCell.Ns,obj.Nq]});
                % Compute L2 error
            err = norm(obj.Qxs-qe(obj.xs_c),2);
            err = err./sqrt(obj.SDCell.Ns*prod(obj.Mesh.N));
        end
        
        function PrintSummary(obj)
        %PrintSummary Display a summary of the case on the standard input.

            fprintf('---- 2D SD Computational Case (Standard cell) summary:\n');
            fprintf('Equation: %s\n',obj.EqnName);
            fprintf('Computational case: %s\n',obj.CaseName);
            fprintf('Dimension d=%d\n',obj.d);
            fprintf('No. of conservative var. Nq=%d\n',obj.Nq);
            fprintf('No. of iter done %d, t=%1.2g\n',obj.getIterationNumber(),obj.getElapsedTime());
            fprintf('--------------------------------\n');
        end

        function nit=getIterationNumber(obj)
        %getIterationNumber Return the number of iterations performed.
        % Output:
        %   nit (1x1) number of iterations performed (>=0)

            nit = length(obj.t)-1;

        end

        function DeltaT=getElapsedTime(obj)
        %getElapsedTime Return the elapsed time.
        % Output:
        %   DeltaT (1x1) elapsed time

            DeltaT = obj.t(end)-obj.t(1);

        end

        function CheckNormalFluxContinuity(obj)
        % Check normal flux continuity at face flux points.
        % (This is enough to ensure conservativity is the cell is
        % appropriately designed.)
        % (Debugging purposes mainly.)
        
            iscont = true;
            Ny = obj.Mesh.N(2);
            for i=1:size(obj.Mesh.Face_top_bound,1)
                    % Get top and bottom cells (k,l) coordinates
                kb = obj.Mesh.Face_top_bound(i,1);
                lb = obj.Mesh.Face_top_bound(i,2);
                kt=kb; lt=Ny;
                iscont = iscont && (obj.FYxfy(obj.mfy_t(kb,lb),obj.y_idx)==obj.FYxfy(obj.mfy_b(kt,lt),obj.y_idx));
            end
            for i=1:size(obj.Mesh.Face_right_bound,1)
                kl=obj.Mesh.Face_right_bound(i,1);
                ll=obj.Mesh.Face_right_bound(i,2);
                kr=1; lr=ll;
                iscont = iscont && (obj.FXxfx(obj.mfx_l(kr,lr),obj.x_idx) == obj.FXxfx(obj.mfx_r(kl,ll),obj.x_idx));
            end
            for i=1:size(obj.Mesh.Face_vert_int,1) % loop over every vertical interior face
                kl=obj.Mesh.Face_vert_int(i,1);
                ll=obj.Mesh.Face_vert_int(i,2);
                kr=obj.Mesh.Face_vert_int(i,3);
                lr=obj.Mesh.Face_vert_int(i,4);
                iscont = iscont && (obj.FXxfx(obj.mfx_l(kr,lr),obj.x_idx) == obj.FXxfx(obj.mfx_r(kl,ll),obj.x_idx));
            end
                % Horizontal faces
                % ('normal flux' = y-flux)
            for i=1:size(obj.Mesh.Face_hor_int,1) % loop over every horizontal interior face
                kb=obj.Mesh.Face_hor_int(i,1);
                lb=obj.Mesh.Face_hor_int(i,2);
                kt=obj.Mesh.Face_hor_int(i,3);
                lt=obj.Mesh.Face_hor_int(i,4);
                iscont = iscont && (obj.FYxfy(obj.mfy_b(kt,lt),obj.y_idx) == obj.FYxfy(obj.mfy_t(kb,lb),obj.y_idx));
            end
        end

    end
end