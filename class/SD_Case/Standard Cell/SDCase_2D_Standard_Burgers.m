classdef SDCase_2D_Standard_Burgers < SDCase_2D_Standard
    %SDCase_2D_Standard_Burgers Implements the 2D scalar Burgers
    %equation.
    % This is simply an implementation of the abstract methods defined in
    % SDCase_2D_Standard.
    
    properties (SetAccess = protected)
        d=2;
        Nq=1;
        EqnName='Burgers 2D';

        x_idx = [1];
        y_idx = [1];  
    end
    
    methods (Access = public)
        function Case = SDCase_2D_Standard_Burgers(q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter)
        % (ref. constructor of SDCase_2D_Standard.)
        Case = Case@SDCase_2D_Standard(q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter);
        end
    end
    
    methods (Access = protected)
        function U_max = GetMaxSpeed(obj)
            U_max = max(obj.Qxs)*ones(2,1);
        end
        
        function Fx = xFlux(obj,Q)
            Fx = 0.5*(Q.^2);
        end
        
        function Fy = yFlux(obj,Q)
            Fy = 0.5*(Q.^2);
        end
        
        function F = RiemannSolver1D(obj,Ql,Qr,n)
        %RiemannSolver1D Burgers equation.
        % Expression from « Introduction a la resolution numerique des
        %equations d'Euler » by J.M. Moschetta (Supaero,1997).
        % Input:
        %   Ql (Nx1) left speed
        %   Qr (Nx1) right speed
        % Output:
        %   F (Nx1) flux vector
            F = 0.5*max(min(Qr,0).^2,max(Ql,0).^2); 
        end 
    end
    
    methods (Access = public)
    end
end

