classdef SDCase_2D_Standard_LinearTransport < SDCase_2D_Standard
    %SDCase_2D_Standard_LinearTransport Linear version of
    %SDCase_2D_Standard : all operations are performed through matrix
    %operations.
    
    properties (SetAccess = protected)
        d=2;
        Nq=1;
        EqnName='Transport 2D linear';
        V; % (2x1) [Vx; Vy] propagation speed
        
        x_idx = [1];
        y_idx = [1];
        
        %-- Global matrices
            % Flux computation
            % (usage: FXxfx = Qxfx * Fx')
            % (Fx follows the natural definition x-flux = Fx*Q, where <Q>
            % is (Nqx1)).
        Fx; % (NqxNq)
        Fy; % (NqxNq)
        
            % Riemann Solver
            % (usage: Qxfx = Rx*Qxfx)
        Rx; % (Ncell*Nf x Ncell*Nf)
        Ry; % (Ncell*Nf x Ncell*Nf)
        
            % Divergence w.r.t global coordinates
            % (usage: div(F(Q)) = div x CQ) (and NOT divxQ !)
        div; % (Ncell*NsxNcell*Ns)
        
    end
    
    methods (Access = public)
        function Case = SDCase_2D_Standard_LinearTransport(V,q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter)
        % (ref. SDCase_2D_Standard for details) 
        % Additional input:
        %   V (2x1) Propagation speed
            Case = Case@SDCase_2D_Standard(q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter);
            
            validateattributes(V,{'numeric'},{'2d','size',[2,1]});    
            Case.V = zeros(2,1);
            Case.V(1) = V(1);
            Case.V(2) = V(2);
            
            Ncell = prod(Case.Mesh.N);
            Ns = Case.SDCell.Ns;
            Nf = Case.SDCell.Nf;
            
            % -- Build the global matrices & check size
                % Flux computation
            Case.Fx = V(1);
            Case.Fy = V(2);
            validateattributes(Case.Fx,{'numeric'},{'2d','size',[Case.Nq,Case.Nq]});
            validateattributes(Case.Fy,{'numeric'},{'2d','size',[Case.Nq,Case.Nq]});
            
                % Riemann Solver
            [Case.Rx,Case.Ry] = Case.buildRiemannMatrix();
            validateattributes(Case.Rx,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Nf]});
            validateattributes(Case.Ry,{'numeric'},{'2d','size',[Ncell*Nf,Ncell*Nf]});
            
                % Divergence
                % (local!= global)
            dx=Case.Mesh.cell_size(1);
            dy=Case.Mesh.cell_size(2);
            Case.div = ((2/dx)*Case.Dx1Lfx_xs*Case.iLfx_xfx*Case.Fx*Case.Rx*Case.Ls_xfx + ...
                        (2/dy)*Case.Dx2Lfy_xs*Case.iLfy_xfy*Case.Fy*Case.Ry*Case.Ls_xfy);
            validateattributes(Case.div,{'numeric'},{'2d','size',[Ncell*Ns,Ncell*Ns]});
            
        end
    end
    
    methods (Access = protected)
        
        function U_max = GetMaxSpeed(obj)
            U_max = obj.V;
        end
        
        function Fx = xFlux(obj,Q)
        % Not used here.
        end
        function Fy = yFlux(obj,Q)
        % Not used here.
        end
        
        function F = RiemannSolver1D(obj,Ql,Qr,n)
        % Not used here
        end
        
        function [Rx,Ry] = buildRiemannMatrix(obj)
        %buildRiemannMatrix
        % Output:
        %   Rx & Ry  (Ncell*NfxNcell*Nf)
        
            Nx=obj.Mesh.N(1); Ny=obj.Mesh.N(2);
            nr = prod(obj.Mesh.N)*obj.SDCell.Nf;
            Rx = sparse(1:nr,1:nr,1); % sparse identity matrix
            Ry = sparse(1:nr,1:nr,1);
            nx = [1;0]; % normal vector, vertical face
            ny = [0;1]; % normal vector, horizontal face

            % Top boundary: Riemann with bottom boundary
            % (y-Flux only)
            n = ny; % face normal
            for i=1:size(obj.Mesh.Face_top_bound,1)
                    % Get top and bottom cells (k,l) coordinates
                kb = obj.Mesh.Face_top_bound(i,1);
                lb = obj.Mesh.Face_top_bound(i,2);
                kt=kb; lt=Ny;
                    % iR & iL may be vectors !
                    % (but should be of the same size, otherwise, we are in
                    %trouble.)
                iL = obj.mfy_t(kb,lb); % left state indexes
                iR = obj.mfy_b(kt,lt); % right state indexes
                    % Update Ry from the sign of Vn (normal speed)
                Vn = dot(obj.V,n);
                if Vn>0
                    for k=1:length(iR)
                        Ry(iR(k),iR(k)) = 0;
                        Ry(iR(k),iL(k)) = 1;
                    end
                elseif Vn<0
                    for k=1:length(iR)
                        Ry(iL(k),iL(k)) = 0;
                        Ry(iL(k),iR(k)) = 1;    
                    end
                end
            end

            % Right boundary: Riemann with left boundary
            % (x-Flux only)
            n = nx;
            for i=1:size(obj.Mesh.Face_right_bound,1)
                kl=obj.Mesh.Face_right_bound(i,1);
                ll=obj.Mesh.Face_right_bound(i,2);
                kr=1; lr=ll;
                iL = obj.mfx_r(kl,ll);
                iR = obj.mfx_l(kr,lr);
                    % Update R from the sign of Vn (normal speed)
                Vn = dot(obj.V,n);
                if Vn>0
                    for k=1:length(iR)
                        Rx(iR(k),iR(k)) = 0;
                        Rx(iR(k),iL(k)) = 1;
                    end
                elseif Vn<0
                    for k=1:length(iR)
                        Rx(iL(k),iL(k)) = 0;
                        Rx(iL(k),iR(k)) = 1;
                    end
                end
            end

            % Interior vertical faces
            % (x-flux only)
            n = nx;
            for i=1:size(obj.Mesh.Face_vert_int,1) % loop over every vertical interior face
                kl=obj.Mesh.Face_vert_int(i,1);
                ll=obj.Mesh.Face_vert_int(i,2);
                kr=obj.Mesh.Face_vert_int(i,3);
                lr=obj.Mesh.Face_vert_int(i,4);
                iL=obj.mfx_r(kl,ll);
                iR=obj.mfx_l(kr,lr);
                    % Update R from the sign of Vn (normal speed)
                Vn = dot(obj.V,n);
                if Vn>0
                    for k=1:length(iR)
                        Rx(iR(k),iR(k)) = 0;
                        Rx(iR(k),iL(k)) = 1;
                    end
                elseif Vn<0
                    for k=1:length(iR)
                        Rx(iL(k),iL(k)) = 0;
                        Rx(iL(k),iR(k)) = 1;
                    end
                end
            end
            
            % Horizontal faces
            % (y-flux only)
            n = ny;
            for i=1:size(obj.Mesh.Face_hor_int,1) % loop over every horizontal interior face
                kb=obj.Mesh.Face_hor_int(i,1);
                lb=obj.Mesh.Face_hor_int(i,2);
                kt=obj.Mesh.Face_hor_int(i,3);
                lt=obj.Mesh.Face_hor_int(i,4);
                iL=obj.mfy_t(kb,lb);
                iR=obj.mfy_b(kt,lt);
                    % Update R from the sign of Vn (normal speed)
                Vn = dot(obj.V,n);
                if Vn>0
                    for k=1:length(iR)
                        Ry(iR(k),iR(k)) = 0;
                        Ry(iR(k),iL(k)) = 1;
                    end
                elseif Vn<0
                    for k=1:length(iR)
                        Ry(iL(k),iL(k)) = 0;
                        Ry(iL(k),iR(k)) = 1;
                    end
                end
            end
        end
    end

    methods (Access = public)
        function obj = Iterate(obj,Deltat)
         %Iterate Iterates to advance time by Deltat.
         % Inputs:
         %  Deltat (1x1) time advance
         %  CQ, Qxs
         % Outputs:
         %  CQ (N new arrays) (more arrays can be added due to
         %  pre-allocation)
         %  t (N new values)
         %  Qxs
         
            validateattributes(Deltat,{'numeric'},{'positive','nonzero'});
            
                % temp values for the 3rd order R-K scheme
                % (two temporary values are needed)
            Qt = zeros(size(obj.Qxs,1), size(obj.Qxs,2),2);

            Nx=obj.Mesh.N(1);
            Ny=obj.Mesh.N(2);
            Nf=obj.SDCell.Nf;
            Ns=obj.SDCell.Ns;

                % Get estimate of niter
            obj.dt = obj.GetTimeStep();
            niter = ceil(Deltat/obj.dt);
                % pre-allocate niter matrix
            obj.CQ(:,:,end+niter) = zeros(Nx*Ny*Ns,obj.Nq);
            
                % Maximum time-step computed from:
                %   CFL number (given by the user)
                %   Maximum propagation speed (computed from the current iter.)
                % first iteration to be performed is n°<siter> (>=1)
            siter = obj.getIterationNumber()+1;
            n=-1;
            while((obj.t(end)-obj.t(siter))<=Deltat)
                n=n+1;
                % Marker & Limiter on previous iteration
                if(obj.Marker==1)
                    [k_tr,l_tr] = obj.PerformMarking(obj.Qxs);
                    obj.k_tr{siter+n} = k_tr;
                    obj.l_tr{siter+n} = l_tr;
                    %obj.DisplayTroubledCells(1,n);
                else % no troubled cells
                    obj.k_tr{siter+n} = [];
                    obj.l_tr{siter+n} = [];
                end
                if(obj.Limiter==1)
                   obj.Qxs=obj.PerformLimiting(obj.Qxs,[obj.k_tr{siter+n},obj.l_tr{siter+n}]);
                   obj.CQ(:,:,siter+n)=obj.iLs_xs*obj.Qxs;
                end
                        % Update time step
                obj.dt = obj.GetTimeStep();
                fprintf('[Iterate] Starting iteration n°%d. Remaining (estimate): %d...\n',n+siter,ceil((Deltat+obj.t(siter)-obj.t(end))/obj.dt));
                
                    % -- Compute new Qxs
                        % Euler 1st order
                %obj.Qxs = obj.Qxs -obj.dt*obj.div*obj.CQ(:,:,siter+n);
                        % TVD 3-rd order R-K time integration
                Qt(:,:,1) = obj.Qxs - obj.dt*obj.div*obj.CQ(:,:,siter+n);
                Qt(:,:,2) = (3/4)*obj.Qxs+(1/4)*Qt(:,:,1)-(1/4)*obj.dt*obj.div*obj.iLs_xs*Qt(:,:,1);
                obj.Qxs=(1/3)*obj.Qxs+(2/3)*Qt(:,:,2)-(2/3)*obj.dt*obj.div*obj.iLs_xs*Qt(:,:,2);

                    % -- Sync <t> and <CQ> with Q(xs)
                obj.CQ(:,:,siter+n+1)=obj.iLs_xs*obj.Qxs;
                obj.t(end+1) = obj.t(end) + obj.dt;
            end
            clear Qt
        end
    end
end
