classdef SDCase_2D_Standard_Euler < SDCase_2D_Standard
    %SDCase_2D_Standard_Euler Implements the 2D Euler equations.
    % This is simply an implementation of the abstract methods defined in
    % SDCase_2D_Standard.
    
    properties (SetAccess = protected)
        d=2;
        Nq=4;
        EqnName='Euler 2D';
        gamma = 1.4; % heat capacity ratio
        x_idx = [1,2,4];
        y_idx = [1,3,4];  
    end
    
    methods (Access = public)
        function Case = SDCase_2D_Standard_Euler(q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter)
        % (ref. constructor of SDCase_2D_Standard.)
            Case = Case@SDCase_2D_Standard(q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter);
        end
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Implementaton of SDCase_2D_Standard abstract methods.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    methods (Access = protected)
        function U_max = GetMaxSpeed(obj)
            U_max = max([obj.Qxs(:,2)./obj.Qxs(:,1), obj.Qxs(:,3)./obj.Qxs(:,1)]);
        end
        
        function Fx = xFlux(obj,Q)
            Fx(:,1) = [Q(:,2)];
            Fx(:,2) = [(Q(:,2).^2)./Q(:,1) + obj.computePressure(Q,obj.gamma)];
            Fx(:,3) = [(Q(:,2).*Q(:,3))./Q(:,1)];
            Fx(:,4) = [(Q(:,2)./Q(:,1)).*obj.computeH(Q,obj.gamma)];
        end
        
        function Fy = yFlux(obj,Q)
            Fy(:,1) = [Q(:,3)];
            Fy(:,2) = [(Q(:,2).*Q(:,3))./Q(:,1)];
            Fy(:,3) = [(Q(:,3).^2)./Q(:,1) + obj.computePressure(Q,obj.gamma)];
            Fy(:,4) = [(Q(:,3)./Q(:,1)).*obj.computeH(Q,obj.gamma)];
        end
        
        function F = RiemannSolver1D(obj,Ql,Qr,n)
        %RiemannSolver1D Burgers equation.
        % Expression from « Introduction a la resolution numerique des
        %equations d'Euler » by J.M. Moschetta (Supaero,1997).
        % Input:
        %   Ql (Nx(Nq-1)) left speed [rho,rho*vn,rho*E]_L
        %   Qr (Nx(Nq-1)) right speed [rho,rho*vn,rho*E]_R
        % Output:
        %   F (Nx(Nq-1)) flux vector
        
            pl = Ql(:,3) -(1/2)*(1./Ql(:,1)).*(Ql(:,2).^2);
            pl = (obj.gamma-1)*pl;
            pr = Qr(:,3) -(1/2)*(1./Qr(:,1)).*(Qr(:,2).^2);
            pr = (obj.gamma-1)*pr;
            Hl = Ql(:,3)+pl;
            Hr = Qr(:,3)+pr;
            Fl = [Ql(:,2), (Ql(:,2).^2)./Ql(:,1)+pl,(Ql(:,2)./Ql(:,1)).*Hl];
            Fr = [Qr(:,2), (Qr(:,2).^2)./Qr(:,1)+pr,(Qr(:,2)./Qr(:,1)).*Hr];
                    % Higly unefficient
            F = zeros(size(Ql,1),3);
            for i=1:size(Ql,1)
                F(i,:) = Riemann_Roe_Euler( Ql(i,:)', Qr(i,:)', Fl(i,:)', Fr(i,:)', obj.gamma)';
            end
        end
    end
    methods (Access = protected, Static = true)
        function p = computePressure(Q,gamma)
        %computePressure Compute the static pressure from the conservative
        %variables.
        % Inputs:
        %   Q (NxNq) Conservative variables
        %   gamma (1x1) heat capacity ratio
        % Output:
        %   p (Nx1) static pressure
        
            p = Q(:,4);
            %-(1/2)*(1./Q(:,1)).*(Q(:,2).^2+Q(:,3).^2);
            p = (gamma-1)*p;
        end
        
        function [u,v] = computeSpeeds(Q)
        %computeSpeeds Compute the x and y speed from the conservative
        %variables.
        % Inputs:
        %   Q (NxNq) Conservative variables
        % Output:
        %   [u,v] (Nx2) static pressure
            u = Q(:,2)./Q(:,1);
            v = Q(:,3)./Q(:,1);
        end
        
        function H = computeH(Q,gamma)
        %computeH Compute the total enthalpy density.
        % Input:
        %   Q (NxNq) conservative variable
        % Output:
        %   H (Nx1) total enthalpy density (rho*H, J/m^3)
            H = Q(:,4) + SDCase_2D_Standard_Euler.computePressure(Q,gamma);
        end
    end
    
    methods (Access = public)
    end
end

