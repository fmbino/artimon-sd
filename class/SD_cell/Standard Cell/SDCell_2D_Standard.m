classdef SDCell_2D_Standard
    %SDCell_2D_Standard 2D Rectangular Spectral Difference (SD) cell.
    % This class implements a "standard" 2D rectangular SD cell. (Any
    %quadrilateral cell can be mapped to such a cell.)
    %
    % In this cell type, horizontal & vertical flux are treated separately.
    % Each flux uses a tensorial polynomial basis.
    % Example : For a cell of order 2, the solution basis is :
    %                           [1 x] (x) [1 y] (4 pol.)
    %           the x-flux basis :
    %                           [1 x x^2] (x) [1 y] (6 pol.)
    %           the y-flux basis :
    %                           [1 x] (x) [1 y y^2]
    % with '(x)' denoting the tensor product.
    % The x-flux, y-flux & solution points distribution can be chosen
    % freely, though Nfx = Nfy > Ns is enforced (Nfx, Nfy & Ns values are
    % function of the cell order, see below).
    %
    % Usage:
    %   (ref. constructor description.)
    % Main properties:
    %   - The cell coordinates are named x1 & x2 ("local coordinates").
    %They span over [-1,1]x[-1,1] : 
    %       - x1 grows from left to right
    %       - x2 grows from bottom to top
    %       - The origin (x1,x2)=(0,0) is at the cell center.
    %   - The cell is entirely defined by:
    %       * Order of the solution reconstruction (<order>). (The flux
    %       reconstruction order is <order+1>).
    %       * Coordinates of the <Ns> solutions points (<xs>) : 
    %                   <Ns> = <order> x <order>
    %       * Coordinates of the <Nfx> x-flux points (<xfx>) :
    %                   <Nfx> = <order> x <order+1>
    %       * Coordinates of the <Nfy> y-flux points (<xfy>) :
    %                   <Nfy> = <order> x <order+1>
    %       * Polynomial basis for solution/flux reconstruction. (<Ns>,
    %       <Nfx> & <Nfy> polynomials, respectively.)
    %   - To define the solution/flux points coordinates, the user
    %provides the <Ns>/<Nfx>/<Nfy> coordinates of the solution and
    %flux points. (Any point distribution can be defined.)
    %       
    % REMARK: Every "grid" variable (i.e. matrix of coordinates, like the
    %one used by meshgrid) follows the 
    %"top-left (1,1)" -> "bottom-right (end,end)" convention.
    
    properties (SetAccess = protected)
        name = '2D Standard'; % (string) SDCell name (display purp.)
            % (cell of string, 1x?) names of the implemented polynomial 
            % basis. Only one available here.
        implementedBasis = {'TensorProductBasis'}; 
        
            %-- General scalar parameter
        d=2; % (1x1) spatial dimeNsdon.
        order; % (1x1) <p+1> is the target order for the numerical scheme
        Ns; % (1x1) Number of solutions points
        Nf; % (1x1) Number of flux points (horizontal & vertical)
            % Beware: Nfx = Nf & Nfy = Nf, hence the total number of flux
            % points is 2*Nf !
        
            %-- Solution points coordinates
        xs_c; % (Nsx2) solution points x1 and x2 coordinates: [x1,x2]
              % (column style: 'c')
        idx_xs_t; % (?x1) <xs_c(idx_top,:)>: top solution points coordinates
        idx_xs_b;
        idx_xs_l;
        idx_xs_r;
        idx_xs_int; % interior
            %-- x-flux points coordinates
            % ! No x-flux points on the top or bottom boundary.
        xfx_c; % (Nfx2) flux points coordinates: [x1,x2] (column style)
        idx_xfx_l; % (?x1) <xfx_c(idx_top,:)>: top flux points coordinates
        idx_xfx_r;
        idx_xfx_int; % interior
            %-- y-flux points coordinates
            % ! No y-flux points on the left or bottom boundary.
        xfy_c; % (Nfx2) flux points coordinates: [x1,x2] (column style)
        idx_xfy_t; % (?x1) <xfy_c(idx_top,:)>: top flux points coordinates
        idx_xfy_b;
        idx_xfy_int; % interior
        
            %-- Local polynomial matrices "L"
        basis; % (string) name of the polynomial basis (sol. & flux.)
                %-- Solution polynomial
        Ls_xs; % (NsxNs) @ solution points
        Ls_xfx; % (NfxNs) @ x-flux points
        Ls_xfy; % (NfxNs) @ y-flux points
        Ls_xsd; % (prod(Nsd)xNs) @ solution points (sol. display grid)
                %-- Solution polynomial mean values (& derivative thereof)
        MLs; % (1xNs) MLs*CQ is the mean value over the cell
        MDx1Ls; % (1xNs) mean value of d/dx1
        MDx2Ls; % (1xNs) mean value of d/dx2
        MD2x1Ls; % (1xNs) mean value of d^2/dx1^2
        MD2x2Ls; % (1xNs) mean value of d^2/dx2^2
                %-- x-flux points
        Lfx_xfx; % (NfxNf) @ x-flux points
        Lfx_xs; % (NsxNf) @ solution points
        Dx1Lfx_xs; % (NsxNf) x1-derivative @ solution points 
                %-- y-flux points
        Lfy_xfy; % (NfxNf) @ y-flux points
        Lfy_xs; % (NsxNf) @ solution points
        Dx2Lfy_xs; % (NsxNf) x2-derivative @ solution points 
        
             %-- display grid
        % (For display purposes only, one need to define a rectangular 
        %grid on the SD cell. These variables implement this.)
        % The display grids contain:
        %   - user-defined points (sol. or flux.)
        %   - necessary additionnal points to construct a rectangular
        %grid.
        Nsd; % (1x2) [Nx1,Nx2] Number of points on the solution display
             %grid on each axis.
        xsd_c; % (prod(Nsd)xd) sol. display points
        xsd_g; % (Nsd(2)xNsd(1)xd) x1 and x2 coordinates of the
               %display grid for the solution. (grid style: 'g')
               % Grid version of <xsd_c>.
               % ! Top-left -> bottom-right convention
    end
    
    methods(Access = protected)
        
        function Ls_x = PolynomialMatrix_Sol(obj,x,d,mean)
        %PolynomialMatrix_Sol Builds polynomial matrix Ls(x) for the
        %solution.
        % Input:
        %   x (Nxd) points coordinates. d = 1 (1D) or 2 (2D)
        % Optional inputs:
        %   d (1x2) [dx1,dx2] order of derivation in x1 & x2 (local cord)
        %   mean (string) 'mean' returns the mean values
        % Output:
        %   Ls_x (NxNs) Polynomial matrix.
        %               Ls_x = [L1(x1) .. LNs(x1); 
        %                       L1(x2) .. LNs(x2);
        %                            ...          ]
        %               Li: i-th basis polynomial (Ns in total)
        %
        % Usage:    
        %   Q(x) = Ls(x) x C, where:
        %       Q(x) = [q1(x1)...; q1(x2)...;]
        %       C = [C1, C2, ...] (polynomial coefficients)
            if(strcmp(obj.basis,'TensorProductBasis'))
                if nargin<3
                    d = [0,0]; % no derivation
                end
                if nargin<4
                    mean=''; % no mean values
                end
                Ls_x = PolMat_TensorProduct(x,[obj.order-1,obj.order-1],d,mean);
            end
        end
        
        function Lf_x = PolynomialMatrix_Flux(obj,x,f,deriv)
        %PolynomialMatrix_Flux Builds polynomial matrix Lf(x) for a flux
        %vector.
        % Same as <PolynomialMatrix_Sol>, but with <Nf> polynomial instead
        % of <Ns>.
        % Additional arguments :
        %   f (string)
        %       'fx' Lf(x) for x-flux will be returned.
        %       'fy' Lf(x) for y-flux will be returned.
        % Optional argument:
        %   deriv (string) 
        %       'dx': x-derivative of Lf(x) will be returned.
        %       'dy': y-derivative of Lf(x) will be returned.
            validateattributes(f,{'char'},{'nonempty'});
            if (strcmp(f,'fx'))
                degX = obj.order;
                degY = obj.order-1;
            elseif (strcmp(f,'fy'))
                degX = obj.order-1;
                degY = obj.order;
            else
                error('Invalid f argument : ''fx'' or ''fy'' expected.');
            end
            if(strcmp(obj.basis,'TensorProductBasis'))
                if(nargin>3)
		    deriv = [1,0]*(strcmp(deriv,'dx')) + [0,1]*(strcmp(deriv,'dy'));
                    Lf_x = PolMat_TensorProduct(x,[degX,degY],deriv);
                else
                    Lf_x = PolMat_TensorProduct(x,[degX,degY]);
                end
            end
        end
    end
    
    methods (Access=public)
        function cell = SDCell_2D_Standard(xs,xfx,xfy,order)
        % Class Constructor.
        % Inputs:
        %   xs (Nsx2) solution points x1 and x2 coordinates: [x1,x2] 
        %   xfx (Nfx2) x-flux points x1 and x2 coordinates
        %   xfy (Nfx2) y-flux points x1 and x2 coordinates
        %   order (scalar) target order for the cell
        
                % -- Arguments tests
            validateattributes(order,{'numeric'},{'scalar','>=',1});
            validateattributes(xs,{'numeric'},{'2d','<=',1,'>=',-1,'size',[order^2,2]});
            validateattributes(xfx,{'numeric'},{'2d','<=',1,'>=',-1,'size',[order*(order+1),2]});
            validateattributes(xfy,{'numeric'},{'2d','<=',1,'>=',-1,'size',[order*(order+1),2]});

                % -- Ensure no duplicates values have been given
            if(checkDuplicateLines(xs))
                warning('[SDCell]: <xs> has duplicate lines.');
            elseif(checkDuplicateLines(xfx))
                warning('[SDCell]: <xfx> have duplicate lines.');
            elseif(checkDuplicateLines(xfy))
                warning('[SDCell]: <xfy> have duplicate lines.');
            end
                % Fine, we can copy
            cell.xs_c = xs;
            cell.xfx_c = xfx;
            cell.xfy_c = xfy;
            
                % -- Build indexes for xs_c, xfx_c & xfy_c
                % ! Also act as a sanity check on the point distribution !
            cell = cell.buildIndexes();
            cell.order = order;
            cell.Ns = size(xs,1);
            cell.Nf = size(xfx,1);
                
                % -- Create display grid
                % Remove duplicates: get unique coordinates 
                % (x1,x2 separetly)
            x1s=unique(xs(:,1)); x2s=unique(xs(:,2));
                % Sorting the coordinates in ascending order.
            x1s = sort(x1s); x2s = sort(x2s);
            
            cell.Nsd = [length(x1s),length(x2s)];
            
                % Creating display grid (with the announced convention)
            cell.xsd_g = zeros(cell.Nsd(2),cell.Nsd(1),cell.d);
            cell.xsd_g(:,:,1) = kron(ones(cell.Nsd(2),1),x1s');
            cell.xsd_g(:,:,2) = kron(x2s(end:-1:1),ones(1,cell.Nsd(1))); % ! x2max to x2min
            
                % Create column vector
            cell.xsd_c = cell.reshape_gtc(cell.xsd_g);
                
                %-- Build Polynomial matrices
                % Only one basis here.
            cell.basis = 'TensorProductBasis';
            cell.Ls_xs = cell.PolynomialMatrix_Sol(cell.xs_c);
            cell.Ls_xfx = cell.PolynomialMatrix_Sol(cell.xfx_c);
            cell.Ls_xfy = cell.PolynomialMatrix_Sol(cell.xfy_c);
            cell.Ls_xsd = cell.PolynomialMatrix_Sol(cell.xsd_c);
            cell.MLs = cell.PolynomialMatrix_Sol(cell.xs_c,[0,0],'mean');
            cell.MDx1Ls = cell.PolynomialMatrix_Sol(cell.xs_c,[1,0],'mean');
            cell.MDx2Ls = cell.PolynomialMatrix_Sol(cell.xs_c,[0,1],'mean');
            cell.MD2x1Ls = cell.PolynomialMatrix_Sol(cell.xs_c,[2,0],'mean');
            cell.MD2x2Ls = cell.PolynomialMatrix_Sol(cell.xs_c,[0,2],'mean');
            cell.Lfx_xs = cell.PolynomialMatrix_Flux(cell.xs_c,'fx');
            cell.Lfx_xfx = cell.PolynomialMatrix_Flux(cell.xfx_c,'fx');
            cell.Dx1Lfx_xs = cell.PolynomialMatrix_Flux(cell.xs_c,'fx','dx');
            
            cell.Lfy_xs = cell.PolynomialMatrix_Flux(cell.xs_c,'fy');
            cell.Lfy_xfy = cell.PolynomialMatrix_Flux(cell.xfy_c,'fy');
            cell.Dx2Lfy_xs = cell.PolynomialMatrix_Flux(cell.xs_c,'fy','dy');
            
            validateattributes(cell.Ls_xs,{'numeric'},{'2d','size',[cell.Ns,cell.Ns]});
            validateattributes(cell.Ls_xfx,{'numeric'},{'2d','size',[cell.Nf,cell.Ns]});
            validateattributes(cell.Ls_xfy,{'numeric'},{'2d','size',[cell.Nf,cell.Ns]});
            validateattributes(cell.Ls_xsd,{'numeric'},{'2d','size',[prod(cell.Nsd),cell.Ns]});
            validateattributes(cell.MLs,{'numeric'},{'2d','size',[1,cell.Ns]});
            validateattributes(cell.MDx1Ls,{'numeric'},{'2d','size',[1,cell.Ns]});
            validateattributes(cell.MDx2Ls,{'numeric'},{'2d','size',[1,cell.Ns]});
            validateattributes(cell.MD2x1Ls,{'numeric'},{'2d','size',[1,cell.Ns]});
            validateattributes(cell.MD2x2Ls,{'numeric'},{'2d','size',[1,cell.Ns]});
            
            validateattributes(cell.Lfx_xs,{'numeric'},{'2d','size',[cell.Ns,cell.Nf]});
            validateattributes(cell.Lfx_xfx,{'numeric'},{'2d','size',[cell.Nf,cell.Nf]});
            validateattributes(cell.Dx1Lfx_xs,{'numeric'},{'2d','size',[cell.Ns,cell.Nf]});
            
            validateattributes(cell.Lfy_xs,{'numeric'},{'2d','size',[cell.Ns,cell.Nf]});
            validateattributes(cell.Lfy_xfy,{'numeric'},{'2d','size',[cell.Nf,cell.Nf]});
            validateattributes(cell.Dx2Lfy_xs,{'numeric'},{'2d','size',[cell.Ns,cell.Nf]});
            
        end

        function Display(obj,window,disp)
        %Display Display the cell & the points distribution.
        % Inputs (optional):
        %   window graphic handle. (Display on the current figure
        %   otherwise)
        %   disp (string) if given, display the solution display grid.
        
            if(nargin>1 && ~ishghandle(window))
                figure(window); % put focus on the windows
            end
        
            leg = cell(0);
            boundary = [-1,-1;-1,1;1,1;1,-1;-1,-1];
            eps = 0.04;
            clf
            hold all;
            if nargin>2
               % display grid
                plot(obj.xsd_c(:,1),obj.xsd_c(:,2),'bo','MarkerEdgeColor','b','MarkerSize',20,'LineWidth',2);
                leg{1}=sprintf('Interp. Solution points (prod(Nsd)=%dx%d=%d)',obj.Nsd(1),obj.Nsd(2),prod(obj.Nsd));
            else
                plot(obj.xfx_c(:,1),obj.xfx_c(:,2),'r>','MarkerEdgeColor','k','MarkerSize',20,'MarkerFaceColor','k','LineWidth',2);
                plot(obj.xfy_c(:,1),obj.xfy_c(:,2),'r^','MarkerEdgeColor','r','MarkerSize',20,'MarkerFaceColor','r','LineWidth',2);
                plot(obj.xs_c(:,1),obj.xs_c(:,2),'bo','MarkerEdgeColor','b','MarkerSize',20,'LineWidth',2);
                leg{1}=sprintf('x-Flux points (Nfx1=%d)',obj.Nf);
                leg{2}=sprintf('y-Flux points (Nfx2=%d)',obj.Nf);
                leg{3}=sprintf('Solution points (Ns=%d)',obj.Ns);
            end
            plot(boundary(:,1),boundary(:,2),'k--','LineWidth',2);
            axis([-1-eps,1+eps,-1-eps,1+eps]);
            set(gca, 'DataAspectRatio', [1 1 1]);
            view(0,90);
            legend(leg);
            xlabel('x1');
            ylabel('x2');
            title(sprintf('[%s] Target solution order: %d',obj.name,obj.order));
        end
        
        function PrintSummary(obj)
        %PrintSummary Display a summary of the cell on the standard
        %input.
   
            fprintf('---- 2D SD Standard cell summary:\n');
            fprintf('Name: %s\n',obj.name);
            fprintf('DimeNsdon d=%d\n',obj.d);
            fprintf('Solution: target order=%d, nb. of points Ns=%d\n',obj.order,obj.Ns);
            fprintf('Flux: nb of points Nfx=Nfy=%d -> total : %d\n',obj.Nf,2*obj.Nf);
            fprintf('Polynomial basis used (for both solution and fluxes):''%s''\n',obj.basis);
            fprintf('Polynomial basis implemented:\n');
            for i=1:length(obj.implementedBasis)
                fprintf('\t-%s\n',obj.implementedBasis{i});
            end
            fprintf('No mask avalaible\n');
            fprintf('Number of solution points on the:\n');
            fprintf('\t-left cell boundary: %d\n',length(obj.idx_xs_l));
            fprintf('\t-right cell boundary: %d\n',length(obj.idx_xs_r));
            fprintf('\t-top cell boundary: %d\n',length(obj.idx_xs_t));
            fprintf('\t-bottom cell boundary: %d\n',length(obj.idx_xs_b));
            fprintf('\t-cell interior: %d\n',length(obj.idx_xs_int));
            fprintf('Number of x-flux points on the:\n');
            fprintf('\t-left cell boundary: %d\n',length(obj.idx_xfx_l));
            fprintf('\t-right cell boundary: %d\n',length(obj.idx_xfx_r));
            fprintf('\t-cell interior: %d\n',length(obj.idx_xfx_int));
            fprintf('Number of y-flux points on the:\n');
            fprintf('\t-top cell boundary: %d\n',length(obj.idx_xfy_t));
            fprintf('\t-bottom cell boundary: %d\n',length(obj.idx_xfy_b));
            fprintf('\t-cell interior: %d\n',length(obj.idx_xfy_int));
            fprintf('Display grid:\n');
            fprintf('\tSolution grid: %d points (x1-axis), %d points (x2-axis)\n',obj.Nsd(1),obj.Nsd(2));
            fprintf('\tNo display grid for the fluxes\n');
            fprintf('Polynomial matrices condition number:\n');
            fprintf('(* : low condition number needed)\n');
            fprintf('\t*Ls(xs): %2.1e\n',cond(obj.Ls_xs));
            fprintf('\tLs(xfx): %2.1e\n',cond(obj.Ls_xfx));
            fprintf('\tLs(xfy): %2.1e\n',cond(obj.Ls_xfy));
            fprintf('\tLs(xsd): %2.1e\n',cond(obj.Ls_xsd));
            fprintf('\n');
            fprintf('\t*Lfx(xfx): %2.1e\n',cond(obj.Lfx_xfx));
            fprintf('\tLfx(xs): %2.1e\n',cond(obj.Lfx_xs));
            fprintf('\tDx1Lfx(xs): %2.1e\n',cond(obj.Dx1Lfx_xs));
            fprintf('\n');
            fprintf('\t*Lfy(xfy): %2.1e\n',cond(obj.Lfy_xfy));
            fprintf('\tLfy(xs): %2.1e\n',cond(obj.Lfy_xs));
            fprintf('\tDx2Lfy(xs): %2.1e\n',cond(obj.Dx2Lfy_xs));
            fprintf('--------------------------------\n');

        end
        
        function C = getConditionNumber(obj)
        %getConditionNumber Compute and return the condition number of the
        %interpolation polynomial matrices for which a low condition number
        %is paramount.
        % Output:
        %   C(1x3)  Condition numbers of the matrices.
        %           Content:
        %               C(1): Ls(xs)
        %               C(2): Lfx(xfx)
        %               C(3): Lfy(xfy)
        
            C = [cond(obj.Ls_xs),cond(obj.Lfx_xfx),cond(obj.Lfy_xfy)];
        end
        
        function DisplayPol(obj,C,Ptitle,window,f)
        %DisplayPol Displays the polynomial corresponding to the given
        %coefficients <C>. The polynomial basis chosen (solution or flux)
        %is based on the number of coefficients given in <C>.
        % Inputs:
        %   C (Ns/Nf x 1) polynomial coefficients
        % Inputs (optional):
        %   Ptitle (string) plot title
        %   window graphic handle. Display on the current figure
        %   otherwise.
        %   f (string)
        %       'fx' : the x-flux pol. basis is used
        %       'fy' : the y-flux pol. basis is used
        % Output: plot on the current figure.
            
            if(nargin>3 && ~ishghandle(window))
                figure(window); % put focus on the windows
            end
            if(nargin<3)
               Ptitle = 'Reconstructed polynomial';
            else
               validateattributes(Ptitle,{'char'},{'nonempty'}); 
            end
            
                % dense mesh grid generation
            n = 50;
            [xd_g,yd_g]=meshgrid(linspace(-1,1,n),linspace(-1,1,n));
                    % reshape into column vectors
            xd_c = reshape(xd_g,[numel(xd_g) 1]);
            yd_c = reshape(yd_g,[numel(yd_g) 1]);

                % interpolate
            if(nargin<5) % f not provided
                validateattributes(C,{'numeric'},{'2d','size',[obj.Ns,1]});
                zd = obj.Interpolate_Sol([xd_c,yd_c],C);
                zs = obj.Interpolate_Sol(obj.xs_c,C); % height of sol. points
                zfx = obj.Interpolate_Sol(obj.xfx_c,C); % heght of x-flux points
                zfy = obj.Interpolate_Sol(obj.xfy_c,C); % heght of y-flux points
            else
                if strcmp(f,'fx')
                    validateattributes(C,{'numeric'},{'2d','size',[obj.Nf,1]});
                    zd = obj.Interpolate_xFlux([xd_c,yd_c],C);
                    zs = obj.Interpolate_xFlux(obj.xs_c,C); % height of sol. points
                    zfx = obj.Interpolate_xFlux(obj.xfx_c,C); % heght of x-flux points
                    zfy = obj.Interpolate_xFlux(obj.xfy_c,C); % heght of x-flux points
                elseif strcmp(f,'fy')
                    validateattributes(C,{'numeric'},{'2d','size',[obj.Nf,1]});
                    zd = obj.Interpolate_yFlux([xd_c,yd_c],C);
                    zs = obj.Interpolate_yFlux(obj.xs_c,C); % height of sol. points
                    zfx = obj.Interpolate_yFlux(obj.xfx_c,C); % heght of x-flux points
                    zfy = obj.Interpolate_yFlux(obj.xfy_c,C); % heght of y-flux points
                else
                    error('f should be either ''fx'' or ''fy''');
                end
            end
            
            zd = reshape(zd,size(xd_g));
            
                % display
            hold on
            leg = cell(0);
            scatter3(obj.xfy_c(:,1),obj.xfy_c(:,2),zfy,'r','LineWidth',10);
            scatter3(obj.xfx_c(:,1),obj.xfx_c(:,2),zfx,'k','LineWidth',10);
            scatter3(obj.xs_c(:,1),obj.xs_c(:,2),zs,'bo','LineWidth',10);
            surf(xd_g,yd_g,zd); 
            leg{1} = sprintf('y-Flux points (Nfy=%d)',obj.Nf);
            leg{2} = sprintf('x-Flux points (Nfx=%d)',obj.Nf);
            leg{3} = sprintf('Solution points (Ns=%d)',obj.Ns);
            legend(leg);
            title(Ptitle);
        end
            
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interpolation methods (for solution, x-flux & y-flux).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function z=Interpolate_Sol(obj,x,C)
        %Interpolate Compute the values at <x> of the function 
        %corresponding to the given polynomial coefficients <C>.
        %The polynomial basis chosen is the solution polynomial basis.
        % Input:
        %   x (Nxd) points coordinates. d = 1 (1D) or 2 (2D)
        %   C (Nsx 1) polynomial coefficients
        % Output:
        %   z (Nx1) values f(x)
                % arguments check
            validateattributes(C,{'numeric'},{'2d','size',[obj.Ns,1]});
            validateattributes(x,{'numeric'},{'2d','ncols',obj.d});
                % build polynomial matrix
            L_x = obj.PolynomialMatrix_Sol(x);
                % interpolate
            z = L_x*C;
        end
        function z=Interpolate_xFlux(obj,x,C)
        %Interpolate Compute the values at <x> of the function 
        %corresponding to the given polynomial coefficients <C>.
        %The polynomial basis chosen is the x-flux polynomial basis.
        % Input:
        %   x (Nxd) points coordinates. d = 1 (1D) or 2 (2D)
        %   C (Nf(1)x 1) polynomial coefficients
        % Output:
        %   z (Nx1) values f(x)
                % arguments check
            validateattributes(C,{'numeric'},{'2d','size',[obj.Nf,1]});
            validateattributes(x,{'numeric'},{'2d','ncols',obj.d});
                % build polynomial matrix
            L_x = obj.PolynomialMatrix_Flux(x,'fx');
                % interpolate
            z = L_x*C;
        end
        function z=Interpolate_yFlux(obj,x,C)
        %Interpolate Compute the values at <x> of the function 
        %corresponding to the given polynomial coefficients <C>.
        %The polynomial basis chosen is the y-flux polynomial basis.
        % Input:
        %   x (Nxd) points coordinates. d = 1 (1D) or 2 (2D)
        %   C (Nf(1)x 1) polynomial coefficients
        % Output:
        %   z (Nx1) values f(x)
                % arguments check
            validateattributes(C,{'numeric'},{'2d','size',[obj.Nf,1]});
            validateattributes(x,{'numeric'},{'2d','ncols',obj.d});
                % build polynomial matrix
            L_x = obj.PolynomialMatrix_Flux(x,'fy');
                % interpolate
            z = L_x*C;
        end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reshape functions.
% Purpose: to obtain a grid (flux or solution display grid), which can be 
%displayed.
%Using these function ensures that the shape convention used by the class 
%is respected.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function grid = reshape_xsd_ctg(obj,xsd)
        %reshape_s_ctg Reshape the <xsd> column vector to a grid
        %(plot-friendly format).
        %
        %Input:
        %   xsd (prod(Nsd)xm) column vectors, in which the values are 
        %ordered as in <xsd_c>.
        %Output:
        %   grid (Nsd(2)xNsd(1)xm) one grid for each column in <x>.
        %
        %Typically, <xsd> contains a function F(x1,x2) over the
        %display grid for the solution points.
            
            grid = obj.reshape_ctg(xsd,obj.Nsd(2));
        end
    end
    
    methods (Access=protected)
        function obj = buildIndexes(obj)
        %buildIndexes Build the location indexes for <xs_c>, <xfx_c> &
        %<xfy_c>.
        % Once the cell contains <xs_c> and <xf_c>, builds the indexes to 
        %enable access to top, bottom, left, right and interior.
        % This method also performs a sanity check on the position of the
        % solution & flux points.
        % Inputs :
        %   xs_c, xfx_c & xfy_c.
        
                % Tests on x2 (bottom/top)
            obj.idx_xs_t = find(obj.xs_c(:,2)==1); % indices for x2=1
            obj.idx_xs_b = find(obj.xs_c(:,2)==-1);
            idx_xfx_t = find(obj.xfx_c(:,2)==1);
            idx_xfx_b = find(obj.xfx_c(:,2)==-1);
            obj.idx_xfy_t = find(obj.xfy_c(:,2)==1);
            obj.idx_xfy_b = find(obj.xfy_c(:,2)==-1);
                % Tests on x1 (left/right)
            obj.idx_xs_l = find(obj.xs_c(:,1)==-1);
            obj.idx_xs_r = find(obj.xs_c(:,1)==1);
            obj.idx_xfx_l = find(obj.xfx_c(:,1)==-1);
            obj.idx_xfx_r = find(obj.xfx_c(:,1)==1);
            idx_xfy_l = find(obj.xfy_c(:,1)==-1);
            idx_xfy_r = find(obj.xfy_c(:,1)==1);
                % Interior
            obj.idx_xs_int = find(and(and(and(obj.xs_c(:,1)~=-1,obj.xs_c(:,1)~=1),obj.xs_c(:,2)~=-1),obj.xs_c(:,2)~=1));
            obj.idx_xfx_int = find(and(and(and(obj.xfx_c(:,1)~=-1,obj.xfx_c(:,1)~=1),obj.xfx_c(:,2)~=-1),obj.xfx_c(:,2)~=1));
            obj.idx_xfy_int = find(and(and(and(obj.xfy_c(:,1)~=-1,obj.xfy_c(:,1)~=1),obj.xfy_c(:,2)~=-1),obj.xfy_c(:,2)~=1));
            
                % Sanity check on the position of the x-flux
            if ~isempty(idx_xfx_t)
                warning('Some x-Flux points are on the top boundary.');
            end
            if ~isempty(idx_xfx_b)
                warning('Some x-Flux points are on the bottom boundary.');
            end
            if ~isempty(idx_xfy_l)
                warning('Some y-Flux points are on the left boundary.');
            end
            if ~isempty(idx_xfy_r)
                warning('Some y-Flux points are on the right boundary.');
            end
            if ~isempty([obj.idx_xs_t,obj.idx_xs_b,obj.idx_xs_l,obj.idx_xs_r])
                warning('Some solution points are on a cell boundary.');
            end
        end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reshape functions.
% Purpose: to transform a column into a grid and vice-versa.
% Using these function ensures that the shape convention used by the class 
%is respected.
% These are purely utility function, not related to the object (they could 
%be static).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        
        function grid = reshape_ctg(~, x,P)
        %reshape_ctg Reshape the <x> vector into a grid (column to grid).
        % Input:
        %   x (Nxm) column vectors
        %   P (1x1) integer
        % Output:
        %   grid (P x N/P x m) grid, constructed column-by-column.
        % Remark: N must be a multiple of P.

        N = size(x,1);
        m = size(x,2);
            % Check that N/P is an integer
        validateattributes(N/P,{'numeric'},{'integer'});

        grid = zeros(P,N/P,m);
            for i=1:m
                grid(:,:,i)=reshape(x(:,i),[P,N/P]);
            end
        end
        
        function x = reshape_gtc(~, grid)
        %reshape_gtc Does this opposite of <reshape_ctg>.
        %(ref. <reshape_ctg> description.)
        %TODO : update for d general integer.
        
            N = size(grid,1)*size(grid,2);
            m = size(grid,3);
            x = zeros(N,m);
            for i=1:m
                x(:,i) = reshape(grid(:,:,i),[N,1]);
            end
        end
    end
    
end