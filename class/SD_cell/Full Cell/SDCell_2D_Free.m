classdef SDCell_2D_Free < SDCell_2D_Full
    %SDCell_2D_Free Freely defined SD cell. The point distribution can be
    %freely defined.
    %
    % This type of SD cell is characterised by:
    %   - Ns (no constraint) (number of solution points, d: dimensionality)
    %   - Nf (>Ns) (number of flux points, d: dimensionality)
    %   - Solution polynomial basis : tensor product (x) of monomials (Ns
    % polynomials)
    %       1st order 2D: [1](x)[1] (Ns=1)
    %       2nd order 2D: [1;x](x)[1,y] (Ns=2^2=4)
    %       3rd order 2D: [1;x;x^2](x)[1,y,y^2] (Ns=3^2=9)
    %   - Flux polynomial basis : same as for the solution, but with Nf
    %   polynomials.
    %   - Two masks enable the creation of custom polynomial basis (for
    %both the solution and the flux), to suit the chosen point distribution.
    %(ref. constructor for further information)
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation of abstract methods & properties.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (SetAccess = protected)
        
        name = '2D Free';
        implementedBasis = {'TensorProductBasis'};

    end
    
    methods (Access = protected)
       
        function Ls_x = PolynomialMatrix_Sol(obj,x)
            if(strcmp(obj.basis,'TensorProductBasis'))
                Ls_x = PolMat_TensorProduct(x,[obj.Ns-1,obj.Ns-1],[0, 0],'',obj.mask_Ps);
            end
        end
        
        function Lf_x = PolynomialMatrix_Flux(obj,x,deriv)
            if(strcmp(obj.basis,'TensorProductBasis'))
                if(nargin>2)
                    deriv = [1,0]*(strcmp(deriv,'dx')) + [0,1]*(strcmp(deriv,'dy')); 
                    Lf_x = PolMat_TensorProduct(x,[obj.Nf-1,obj.Nf-1],deriv,'',obj.mask_Pf);
                else
                    Lf_x = PolMat_TensorProduct(x,[obj.Nf-1,obj.Nf-1],[0,0],'',obj.mask_Pf);
                end
            end
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    methods (Access = public)
        function cell = SDCell_2D_Free(xs,xf,order,mask_Ps,mask_Pf)
        % Class Constructor.
        % Input:
        % xs (Nsx2) solution points x1 and x2 coordinates: [x1,x2] 
        % xf (Nfx2) flux points x1 and x2 coordinates: [x1,x2]
        % order (scalar) target order of accuracy for the num. scheme
        % Mask_Ps (NsxNs) index mask to select only some polynomial 
        %functions in the solution polynomial basis. Contains either '1' 
        %(polynomial fun. included) or '0' (polynomial fun. excluded). 
        %This enables the creation of custom polynomial basis.
        %   If M(i,j)==1, then X^(i-1)Y^(j-1) is in the basis.
        %   If M(i,j)==0, then X^(i-1)Y(j-1) is NOT in the basis.
        % Mask_Pf (NfxNf) mask for flux polynomial basis.
        % Remark : the number of '1' in Mask_Ps must be <Ns>
        %          the number of '1' in Mask_Pf must be <Nf>

                % -- Arguments tests
            validateattributes(order,{'numeric'},{'scalar','>=',1});
            validateattributes(xs,{'numeric'},{'2d','ncols',2,'<=',1,'>=',-1});
            validateattributes(xf,{'numeric'},{'2d','ncols',2,'<=',1,'>=',-1});
            Ns = size(xs,1);
            Nf = size(xf,1);
            validateattributes(mask_Ps,{'numeric'},{'2d','size',[Ns,Ns],'<=',1,'>=',0});
            validateattributes(mask_Pf,{'numeric'},{'2d','size',[Nf,Nf],'<=',1,'>=',0});
            mask_Ps = logical(mask_Ps);
            mask_Pf = logical(mask_Pf);
            if(sum(sum(mask_Ps))~=Ns)
                error('Wrong number of 1 in <Mask_Ps>: %d are given, but Ns=%d are expected.',sum(sum(mask_Ps)),Ns);
            end
            if(sum(sum(mask_Pf))~=Nf)
                error('Wrong number of 1 in <Mask_Pf>: %d are given, but Nf=%d are expected.',sum(sum(mask_Pf)),Nf);
            end
            clear Ns Nf;
                % class construction
            cell = cell@SDCell_2D_Full(xs,xf,order,'TensorProductBasis',mask_Ps,mask_Pf);
        end
    end
    
end

