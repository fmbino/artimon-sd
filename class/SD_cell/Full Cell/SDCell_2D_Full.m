classdef (Abstract) SDCell_2D_Full
    %SDCell_2D_Full 2D Rectangular Spectral Difference (SD) cell.
    % This class implements a "full" 2D rectangular SD cell. (Any
    %quadrilateral cell can be mapped to such a cell.)
    % No asumption is made as to the chosen distribution points or the
    % polynomial basis. Only Nf > Ns is enforced. It is up to each subclass
    %to define these.
    %
    % Usage:
    %   (ref. constructor description.)
    % Main properties:
    %   - The cell coordinates are named x1 & x2 ("local coordinates").
    %They span over [-1,1]x[-1,1] : 
    %       - x1 grows from left to right
    %       - x2 grows from bottom to top
    %       - The origin (x1,x2)=(0,0) is at the cell center.
    %   - The cell is entirely defined by:
    %       * Order of the solution reconstruction (<order>). (The flux
    %       reconstruction order is <order+1>).
    %       * Coordinates of the <Ns> solutions points (<xs>).
    %       * Coordinates of the <Nf>Ns> flux points (<xf>).
    %       * Polynomial basis for solution/flux reconstruction. (<Ns> and
    %       <Nf> polynomials, respectively.)
    %   - To define the solution/flux points coordinates, the user
    %provides the <Ns>/<Nf> coordinates of the solution and
    %flux points. (Any point distribution can be defined.)
    %       
    % REMARK: This element can (and is intented to) be generalized to 1D.
    % Hence, the presence of the "d" parameter. Only minimal adaptation is
    % needed.
    %
    % REMARK: Every "grid" variable (i.e. matrix of coordinates, like the
    %one used by meshgrid) follows the 
    %"top-left (1,1)" -> "bottom-right (end,end)" convention.
       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract methods & properties.
% (To be implemented by each subclass.)
% Rmk: These abstract methods & properties effectively define what is an SD
% cell.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
   properties (Abstract, SetAccess = protected)
       
       name; % (string) SDCell name (display purposes)
       implementedBasis % (cell of string, 1x?) names of implemented 
                        %polynomial basis.

   end
   
   methods (Abstract, Access = protected)
       
        Ls_x = PolynomialMatrix_Sol(obj,x)
        %PolynomialMatrix_Sol Builds polynomial matrix Ls(x) for the
        %solution.
        % Input:
        %   x (Nxd) points coordinates. d = 1 (1D) or 2 (2D)
        % Output:
        %   Ls_x (NxNs) Polynomial matrix.
        %               Ls_x = [L1(x1) .. LNs(x1); 
        %                       L1(x2) .. LNs(x2);
        %                           ...          ]
        %               Li: i-th basis polynomial (Ns in total)
        %
        % Usage:    
        %   Q(x) = Ls(x) x C, where:
        %       Q(x) = [q1(x1)...; q1(x2)...;]
        %       C = [C1, C2, ...] (polynomial coefficients)
        
        Lf_x = PolynomialMatrix_Flux(obj,x,deriv)
        %PolynomialMatrix_Flux Builds polynomial matrix Lf(x) for a flux
        %vector.
        % Same as <PolynomialMatrix_Sol>, but with <Nf> polynomial instead
        % of <Ns>.
        % Optional argument:
        %   deriv (string) 
        %       'dx': x-derivative of Lf(x) will be returned.
        %       'dy': y-derivative of Lf(x) will be returned.
        
       
   end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   properties (SetAccess = protected)
        
            %-- General scalar parameter
        d=2; % (1x1) dimeNsdon.
        order; % (1x1) target order for the numerical scheme
        Ns; % (1x1) Number of solutions points
        Nf; % (1x1) Number of flux points
              
            %-- Solution/Flux points coordinates
        xs_c; % (Nsx2) solution points x1 and x2 coordinates: [x1,x2]
              % (column style: 'c')
        idx_xs_t; % (?x1) <xs_c(idx_top,:)>: top solution points coordinates
        idx_xs_b;
        idx_xs_l;
        idx_xs_r;
        idx_xs_int; % interior
        
        xf_c; % (Nfx2) flux points coordinates: [x1,x2] (column style)
        idx_xf_t; % (?x1) <xf_c(idx_top,:)>: top flux points coordinates
        idx_xf_b;
        idx_xf_l;
        idx_xf_r;
        idx_xf_int; % interior
        
            %-- Local polynomial matrices "L"
            % Index mask for the solution and flux polynomial basis
            % These properties enable the definition of a 'custom' 
            %polynomial basis.
            % It is up to each subclass to define these (or ignore them if
            % they are not needed)
        mask_Ps; % (size defined by the abstract class) (boolean) 
        mask_Pf; % (size defined by the abstract class) (boolean)
        
        basis; % (string) name of the polynomial basis (sol. & flux.)
            % Solution
        Ls_xs; % (NsxNs) @ solution points
        Ls_xf; % (NfxNs) @ flux points
        Ls_xsd; % (prod(Nsd)xNs) @ solution points (sol. display grid)
        Ls_xfd; % (prod(Nfd)xNs) 
            % Flux (same for all the fluxes)
        Lf_xf; % (NfxNf) @ flux points
        Lf_xs; % (NsxNf) @ solution points
        Lf_xfd; % (prod(Nfd)xNf) @ flux points (flux display grid)
        Dx1Lf_xs; % (NsxNf) x1-derivative @ solution points 
        Dx2Lf_xs; % (NsxNf) x2-derivative @ solution poiints
       
                 %-- Display grid
            % (For display purposes only, one need to define a rectangular 
            %grid on the SD cell. These variables implement this.)
            % The display grid contain:
            %   - user-defined points (sol. or flux.)
            %   - necessary additionnal points to construct a rectangular
            %grid.
        Nsd; % (1x2) [Nx1,Nx2] Number of points on the solution display
             %grid on each axis.
        xsd_c; % (prod(Nsd)xd) sol. display points
        xsd_g; % (Nsd(2)xNsd(1)xd) x1 and x2 coordinates of the
               %display grid for the solution. (grid style: 'g')
               % Grid version of <xsd_c>.
               % ! Top-left -> bottom-right convention
        
        Nfd; % (1x2) same as <Nsd>, but for the fluxes.
        xfd_c; % (prod(Nfd)x2) same as <xsd_c>, but for the fluxes.
        xfd_g; % (Nfd(2)xNfd(1)xd) same as <xfd_g>, but for the fluxes.
        
    end
    
    methods (Access = public)
        
        function cell = SDCell_2D_Full(xs,xf,order,PolBasis,mask_Ps,mask_Pf)
        % Class Constructor.
        % Input:
        % xs (Nsx2) solution points x1 and x2 coordinates: [x1,x2] 
        % xf (Nfx2) flux points x1 and x2 coordinates: [x1,x2]
        % order (scalar) order of accuracy for the num. scheme
        % PolBasis (string) name of the desired polynomial basis, checked
        % against <implementedBasis>.
        % mask_Ps index mask (custom solution polynomial basis) (boolean)
        % mask_Pf index mask (custom solution polynomial basis) (boolean)
        % (masks are defined by the subclass, if needed)
        
                % -- Arguments tests
            validateattributes(order,{'numeric'},{'scalar','>=',1});
            validateattributes(xs,{'numeric'},{'2d','<=',1,'>=',-1});
            validateattributes(xf,{'numeric'},{'2d','<=',1,'>=',-1});
            validateattributes(size(xf,1)-size(xs,1),{'numeric'},{'scalar','>',0});
            validateattributes(PolBasis,{'char'},{'nonempty'});
            validateattributes(mask_Ps,{'logical'},{'2d'});
            validateattributes(mask_Pf,{'logical'},{'2d'});
            
                % ENSURE <xs> and <xf> do not contain duplicate values
                % (for (x1,x2) coNsddered together)
            if(checkDuplicateLines(xs))
                warning('[SDCell]: <xs> has duplicate lines.');
            elseif(checkDuplicateLines(xf))
                warning('[SDCell]: <xf> have duplicate lines.');
            end
            
                % --
            cell.mask_Ps = mask_Ps;
            cell.mask_Pf = mask_Pf;
            
            cell.xs_c = xs;
            cell.xf_c = xf;
                % Build indexes for xs_c and xf_c
            cell = cell.buildIndexes();
            cell.order = order;
            cell.Ns = size(xs,1);
            cell.Nf = size(xf,1);
           
                % -- Create display grid
                % Remove duplicates: get unique coordinates (x1,x2 separetly)
            x1s=unique(xs(:,1)); x2s=unique(xs(:,2));
            x1f=unique(xf(:,1)); x2f=unique(xf(:,2));
                % Sorting the coordinates in ascending order.
            x1s = sort(x1s); x2s = sort(x2s); 
            x1f = sort(x1f); x2f = sort(x2f);            
            
            cell.Nsd = [length(x1s),length(x2s)];
            cell.Nfd = [length(x1f),length(x2f)];
            
                % Creating display grid (with the announced convention)
            cell.xsd_g = zeros(cell.Nsd(2),cell.Nsd(1),cell.d);
            cell.xfd_g = zeros(cell.Nfd(2),cell.Nfd(1),cell.d);
            cell.xsd_g(:,:,1) = kron(ones(cell.Nsd(2),1),x1s');
            cell.xsd_g(:,:,2) = kron(x2s(end:-1:1),ones(1,cell.Nsd(1))); % ! x2max to x2min
            cell.xfd_g(:,:,1) = kron(ones(cell.Nfd(2),1),x1f');
            cell.xfd_g(:,:,2) = kron(x2f(end:-1:1),ones(1,cell.Nfd(1))); % ! x2max to x2min
            
                % Create column vector
            cell.xsd_c = cell.reshape_gtc(cell.xsd_g);
            cell.xfd_c = cell.reshape_gtc(cell.xfd_g);
        
                % -- Build polynomial matrices
                
                % Check asked polynomial basis.
            if(sum(strcmp(PolBasis,cell.implementedBasis)))
                cell.basis = PolBasis;
            else
                error('Asked polynomial basis not implemented.');
            end
            cell.Ls_xs = cell.PolynomialMatrix_Sol(cell.xs_c);
            cell.Ls_xf = cell.PolynomialMatrix_Sol(cell.xf_c);
            cell.Ls_xsd = cell.PolynomialMatrix_Sol(cell.xsd_c);
            cell.Ls_xfd = cell.PolynomialMatrix_Sol(cell.xfd_c);

            cell.Lf_xf = cell.PolynomialMatrix_Flux(cell.xf_c);
            cell.Lf_xs = cell.PolynomialMatrix_Flux(cell.xs_c);
            cell.Lf_xfd = cell.PolynomialMatrix_Flux(cell.xfd_c);

            cell.Dx1Lf_xs = cell.PolynomialMatrix_Flux(cell.xs_c,'dx');
            cell.Dx2Lf_xs = cell.PolynomialMatrix_Flux(cell.xs_c,'dy');
            
        end

        function Display(obj,interp)
        %Display Display the cell & the points distribution.
        %Display on the current plot window.
        % interp (string) if given, display the solution and flux
        % display grids.
            leg = cell(0);
            boundary = [-1,-1;-1,1;1,1;1,-1;-1,-1];
            eps = 0.04;
            hold all;
            if nargin>1
               % display grid
                plot(obj.xfd_c(:,1),obj.xfd_c(:,2),'r^','MarkerEdgeColor','r','MarkerSize',20,'MarkerFaceColor','k','LineWidth',2);
                plot(obj.xsd_c(:,1),obj.xsd_c(:,2),'bo','MarkerEdgeColor','b','MarkerSize',20,'LineWidth',2);
                leg{1}=sprintf('Interp. Flux points (prod(Nfd)=%dx%d=%d)',obj.Nfd(1),obj.Nfd(2),prod(obj.Nfd));
                leg{2}=sprintf('Interp. Solution points (prod(Nsd)=%dx%d=%d)',obj.Nsd(1),obj.Nsd(2),prod(obj.Nsd));
            else
                plot(obj.xf_c(:,1),obj.xf_c(:,2),'r^','MarkerEdgeColor','r','MarkerSize',20,'MarkerFaceColor','r','LineWidth',2);
                plot(obj.xs_c(:,1),obj.xs_c(:,2),'bo','MarkerEdgeColor','b','MarkerSize',20,'LineWidth',2);
                leg{1}=sprintf('Flux points (Nf=%d)',obj.Nf);
                leg{2}=sprintf('Solution points (Ns=%d)',obj.Ns);
            end
            plot(boundary(:,1),boundary(:,2),'k--','LineWidth',2);
            axis([-1-eps,1+eps,-1-eps,1+eps]);
            set(gca, 'DataAspectRatio', [1 1 1]);
            view(0,90);
            legend(leg);
            xlabel('x1');
            ylabel('x2');
            title(sprintf('[%s] Target solution order: %d',obj.name,obj.order));
        end
        
        
        function PrintSummary(obj)
        %PrintSummary Display a summary of the object on the standard
        %input.
   
            fprintf('---- 2D SD Rectangular cell summary:\n');
            fprintf('Name: %s\n',obj.name);
            fprintf('DimeNsdon d=%d\n',obj.d);
            fprintf('Solution: target order=%d, nb. of points Ns=%d\n',obj.order,obj.Ns);
            fprintf('Flux: nb of points Nf=%d\n',obj.Nf);
            fprintf('Polynomial basis used (for both solution and fluxes):''%s''\n',obj.basis);
            fprintf('Polynomial basis implemented:\n');
            for i=1:length(obj.implementedBasis)
                fprintf('\t-%s\n',obj.implementedBasis{i});
            end
            fprintf('Solution Polynomial basis - index mask:\n');
            disp(obj.mask_Ps);
            fprintf('Flux Polynomial basis - index mask:\n');
            disp(obj.mask_Pf);
            fprintf('Number of solution points on the:\n');
            fprintf('\t-left cell boundary: %d\n',length(obj.idx_xs_l));
            fprintf('\t-right cell boundary: %d\n',length(obj.idx_xs_r));
            fprintf('\t-top cell boundary: %d\n',length(obj.idx_xs_t));
            fprintf('\t-bottom cell boundary: %d\n',length(obj.idx_xs_b));
            fprintf('\t-cell interior: %d\n',length(obj.idx_xs_int));
            fprintf('Number of flux points on the:\n');
            fprintf('\t-left cell boundary: %d\n',length(obj.idx_xf_l));
            fprintf('\t-right cell boundary: %d\n',length(obj.idx_xf_r));
            fprintf('\t-top cell boundary: %d\n',length(obj.idx_xf_t));
            fprintf('\t-bottom cell boundary: %d\n',length(obj.idx_xf_b));
            fprintf('\t-cell interior: %d\n',length(obj.idx_xf_int));
            fprintf('Display grid (for display purposes only):\n');
            fprintf('\tSolution grid: %d points (x1-axis), %d points (x2-axis)\n',obj.Nsd(1),obj.Nsd(2));
            fprintf('\tFlux grid: %d points (x1-axis), %d points (x2-axis)\n',obj.Nfd(1),obj.Nfd(2));
            fprintf('Polynomial matrices condition number:\n');
            fprintf('\tLs(xs): %2.1e\n',cond(obj.Ls_xs));
            fprintf('\tLs(xf): %2.1e\n',cond(obj.Ls_xf));
            fprintf('\tLs(xsd): %2.1e\n',cond(obj.Ls_xsd));
            fprintf('\tLf(xf): %2.1e\n',cond(obj.Lf_xf));
            fprintf('\tLf(xs): %2.1e\n',cond(obj.Lf_xs));
            fprintf('\tLf(xfd): %2.1e\n',cond(obj.Lf_xfd));
            fprintf('--------------------------------\n');
        end
        
        function C = getConditionNumber(obj)
        %getConditionNumber Compute and return the condition number of the
        %various interpolation polynomial matrices.
        % Output:
        %   C(1x4)  Condition numbers of the matrices.
        %           Content:
        %               C(1): Ls(xs)
        %               C(2): Ls(xf)
        %               C(3): Lf(xs)
        %               C(4): Lf(xf)
        
        C = [cond(obj.Ls_xs),cond(obj.Ls_xf),cond(obj.Lf_xs),...
            cond(obj.Lf_xf)];
        end
        
        function DisplayPol(obj,C,Ptitle)
        %DisplayPol Displays the polynomial corresponding to the given
        %coefficients <C>. The polynomial basis chosen (solution or flux)
        %is based on the number of coefficients given in <C>.
        % Input:
        %   C (Ns or Nf x 1) polynomial coefficients
        %   Ptitle (string) (optional) plot title
        % Output: plot on the current figure.
        
                % arguments check
            validateattributes(C,{'numeric'},{'column','nonempty'});
            if (size(C,1)~=obj.Ns && size(C,1)~=obj.Nf)
                error('<C> size is neither <Ns> nor <Nf>.');
            end
            if(nargin<3)
               Ptitle = 'Reconstructed polynomial';
            else
               validateattributes(Ptitle,{'char'},{'nonempty'}); 
            end
            
                % dense mesh grid generation
            n = 50;
            [xd_g,yd_g]=meshgrid(linspace(-1,1,n),linspace(-1,1,n));
                    % reshape into column vectors
            xd_c = reshape(xd_g,[numel(xd_g) 1]);
            yd_c = reshape(yd_g,[numel(yd_g) 1]);

                % interpolate
            zd = obj.Interpolate([xd_c,yd_c],C);
            zd = reshape(zd,size(xd_g));
            zs = obj.Interpolate(obj.xs_c,C); % height of sol. points
            zf = obj.Interpolate(obj.xf_c,C); % heght of flux points

                % display
            hold on
            leg = cell(0);
            scatter3(obj.xf_c(:,1),obj.xf_c(:,2),zf,'r^','LineWidth',10);
            scatter3(obj.xs_c(:,1),obj.xs_c(:,2),zs,'bo','LineWidth',10);
            surf(xd_g,yd_g,zd); 
            leg{1} = sprintf('Flux points (Nf=%d)',obj.Nf);
            leg{2} = sprintf('Solution points (Ns=%d)',obj.Ns);
            legend(leg);
            title(Ptitle);
        end
        
        function z=Interpolate(obj,x,C)
        %Interpolate Compute the values at <x> of the function 
        %corresponding to the given polynomial coefficients <C>.
        %The polynomial basis chosen (solution or flux) is based on the 
        %number of coefficients given in <C>.
        % Input:
        %   x (Nxd) points coordinates. d = 1 (1D) or 2 (2D)
        %   C (Ns or Nf x 1) polynomial coefficients
        % Output:
        %   z (Nx1) values f(x)
        
                % arguments check
            validateattributes(C,{'numeric'},{'column','nonempty'});
            validateattributes(x,{'numeric'},{'2d','ncols',obj.d});
            
                % build polynomial matrix
            if(size(C,1)==obj.Ns)
                L_x = obj.PolynomialMatrix_Sol(x);
            elseif(size(C,1)==obj.Nf)
                L_x = obj.PolynomialMatrix_Flux(x); 
            else
               error('<C> size is neither <Ns> nor <Nf>.'); 
            end
                % interpolate
            z = L_x*C;
        end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Reshape functions.
% Purpose: to obtain a grid (flux or solution display grid), which can be 
%displayed.
%Using these function ensures that the shape convention used by the class 
%is respected.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function grid = reshape_xsd_ctg(obj,xsd)
        %reshape_s_ctg Reshape the <xsd> column vector to a grid
        %(plot-friendly format).
        %
        %Input:
        %   xsd (prod(Nsd)xm) column vectors, in which the values are 
        %ordered as in <xsd_c>.
        %Output:
        %   grid (Nsd(2)xNsd(1)xm) one grid for each column in <x>.
        %
        %Typically, <xsd> contains a function F(x1,x2) over the
        %display grid for the solution points.
            
            grid = obj.reshape_ctg(xsd,obj.Nsd(2));
        end
        
        function grid = reshape_xfd_ctg(obj,xfd)
        %reshape_xfd_ctg ref. <reshape_xsd_ctg>.
            grid = obj.reshape_ctg(xfd,obj.Nfd(2));
        end
    end
    
    
    
    methods (Access=protected)
        
        function obj = buildIndexes(obj)
        %buildIndexes Build the indexes for <xs_c> and <xf_c>.
        %Once the cell contains <xs_c> and <xf_c>, builds the indexes to 
        %enable access to top, bottom, left, right and interior.
        
                % Tests on x2 (bottom/top)
            obj.idx_xs_t = find(obj.xs_c(:,2)==1); % indices for x2=1
            obj.idx_xs_b = find(obj.xs_c(:,2)==-1);
            obj.idx_xf_t = find(obj.xf_c(:,2)==1);
            obj.idx_xf_b = find(obj.xf_c(:,2)==-1);
                % Tests on x1 (left/right)
            obj.idx_xs_l = find(obj.xs_c(:,1)==-1);
            obj.idx_xs_r = find(obj.xs_c(:,1)==1);
            obj.idx_xf_l = find(obj.xf_c(:,1)==-1);
            obj.idx_xf_r = find(obj.xf_c(:,1)==1);
                % Interior
            obj.idx_xs_int = find(and(and(and(obj.xs_c(:,1)~=-1,obj.xs_c(:,1)~=1),obj.xs_c(:,2)~=-1),obj.xs_c(:,2)~=1));
            obj.idx_xf_int = find(and(and(and(obj.xf_c(:,1)~=-1,obj.xf_c(:,1)~=1),obj.xf_c(:,2)~=-1),obj.xf_c(:,2)~=1));

        end
        function grid = reshape_ctg(~, x,P)
        %reshape_ctg Reshape the <x> vector into a grid (column to grid).
        % Input:
        %   x (Nxm) column vectors
        %   P (1x1) integer
        % Output:
        %   grid (P x N/P x m) grid, constructed column-by-column.
        % Remark: N must be a multiple of P.

        N = size(x,1);
        m = size(x,2);
            % Check that N/P is an integer
        validateattributes(N/P,{'numeric'},{'integer'});

        grid = zeros(P,N/P,m);
            for i=1:m
                grid(:,:,i)=reshape(x(:,i),[P,N/P]);
            end
        end
        
        function x = reshape_gtc(~, grid)
        %reshape_gtc Does this opposite of <reshape_ctg>.
        %(ref. <reshape_ctg> description.)
        %TODO : update for d general integer.
        
            N = size(grid,1)*size(grid,2);
            m = size(grid,3);
            x = zeros(N,m);
            for i=1:m
                x(:,i) = reshape(grid(:,:,i),[N,1]);
            end
        end
        
    end
    
end

