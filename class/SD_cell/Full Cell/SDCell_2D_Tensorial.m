classdef SDCell_2D_Tensorial < SDCell_2D_Full
    %SDCell_2D_Tensorial 'Alternative' SD cell, as described by
    %Abeele in "On the stability and accuracy of the SD method."
    %
    % This type of SD cell is characterised by:
    %   - Ns = (order)^d (number of solution points, d: dimensionality)
    %   - Nf = (order+1)^d (number of flux points, d: dimensionality)
    %   - Solution polynomial basis : tensor product (x) of monomials (Ns
    % polynomials)
    %       1st order 2D: [1](x)[1] (Ns=1)
    %       2nd order 2D: [1;x](x)[1,y] (Ns=2^2=4)
    %       3rd order 2D: [1;x;x^2](x)[1,y,y^2] (Ns=3^2=9)
    %   - Flux polynomial basis : same as for the solution, but with Nf
    %   polynomials.
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation of abstract methods & properties.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties (SetAccess = protected)
        
        name = '2D Tensorial';
        implementedBasis = {'TensorProductBasis'};
    
    end
    
    methods (Access = protected)
       
        function Ls_x = PolynomialMatrix_Sol(obj,x)
            if(strcmp(obj.basis,'TensorProductBasis'))
                Ls_x = PolMat_TensorProduct(x,[obj.order-1,obj.order-1]);
            end
        end
        
        function Lf_x = PolynomialMatrix_Flux(obj,x,deriv)
	    
            if(strcmp(obj.basis,'TensorProductBasis'))
                if(nargin>2)
                    deriv = [1,0]*(strcmp(deriv,'dx')) + [0,1]*(strcmp(deriv,'dy')); 
                    Lf_x = PolMat_TensorProduct(x,obj.order*[1,1],deriv);
                else
                    Lf_x = PolMat_TensorProduct(x,obj.order*[1,1]);
                end
            end
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    methods (Access = public)
        function cell = SDCell_2D_Tensorial(xs,xf,order,PolBasis)
        % Class Constructor.
        % Input:
        % xs (<order>^2x2) solution points x1 and x2 coordinates: [x1,x2] 
        % xf (<order+1>^2x2) flux points x1 and x2 coordinates: [x1,x2]
        % order (scalar) order of accuracy for the num. scheme
        % PolBasis (string) name of the desired polynomial basis.
        %The following polynomial basis are implemented:
        %   -"TensorProductBasis": X^iY^j
        %   -"Lagrangian": 2D Lagrange basis
        
                % -- Arguments tests
            validateattributes(order,{'numeric'},{'scalar','>=',1});
            validateattributes(xs,{'numeric'},{'2d','size',[order^2,2],'<=',1,'>=',-1});
            validateattributes(xf,{'numeric'},{'2d','size',[(order+1)^2,2],'<=',1,'>=',-1});
            validateattributes(PolBasis,{'char'},{'nonempty'});
        
                % further input processing (if needed)
                % ex: generate <xf> and <xc> from input arguments.
            mask_Ps = true;
            mask_Pf = true;
                % class construction
            cell = cell@SDCell_2D_Full(xs,xf,order,PolBasis,mask_Ps,mask_Pf);
            
        end
    end
    
end

