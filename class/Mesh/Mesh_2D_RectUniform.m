classdef Mesh_2D_RectUniform
    %Mesh_2D_RectUniform Uniform 2D rectangular mesh.
    % This class implements a uniform 2D rectangular mesh. The mesh is
    %stored in a structured manner: every cell is identified by two
    %integers (k,l). (ref. properties description.)
    % In addition to coordinates, an additional set of arrays is computed,
    % to enable an easy way to loop over the cell faces.
    % Global coordinates (x,y).
    %
    % Usage:
    %   (ref. constructor description.)
    %
    % Mesh storage convention:
    % Each node is identified by two integers, (k,l).
    % The cell n°(k,l) is the one whose top left node is (k,l).
    % (1,1): top left node.
    % Ex.:
    %   right of (k,l): (k+1,l) 
    %   left of (k,l): (k-1,l) 
    %   BOTTOM of (k,l): (k,l+1)  (beware!)
    %   TOP of (k,l): (k,l-1)  (beware!)

    
    properties

        N; % (2x1) [Nx; Ny] number of CELLS in x & y-direction
        cell_size; % (2x1) [dx; dy]
        bound; % (2x2) mesh boundaries [xmin,xmax; ymin,ymax]
        length; % (2x1) [Lx; Ly] domain length in the x and y-direction
        
        x; % ((Nx+1)x1) NODES x-coordinates, from left to right 
        y; % ((Ny+1)x1) NODES y-coordinates, from TOP to BOTTOM
        
            
        %-- Arrays which describe the "faces" (2D: edge, 1D: point).
        % Each face is identified by one integer.
        % (Intended use: Riemann solver)
            
            % 1) Faces within the domain interior.
            % Array format (.x4): 4-row large. 
            % The i-th row of one of these arrays gives the (k,l)
            % coordinates of the left/right (or bottom/top) cells of the 
            %face n°i.
            %     F(i,:)=[k_left,l_left, k_right, l_right] (vert_int)
            %   or
            %     F(i,:)=[k_bottom,l_bottom, k_top, l_top] (hor_int)
        Face_vert_int; % vertical faces, domain interior
        Face_hor_int; % horizontal faces, domain interior
        
            % 2) Domain boundary (top,bottom,left,right)
            % Format: 2-row wide.
            %   F(i,:) = [k,l]
        Face_top_bound; % [k_bottom,l_bottom]
        Face_bottom_bound; % [k_top,l_top]
        Face_left_bound; % [k_right,l_right]
        Face_right_bound; % [k_left,l_left]
        
    end
    
    methods
        

        function mesh = Mesh_2D_RectUniform(bound, N)
        %Mesh_2D_RectUniform Class constructor.
        %Input:
        %   bound (2x2) mesh boundaries [xmin,xmax; ymin,ymax]
        %   N (2x1) [Nx; Ny] number of CELLS in x & y-direction
        
                % -- Arguments check
            validateattributes(bound,{'numeric'},{'2d','size',[2,2]});
            validateattributes(N,{'numeric'},{'2d','size',[2,1]});
            validateattributes(N(1),{'numeric'},{'integer','positive','nonzero'});
            validateattributes(N(2),{'numeric'},{'integer','positive','nonzero'});
                % ---
            
                % Domain boundaries
            xmin = min(bound(1,:));
            xmax = max(bound(1,:));
            ymin = min(bound(2,:));
            ymax = max(bound(2,:));
                
                % Mesh description parameters
            mesh.N = N;
            mesh.bound = [xmin,xmax; ymin, ymax];
            mesh.length = bound(:,2) - bound(:,1);
            mesh.cell_size = mesh.length./N;
                
                % Mesh nodes coordinates
            mesh.x = zeros(1,mesh.N(1)+1); 
            mesh.y = zeros(1,mesh.N(2)+1);
            mesh.x = linspace(xmin,xmax,mesh.N(1)+1);
            mesh.y = linspace(ymax,ymin,mesh.N(2)+1); % ! ymax to ymin
                
                % Additional "faces arrays"
            mesh = mesh.BuildFacesArrays();
        end
        
        
        
        function Display(obj)
        %Display Display the mesh in a new plot window.
            [X,Y]=meshgrid(obj.x,obj.y);
            mesh(X',Y',zeros(obj.N(1)+1,obj.N(2)+1));
            view(0,90);
            set(gca, 'DataAspectRatio', [1 1 1])
            title(sprintf('2D Mesh Nx=%d, Ny=%d',obj.N(1),obj.N(2)));
            xlabel('x');
            ylabel('y');
            text(obj.x(1),obj.y(1),0,sprintf('(k=%d,l=%d)',1,1),'FontSize',20);
            text(obj.x(end),obj.y(1),0,sprintf('(k=%d,l=%d)',length(obj.x),1),'FontSize',20);
            text(obj.x(1),obj.y(end),0,sprintf('(k=%d,l=%d)',1,length(obj.y)),'FontSize',20);
            text(obj.x(end),obj.y(end),0,sprintf('(k=%d,l=%d)',length(obj.x),length(obj.y)),'FontSize',20);
        end
        
        function PrintSummary(obj)
        %PrintSummary Display an object summary on the standard input.
            
                % Temp variables
            fv_int=length(obj.Face_vert_int);
            fh_int=length(obj.Face_hor_int);
            f_tb = length(obj.Face_top_bound);
            f_bb = length(obj.Face_bottom_bound);
            f_lb = length(obj.Face_left_bound);
            f_rb = length(obj.Face_right_bound);
            
                % Display
            fprintf('---- 2D Uniform Rectangular mesh summary:\n');
            fprintf('Computational domain: [%d,%d]x[%d,%d] ([x]x[y])\n',obj.bound(1,1),obj.bound(1,2),obj.bound(2,1),obj.bound(2,2));
            fprintf('Number of nodes: (%d)x(%d)=%d\n',length(obj.x),length(obj.y),length(obj.x)*length(obj.y));
            fprintf('Number of cells: Nx=%d, Ny=%d, total: Nx x Ny=%d\n',obj.N(1), obj.N(2),obj.N(1)*obj.N(2));
            fprintf('Cell size: dx=%d, dy=%d\n',obj.cell_size(1), obj.cell_size(2));
            fprintf('Mesh faces - Domain interior:\n');
            fprintf('\tVertical faces: %d\n',fv_int);
            fprintf('\tHorizontal faces: %d\n',fh_int);
            fprintf('\t(Total: %d)\n',fv_int+fh_int);
            fprintf('Mesh faces - Domain boundaries:\n');
            fprintf('\tTop boundary: %d\n',f_tb);
            fprintf('\tBottom boundary: %d\n',f_bb);
            fprintf('\tLeft boundary: %d\n',f_lb);
            fprintf('\tRight boundary: %d\n',f_rb);
            fprintf('\t(Total:%d)\n',f_tb+f_bb+f_lb+f_rb);
            fprintf('Total number of faces: %d\n',f_tb+f_bb+f_lb+f_rb+fv_int+fh_int);
            fprintf('------------------\n');
        end

        function obj=BuildFacesArrays(obj)
        %BuildFacesArrays Build faces arrays (ref. fields description)
        % Input: x,y and N.
        % Output: Face_*.
        
        % Remark: It can also be done by looping over nodes and testing
        %coordinates, but this is way slower than this. (As the mesh
        %topology is well known, we already know the face-cell
        %relationships.)
        
        Nx = obj.N(1);
        Ny = obj.N(2);
        Nv_int = Ny*(Nx+1)-2*Ny; % number of interior vertical faces
        Nh_int = Nx*(Ny+1)-2*Nx; % number of interior horizontal faces
        
        obj.Face_vert_int=zeros(Nv_int,4);
        obj.Face_hor_int=zeros(Nh_int,4);
        obj.Face_top_bound=zeros(Nx,2);
        obj.Face_bottom_bound=zeros(Nx,2);
        obj.Face_left_bound=zeros(Ny,2);
        obj.Face_right_bound=zeros(Ny,2);    
            
            % External boundaries : right,left and top/bottom
        obj.Face_right_bound = [Nx*ones(Ny,1), (1:Ny)'];
        obj.Face_left_bound = [ones(Ny,1), (1:Ny)'];
        obj.Face_top_bound = [(1:Nx)', ones(Nx,1)];
        obj.Face_bottom_bound = [(1:Nx)', Ny*ones(Nx,1)];
            
            % Internal faces : vertical and horizontal
        k_l = (1:(Nx-1))'; % k_left
        k_r = k_l+1; % k_right
        k_b = (1:Nx)'; % k_bottom
        id1 = ones(Nx-1,1);
        id2 = ones(Nx,1);
            for l=1:Ny % for each horizontal line
                        % There are <Nx-1> vertical faces.
                obj.Face_vert_int(k_l+(l-1)*(Nx-1),:)=[k_l,l*id1,k_r,l*id1];
                if l<Ny
                        % There are <Nx> horizontal faces. 
                    obj.Face_hor_int(k_b+(l-1)*Nx,:) = [k_b,l*id2+1,k_b,l*id2];
                end
            end
        end
        
        
        function obj=BuildFacesArraysSlow(obj)
        %BuildFacesArraysSlow Build Faces arrays.
        %This produces the same result as <BuildFacesArrays>, but a lot
        %slower. (For testing purposes only.)
        
        xmin = obj.bound(1,1);
        xmax = obj.bound(1,2);
        ymin = obj.bound(2,1);
        ymax = obj.bound(2,2);
        Nx = obj.N(1);
        Ny = obj.N(2);
        obj.Face_vert_int=zeros(0);
        obj.Face_hor_int=zeros(0);
        obj.Face_top_bound=zeros(0);
        obj.Face_bottom_bound=zeros(0);
        obj.Face_left_bound=zeros(0);
        obj.Face_right_bound=zeros(0);
        
            % Loop over every NODES
            % For each node, we assess the right horizontal and the bottom
            % vertical face (so, two faces per node).
            % Illustration:
            %  (*) --
            %   |

        for l=1:(Ny+1) % y-axis (top to bottom)
            for k=1:(Nx+1) % x-axis (left to right)
                istop = obj.y(l)==ymax;
                isbottom = obj.y(l)==ymin;
                isleft = obj.x(k)==xmin;
                isright = obj.x(k)==xmax;
                    % node on top left corner
                if istop && isleft
                    obj.Face_top_bound(end+1,:)=[k,l];
                    obj.Face_left_bound(end+1,:)=[k,l];
                   % node on bottom left corner
                elseif isbottom && isleft
                    obj.Face_bottom_bound(end+1,:)=[k,l-1];
                   % node on top right corner
                elseif istop && isright 
                    obj.Face_right_bound(end+1,:)=[k-1,l];
                    % node on bottom right corner
                elseif isbottom && isright
                    % no face to add!
                elseif istop
                    obj.Face_top_bound(end+1,:)=[k,l];
                    obj.Face_vert_int(end+1,:)=[k-1,l,k,l];
                elseif isbottom
                    obj.Face_bottom_bound(end+1,:)=[k,l-1];
                elseif isright
                    obj.Face_right_bound(end+1,:)=[k-1,l];
                elseif isleft
                    obj.Face_left_bound(end+1,:)=[k,l];
                    obj.Face_hor_int(end+1,:)=[k,l,k,l-1];
                    % interior node
                else
                    obj.Face_vert_int(end+1,:)=[k-1,l,k,l];
                    obj.Face_hor_int(end+1,:)=[k,l,k,l-1];
                end
            end
        end
            
            % Check that no (k,l) is outside permissible bounds.
            
        end
    end
    
end

