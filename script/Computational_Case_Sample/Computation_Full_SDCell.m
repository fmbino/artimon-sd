%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script demonstrates how to use Artimon to perform a computation over
% a "full" 2D SD cell:
%   - Tensorial SD cell
%   - Free SD cell
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pre-requiste : main.m (for path setting)

%% 1 -- Mesh: definition & display
% Example of a mesh definition.

x = [0,1]; % xmin, xmax
y = [0,1]; % ymin, ymax
Nx=25; % cell in x-axis
Ny=25; % cell in y-axis
Mesh = Mesh_2D_RectUniform([x;y],[Nx;Ny]);

%% 1 -- Useful methods
Mesh.Display();
Mesh.PrintSummary();

%% 2 -- SD cell definition: 2nd order Tensorial cell
% Sample tensorial second order cell (from Abeele)

order=2; % <p+1>
alpha=0.50;
xs = zeros(order^2,2); % Ns = (p+1)^2 (nb. of sol. points)
xs = [alpha,alpha;
      alpha,-alpha;
      -alpha,alpha;
      -alpha,-alpha];
xf = [-1,-1;
      -1,0;
      -1,1;
      0,-1;
      0,0;
      0,1;
      1,-1;
      1,0;
      1,1];
SDCell = SDCell_2D_Tensorial(xs,xf,order,'TensorProductBasis');

%% 2 -- SD cell definition: 2nd order Free cell
% Sample 'free cell'

s=[-1,1,-1,1]; % local coordinates range
    %-- Solution
n_sol = 4; % number of solution points
config_sol = 2; % Configuration number (solution)
base_sol = 1; % pol. basis number (solution
param_sol = [0.44]; % D.o.F. value(s)

    %-- Flux
n_f = 9;
config_f = 2;
base_f = 2;
param_f = [0.44];
    
    % Mask & points creation
mask_Ps = Mask_polynom(n_sol,base_sol);
xs = sol_point_distribution(n_sol,config_sol,param_sol,s);
mask_Pf = Mask_polynom(n_f,base_f);
xf = flux_point_distribution(n_f,config_f,param_f,s);

SDCell = SDCell_2D_Free(xs,xf,1,mask_Ps,mask_Pf);

%% 2 -- Free SD Cell
% This is the parametrization shown in the report.

s=[-1,1,-1,1]; % local coordinates range

n_sol = 4; % number of solution points
config_sol = 2; % Configuration number (solution)
base_sol = 1; % pol. basis number (solution
param_sol = [0.44]; % D.o.F. value(s) [alpha, beta, gamma]

    % same, for flux points
n_f = 13;
config_f = 5;
base_f = 1;
param_f = [1,0.5];

mask_Ps = Mask_polynom(n_sol,base_sol);
xs = sol_point_distribution(n_sol,config_sol,param_sol,s);

mask_Pf = Mask_polynom(n_f,base_f);
xf = flux_point_distribution(n_f,config_f,param_f,s);

SDCell = SDCell_2D_Free(xs,xf,1,mask_Ps,mask_Pf);
%% 2 -- Useful methods
SDCell.Display();
SDCell.PrintSummary();

%% 3 -- Create case
% Case: 2D Transport (periodicity conditions) of a scalar peak.

    % Parameters
Nq = 1; % number of conservative variables
a=0.5; b=80;
CFL = 0.10;
V = [1;0]; % [Vx; Vy] propagation speed
CaseName='TestTransport2D';
    % Initial scalar field [x,y] (Nxd) -> [NxNq]
q0 = @(x)(Gaussian(x(:,1),x(:,2),a,b));
%q0 = @(x)(Sinus(x(:,1),x(:,2)));

    % Case creation
%SDCaseTr = SDCase_2D_Full_Transport(V,q0,CFL,Mesh,SDCell,CaseName);
SDCaseTr = SDCase_2D_Full_LinearTransport(V,q0,CFL,Mesh,SDCell,CaseName);
SDCaseTr.PrintSummary();
%% 3 -- Display the initial field
figure
SDCaseTr.DisplaySol(1);
%% 3 -- Perform iterations
SDCaseTr = SDCaseTr.Iterate(0.5); % t=1
%% 3 -- Display any iteration
iterationToDisplay = 3;
figure
SDCaseTr.DisplaySol(1,iterationToDisplay);

%% Compute L2 error
dt = SDCaseTr.getElapsedTime(); % time elapsed
qe = @(x)(q0(x + dt*[V(1)*ones(size(x,1),1), V(2)*ones(size(x,1),1)])); % exact solution, transported
SDCaseTr.ComputeL2Error(qe)
%% 3 -- Save video (Solution)
filename = 'Transport 2D - 2nd order Tensorial - Side view';
viewangle = [-23,28];
%viewangle = [0,90];
zaxis = [-0.3,1.2];
duration = 0.15;
SDCaseTr.SaveVideo(1,viewangle,zaxis,filename,duration);