%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script demonstrates how to use Artimon to perform a computation over
% a "standard" 2D SD cell.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pre-requiste : main.m (for path setting)

%% 1 -- Mesh: definition & display
% Example of a mesh definition.

x = [-1,1]; % xmin, xmax
y = [-1,1]; % ymin, ymax
Nx=10; % cell in x-axis
Ny=10; % cell in y-axis
Mesh = Mesh_2D_RectUniform([x;y],[Nx;Ny]);

%% 1 -- Useful methods
% Check the mesh

Mesh.Display();
Mesh.PrintSummary();

%% 2 -- Standard 2D SD cell definition: 1st order (Finite Volume)
order=1; % target order
xs_1d = [0];
xf_1d = [-1,1];
[xs,xfx,xfy] = GetLoc1DSD_distribution(xs_1d,xf_1d);
SDCell = SDCell_2D_Standard(xs,xfx,xfy,order);
%% 2 -- Standard 2D SD cell definition: 2nd order
order=2;
alpha_2 = 0.58;
xs_1d = [-alpha_2,alpha_2];
xf_1d = [-1,0,1];
[xs,xfx,xfy] = GetLoc1DSD_distribution(xs_1d,xf_1d);
SDCell = SDCell_2D_Standard(xs,xfx,xfy,order);
%% 2 -- Standard 2D SD cell definition: 3rd order
order=3;
alpha_3 = 0.58;
xs_1d = [-alpha_3,0,alpha_3];
xf_1d = [-1, -alpha_3, alpha_3, 1];
[xs,xfx,xfy] = GetLoc1DSD_distribution(xs_1d,xf_1d);
SDCell = SDCell_2D_Standard(xs,xfx,xfy,order);
%% Standard 2D SD cell definition: 4th order
order=4;
alpha_4 = 0.78;
xs_1d = [-alpha_4,alpha_4-1,-alpha_4+1,alpha_4];
xf_1d = [-1,-alpha_4,0,alpha_4,1];
[xs,xfx,xfy] = GetLoc1DSD_distribution(xs_1d,xf_1d);
SDCell = SDCell_2D_Standard(xs,xfx,xfy,order);

%% Standard 2D SD cell definition: 5th order
order=5;
alpha_5_1 = 0.83;
alpha_5_2 = 0.36;
xs_1d = [-alpha_5_1,-alpha_5_2,0,alpha_5_2,alpha_5_1];
xf_1d = [-1,-alpha_5_1,-alpha_5_2,alpha_5_2,alpha_5_1,1];
[xs,xfx,xfy] = GetLoc1DSD_distribution(xs_1d,xf_1d);
SDCell = SDCell_2D_Standard(xs,xfx,xfy,order);

%% Standard 2D SD cell definition: 6th order
order=6;
alpha_6_1 = 0.88;
alpha_6_2 = 0.53;
xs_1d = [-alpha_6_1,-alpha_6_2,-1+alpha_6_1,1-alpha_6_1,alpha_6_2,alpha_6_1];
xf_1d = [-1,-alpha_6_1,-alpha_6_2,0,alpha_6_2,alpha_6_1,1];
[xs,xfx,xfy] = GetLoc1DSD_distribution(xs_1d,xf_1d);
SDCell = SDCell_2D_Standard(xs,xfx,xfy,order);

%% 2 -- SD cell useful methods
% Check the SD cell
SDCell.Display();
SDCell.PrintSummary();

%% 2 -- SD cell useful methods: Play around with polynomial reconstruction
% View polynomial reconstruction over the cell : create polynomial
% coefficients, and plot them.

        % Shock behaviour 
Qxs = [10;10;1;1]; % (2nd order standard cell)
Qxs(1:6,1)=100; Qxs(7:9,1)=1; % (3rd order standard cell)
Cs = SDCell.Ls_xs\Qxs;

    % Example of polynomial reconstruction
Cs = [0;1;0;0]; % Sol. Pol. Coef. [1;x;y;xy]
Cfx = [0;1;0;0;0;0]; % Flux Pol Coef [1;x;x^2;y;xy;x^2y] for the x-flux
Cfy = [0;1;0;0;0;0]; % Flux Pol Coef [1;x;y;xy;y^2;xy^2] for the y-flux

SDCell.DisplayPol(Cs,'Solution Reconstruction');
SDCell.DisplayPol(Cfx,'x-Flux Reconstruction',0,'fx');
SDCell.DisplayPol(Cfy,'y-Flux Reconstruction',0,'fy');


%% 3 -- Defining a computational case
% Intialize you computational case

Nq = 1; % number of conservative variables
a=0; b=40;
%q0 = @(x)(HorizontalStep(x(:,1),x(:,2)));
%q0 = @(x)(Gaussian(x(:,1),x(:,2),a,b)); 
q0 = @(x)(Sinus1D(x(:,1)));
%q0 = @(x)(Sinus2D_Liu_Transport(x(:,1),x(:,2)));
%q0 = @(x)(Sinus2D_Liu_Burgers(x(:,1),x(:,2)));
CFL = 0.10;
V = [1;1]; % [Vx; Vy] propagation speed (2D transport)
CaseName='Burgers2D-RefSol';
%Marker='AP-TVD';
Marker='None';
%Limiter='1-th order';
Limiter='None';

%SDCaseTr = SDCase_2D_Standard_Transport(V,q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter);
SDCaseTr = SDCase_2D_Standard_LinearTransport(V,q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter);
%SDCaseTr = SDCase_2D_Standard_Euler(q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter);
%SDCaseTr = SDCase_2D_Standard_Burgers(q0,CFL,Mesh,SDCell,CaseName,Marker,Limiter);
SDCaseTr.PrintSummary();

%% 3 -- Display the initial field
% This display the last iteration.
figure
SDCaseTr.DisplaySol(1);
%SDCaseTr.DisplayTroubledCells(1);
%% 3 -- Perform iterations
SDCaseTr = SDCaseTr.Iterate(2); % until t=2
%% 3 -- Display any iteration
iterationToDisplay = 0;
SDCaseTr.DisplaySol(1,iterationToDisplay);
viewangle = [45,20];
view(viewangle);
%% 3 -- Save video (Solution)
filename = 'Transport2D-Gaussian-SideView-Ordre1';
viewangle = [-23,28];
%viewangle = [0,90];
zaxis = [-0.3,1.2];
duration = 0.1;
SDCaseTr.SaveVideo(1,viewangle,zaxis,filename,duration);
%% 3 -- Save video (with Troubled cell)
filename = 'Transport 2D - Step - Top view - Flo Marker - alpha 2';
viewangle = [0,90];
%viewangle = [26,20];
zaxis = [-0.3,1.2];
duration = 0.1;
SDCaseTr.SaveVideoWithTroubledCells(1,viewangle,zaxis,filename,duration);
%% Compute L2 error
dt = SDCaseTr.getElapsedTime(); % time elapsed
qe = @(x)(q0(x + dt*[V(1)*ones(size(x,1),1), V(2)*ones(size(x,1),1)])); % exact solution, transported
fprintf('L2 error is : %1.4g\n',SDCaseTr.ComputeL2Error(qe));