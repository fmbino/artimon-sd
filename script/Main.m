%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon: Main script
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Path Management
% Usage: the current folder must be the root folder. All subfolders are
% included in the path.

addpath(genpath(pwd));

%% Function definitions
% Format is : [x,y] (Nxd) -> q([x,y])(NxNq)
HorizontalStep = @(x,y)(1*((x>0).*(x<0.5)));
Gaussian = @(x,y,a,b)(exp(-b*(a-x).^2-b*(a-y).^2)); 
Sinus1D = @(x)(sin(pi*(x))); % x in [-1,1]
Sinus2D_Liu_Transport = @(x,y)(sin(pi*(x+y))); % (x,y) in [-1,1]x[-1,1]
Sinus2D_Liu_Burgers = @(x,y)(0.25+0.5*sin(pi*(x+y)));  % (x,y) in [-1,1]x[-1,1]