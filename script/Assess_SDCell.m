%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Artimon: test properties of a point distribution
%%%%%
% Usage:
%   - Set up parameters
%   - Run relevant section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Parameters
    % [Degrees of freedom - number of cell in x & y-axis]
Nxint = [50,100,125];
Nyint = [50,100,125];
Title = 'SDCell Standard 4th order'; % for plot

%% SD Cell choice
% Define a SDCell

%% 1a - Compute error : Transport
% Assess order as in (Liu,2006)
Nq = 1; % number of conservative variables
CFL = 0.1; % Small CFL number
V = [1;1]; % [Vx; Vy] propagation speed
q0 = @(x)(Sinus2D_Liu_Transport(x(:,1),x(:,2)));
CaseName='TestTransport2D';
% Output:
%   err: error vector
err = zeros(0);
for i=1:length(Nxint)
        % -- Build mesh
    Mesh = Mesh_2D_RectUniform([-1,1;-1,1],[Nxint(i);Nyint(i)]);
        % -- Create case: 2D Transport of a sine wave.
    %SDCaseTr = SDCase_2D_Full_Transport(V,q0,CFL,Mesh,SDCell,CaseName);
    %SDCaseTr = SDCase_2D_Full_LinearTransport_2D(V,q0,CFL,Mesh,SDCell,CaseName);
    %SDCaseTr = SDCase_2D_Standard_Transport(V,q0,CFL,Mesh,SDCell,CaseName,'None','None');
    SDCaseTr = SDCase_2D_Standard_LinearTransport(V,q0,CFL,Mesh,SDCell,CaseName,'None','None');
        % -- Iterate
    SDCaseTr = SDCaseTr.Iterate(1); % until t=1
        % -- Compute L2 error
    dt = SDCaseTr.getElapsedTime(); % time elapsed
    qe = @(x)(q0(x + dt*[V(1)*ones(size(x,1),1), V(2)*ones(size(x,1),1)])); % exact solution, transported
    err(end+1)=SDCaseTr.ComputeL2Error(qe);
end

%% 1a - Plot error
dxint = 2./Nxint;
dyint = 2./Nyint;
order = polyfit(log10(sqrt(dxint.^2+dyint.^2)),log10(err),1); order = order(1);
loglog(sqrt(dxint.^2+dyint.^2),err,'-o');
xlabel('log(char. mesh size)'); ylabel('log(err) (L2-error)');
leg{1}=sprintf('Order: %1.1d',order);
legend(leg);
title(Title);
%% 1b - Compute error
% Displacement along the x-axis
% Rmq : one must perform an anisotropic refinement to get the order.
% (i.e. Nyint = cste)

Nq = 1; % number of conservative variables
CFL = 0.1; % Small CFL number
V = [1;0]; % [Vx; Vy] propagation speed
q0 = @(x)(Sinus1D(x(:,1),x(:,2)));
CaseName='TestTransport2D';
% Output:
%   err: error vector
err = zeros(0);
for i=1:length(Nxint)
        % -- Build mesh
    Mesh = Mesh_2D_RectUniform([-1,1;-1,1],[Nxint(i);Nyint(i)]);
        % -- Create case: 2D Transport of a sine wave.
    %SDCaseTr = SDCase_2D_Full_Transport(V,q0,CFL,Mesh,SDCell,CaseName);
    %SDCaseTr = SDCase_2D_Full_LinearTransport_2D(V,q0,CFL,Mesh,SDCell,CaseName);
    %SDCaseTr = SDCase_2D_Standard_Transport(V,q0,CFL,Mesh,SDCell,CaseName,'None','None');
    SDCaseTr = SDCase_2D_Standard_LinearTransport(V,q0,CFL,Mesh,SDCell,CaseName);
        % -- Iterate
    SDCaseTr = SDCaseTr.Iterate(1); % until t=1
        % -- Compute L2 error
    dt = SDCaseTr.getElapsedTime(); % time elapsed
    qe = @(x)(q0(x + dt*[V(1)*ones(size(x,1),1), V(2)*ones(size(x,1),1)])); % exact solution, transported
    err(end+1)=SDCaseTr.ComputeL2Error(qe);
end
%% 1b - Plot error
order = polyfit(log10(SDCell.Ns*Nxint.*Nyint),log10(err),1); order = -order(1);
loglog(SDCell.Ns*Nxint.*Nyint,err,'-o');
xlabel('log(ddl)'); ylabel('log(err)');
leg{1}=sprintf('Order: %1.1d',order);
legend(leg);
title(Title);

%% 2 - Stability Analaysis: matrix eigeinvalues
% Rmk: its condition number may be too high to compute eigenvalues
A = -SDCaseTr.div*SDCaseTr.iLs_xs; % dQ/dt = AxQ
if cond(A)<10^8
    eignval = eigs(A);
else
    error('Condition number of A too high: reduce the tolerance on eigs');
end
plot(real(eignval),imag(eignval),'o');

%% 3 - SD Cell interpolation matrix condition number
% An SD cell with high condition numbers cannot be used.

SDCell.getConditionNumber();

%% Results: 1dof non-symmetric SD cell

% RK1, CFL=0.01, 4 loop --> order: 0.23
ddl = [40^2, 50^2,60^2,100^2];
err = [0.6117,0.5656,0.5236,0.4015];
% RK1, CFL=0.01, 1/2 loop --> order: 0.43

% RK3, CFL=0.01, 1/2 loop --> order: 0.43
ddl = [40^2, 50^2,60^2,100^2];
err = [0.1554, 0.1279, 0.1089, 0.0695];

% RK3, CFL=0.01, 1 loop --> order: 0.40
ddl = [40^2, 50^2,60^2,100^2];
err = [0.2772, 0.2331,0.2013,0.1325];

% RK3, CFL=0.1, 4 loop --> order: 0.23
ddl = [40^2, 50^2,60^2,100^2];
err = [0.6136, 0.5679, 0.5260, 0.4038];


plot(log10(ddl),log10(err),'-o');
xlabel('log(ddl)'); ylabel('log(err)');
