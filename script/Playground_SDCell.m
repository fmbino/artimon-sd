%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SDCell comparison & test
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% SD cell: '1st order' Tensorial
order=1;
alpha=0.4;
xs = [0,0];
xf = [-1,0.1;
      -0.1,-1;
      1,-0.1;
      0.1,1];
SDCell = SDCell_2D_Tensorial(xs,xf,order,'TensorProductBasis');

%% SD cell: definition & display
% Example of an SD cell defintion.
    % Sample second order cell (from Abel)
order=2; % <p+1>
alpha=0.25;
alpha2=0.05;
xs = zeros(order^2,2); % Ns = (p+1)^2 (nb. of sol. points)
xs = [alpha,alpha;
      alpha,1-alpha;
      1-alpha,alpha;
      1-alpha,1-alpha];
  
xf = zeros((order+1)^2,2); % Nf = (p+2)^2 (nb of flux points)
xf = [0,0;
      1,0;
      0,1;
      1,1;
      0.5,0;
      0,0.5;
      1,0.5;
      0.5,0.5;
      0.5,1];
xf = [0.5,0;
      0,0.5;
      1,0.5;
      0.5,1;
      alpha2,alpha2;
      alpha2,1-alpha2;
      1-alpha2,alpha2;
      1-alpha2,1-alpha2;
      0.5,0.5];
SDCell = SDCell_2D_Tensorial(xs,xf,order,'TensorProductBasis');
      % Useful methods
%SDCell.Display();
SDCell.PrintSummary();


%% Test interpolation over an SD cell
% Perform a sol->fux interpolation manually

Qxs = ones(order,1);

CQ = SDCell.Ls_xs\Qxs;
Qxf = SDCell.Ls_xf*CQ;
Fx = Qxf+linspace(1,2,size(Qxf,1))';
CFx = SDCell.Lf_xf\Fx;
SDCell.DisplayPol(CQ,'solution');
figure
SDCell.DisplayPol(CFx,'x-flux');


%% Free SD Cell
s=[-1,1,-1,1]; % local coordinates range

n_sol = 4; % number of solution points
config_sol = 2; % Configuration number (solution)
base_sol = 1; % pol. basis number (solution
param_sol = [0.44]; % D.o.F. value(s)

    % same, for flux points
n_f = 13;
config_f = 5;
base_f = 1;
param_f = [1,0.5];

mask_Ps = Mask_polynom(n_sol,base_sol);
xs = sol_point_distribution(n_sol,config_sol,param_sol,s);

mask_Pf = Mask_polynom(n_f,base_f);
xf = flux_point_distribution(n_f,config_f,param_f,s);

SDCell = SDCell_2D_Free(xs,xf,1,mask_Ps,mask_Pf);
SDCell.Display();

SDCell.getConditionNumber

%% Ordre 4 : case Nf=20
alpha1 = 0.3;
alpha2 = 0.6;

x = [-alpha1,-alpha2,alpha1,alpha2];
xs = zeros(0);
for i=1:length(x)
   for j=1:length(x)
        xs(end+1,:) = [x(i),x(j)];
   end
end

xf = [0,1;
      0,-1;
      1,0;
      -1,0;
      1,alpha2;
      1,alpha1;
      1,-alpha1;
      1,-alpha2;
      -1,alpha2;
      -1,alpha1;
      -1,-alpha1;
      -1,-alpha2;
      alpha2,-1;
      alpha1,-1;
      -alpha1,-1;
      -alpha2,-1;
        alpha2,1;
      alpha1,1;
      -alpha1,1;
      -alpha2,1];
  
mask_Ps = zeros(length(xs),length(xs));
mask_Ps(1:sqrt(length(xs)),1:sqrt(length(xs))) = 1;
mask_Pf = zeros(length(xf),length(xf));
mask_Pf(1,1:5) = 1;
mask_Pf(2,1:4) = 1;
mask_Pf(3,1:3) = 1;
mask_Pf(4,1:2) = 1;
mask_Pf(5,1:1) = 1;
mask_Pf(5,2)=1; mask_Pf(2,5)=1;  
mask_Pf(4,3)=1; mask_Pf(3,4)=1;
mask_Pf(4,4)=1;

SDCell = SDCell_2D_Free(xs,xf,4,mask_Ps,mask_Pf);

%% Ordre 4 : case Nf=21
alpha1 = 0.3;
alpha2 = 0.6;

x = [-alpha1,-alpha2,alpha1,alpha2];
xs = zeros(0);
for i=1:length(x)
   for j=1:length(x)
        xs(end+1,:) = [x(i),x(j)];
   end
end

xf = [0,0;
      0,1;
      0,-1;
      1,0;
      -1,0;
      1,alpha2;
      1,alpha1;
      1,-alpha1;
      1,-alpha2;
      -1,alpha2;
      -1,alpha1;
      -1,-alpha1;
      -1,-alpha2;
      alpha2,-1;
      alpha1,-1;
      -alpha1,-1;
      -alpha2,-1;
        alpha2,1;
      alpha1,1;
      -alpha1,1;
      -alpha2,1];
  
mask_Ps = zeros(length(xs),length(xs));
mask_Ps(1:sqrt(length(xs)),1:sqrt(length(xs))) = 1;
mask_Pf = zeros(length(xf),length(xf));
mask_Pf(1,1:5) = 1;
mask_Pf(2,1:4) = 1;
mask_Pf(3,1:3) = 1;
mask_Pf(4,1:2) = 1;
mask_Pf(5,1:1) = 1;
mask_Pf(5,2)=1; mask_Pf(2,5)=1;  
mask_Pf(4,3)=1; mask_Pf(3,4)=1;
mask_Pf(4,4)=1; mask_Pf(5,5)=1;

SDCell = SDCell_2D_Free(xs,xf,4,mask_Ps,mask_Pf);

%% Ordre 4 : case Nf=24
alpha1 = 0.3;
alpha2 = 0.6;
alpha3 = 0.45;
x = [-alpha1,-alpha2,alpha1,alpha2];
xs = zeros(0);
for i=1:length(x)
   for j=1:length(x)
        xs(end+1,:) = [x(i),x(j)];
   end
end

xf = [alpha3,alpha3;
      -alpha3,alpha3;
      alpha3,-alpha3;
      -alpha3,-alpha3;
      0,1;
      0,-1;
      1,0;
      -1,0;
      1,alpha2;
      1,alpha1;
      1,-alpha1;
      1,-alpha2;
      -1,alpha2;
      -1,alpha1;
      -1,-alpha1;
      -1,-alpha2;
      alpha2,-1;
      alpha1,-1;
      -alpha1,-1;
      -alpha2,-1;
        alpha2,1;
      alpha1,1;
      -alpha1,1;
      -alpha2,1];
  
mask_Ps = zeros(length(xs),length(xs));
mask_Ps(1:sqrt(length(xs)),1:sqrt(length(xs))) = 1;
mask_Pf = zeros(length(xf),length(xf));
mask_Pf(1:5,1:5) = 1;
%mask_Pf(5,5)=0;
mask_Pf(4,4)=0;

SDCell = SDCell_2D_Free(xs,xf,4,mask_Ps,mask_Pf);
