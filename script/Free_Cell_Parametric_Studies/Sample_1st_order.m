%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SDCell parametric study
% Purpose: To compare various flux point distribution through a simple
% criterion : the maximum condition number of the interpolation matrices.
% (A condition number above a given threshold will lead to a degraded
% computation.)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%clear all;
clear all
lcas=12;
for cas=lcas

%% Condition number study with the 'Free' SD cell class
% Definition of a parametric 1st order SD cell
%figure
%hold on
leg = cell(0);

order=1; % target order for the solution (for references purposes only)
%alpha=linspace(0,1,20);% parameter (flux distribution point)
%alpha=sqrt(2)/2;
%alpha=1;
resol=30;

alpha=linspace(0,1,resol);
%alpha=linspace(0.6,1,resol);
%alpha=1;

beta=linspace(0,1,resol);
%beta=linspace(0.4,0.6,resol);

gamma=linspace(0,1,resol);
%gamma=linspace(0.5,0.7,resol);

delta=linspace(0,1,resol);


%beta=12/20;
%xs = [0.5,0.5]; % solution points coordinates in [0,1]x[0,1]

% -- Index mask (custom polynomial basis)
% (NsxNs) Must contains exactly Ns '1'

% (NfxNf) Must contains exactly Nf '1'
nbpts=17;
config_flux=3;
config_sol=cas;


s=[0,1,0,1];
s=[-1,1,-1,1];

threshold = 1000;
cmax=threshold;
tb=1:nb_basis(nbpts);
tb=4;
for no=tb
    sprintf('%d sur %d',no+(cas-min(lcas))*nb_basis(nbpts),nb_basis(nbpts)*length(lcas))
    mask_Pf = Mask_polynom(nbpts,no);
    mask_Ps = [1];
    % Maximum condition number allowed
    %C = zeros(0); % Maximum condition number for the cell
    
    %%%
    % Gestion of number of DoF
    %%%
    ti=length(alpha);
    %ti=1;
    tj=length(beta);
    %tj=1;
    tk=length(gamma);
    tk=1;
    tl=length(delta);
    tl=1;
    
    al=zeros(0);
    bl=zeros(0);
    gl=zeros(0);    
    cl=zeros(0);    
    
    for i=1:ti
       
        for j=1:tj
             sprintf('%d sur %d',j+(i-1)*length(alpha),length(alpha)*length(beta))
            for k=1:tk
                for l=1:tl
                    
                    param=[alpha(i),beta(j),gamma(k),delta(l)];
                    
                    al(end+1)=alpha(i);
                    bl(end+1)=beta(j);
                    gl(end+1)=gamma(k);

                    
                    
                    xf = flux_point_distribution(nbpts,config_sol,param,s);
                    xs = [0,0];
                    
                    %               C(1): Ls(xs)
                    %               C(2): Ls(xf)
                    %               C(3): Lf(xs)
                    %               C(4): Lf(xf)
                    
                    % Build SD cell & get the maximum condition number
                    SDCell = SDCell_2D_Free(xs,xf,order,mask_Ps,mask_Pf);
                    % !Rmk: run once part 1,2,3 of the script Main.m
                    % (this will define 'Mesh','q0','CFL','V','CaseName')
                    SDCaseTr = SDCase_2D_Full_LinearTransport_2D(V,q0,CFL,Mesh,SDCell,CaseName);
                    SDCaseTr = SDCaseTr.Iterate(ceil(Nx/CFL));
                    max(SDCaseTr.Qxs);
                    %C(end+1) = max(SDCell.getConditionNumber());
                    cond = SDCell.getConditionNumber();
                    C(i,j,k,l,no,cas) = min(cond(1,4),threshold); % saturation
                    cl(end+1)=min(cond(1,4),threshold);;
                    if  cmax>cond
                        
                        cmax=cond(1,4);
                        paramax=param;
                        nomax=no;
                    end
                end
                
            end
        end
    end
    
    % Display
    %figure
    %hold all
    %leg = cell(0);
    % if no==1
    % plot(alpha,C(:,4),'-bo'); leg{1}=sprintf('Polynomial basis n� %d',no);
    % elseif no==2
    % plot(alpha,C(:,4),'-ro'); leg{2}=sprintf('Polynomial basis n� %d',no);
    % elseif no==3
    % plot(alpha,C(:,4),'-go'); leg{3}=sprintf('Polynomial basis n� %d',no);
    % elseif no==4
    % plot(alpha,C(:,4),'-mo'); leg{4}=sprintf('Polynomial basis n� %d',no);
    % end
end
% legend(leg);
% xlabel('Flux point distr. parameter: alpha');
% ylabel(sprintf('Condition number (threshold: %.1e)',threshold));
% title('Flux to Flux');

end
