%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SDCell parametric study
% Purpose: To compare various flux point distribution through a simple
% criterion : the maximum condition number of the interpolation matrices.
% (A condition number above a given threshold will lead to a degraded
% computation.)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%clear all;
clear all

%% 1 -- Mesh: definition & display
% Example of a mesh definition.

x = [0,1]; % xmin, xmax
y = [0,1]; % ymin, ymax
Nx=15; % cell in x-axis
Ny=15; % cell in y-axis
Mesh = Mesh_2D_RectUniform([x;y],[Nx;Ny]);

% Useful methods
%Mesh.Display();
%Mesh.PrintSummary();
%% 2 -- SD cell definition: 1st order
order=1;
alpha=0.4;
xs = [0.5,0.5];
xf = [0,0.6;
    0.4,0;
    1,0.4;
    0.6,1];
SDCell = SDCell_2D_Tensorial(xs,xf,order,'TensorProductBasis');


% Useful methods
%SDCell.Display();
%SDCell.PrintSummary();
%% 3 -- Create case

% Case: 2D Transport (periodicity conditions) of a scalar peak.
Nq = 1; % number of conservative variables
a=0.5; b=80;
f0 = @(x,a,b)(exp(-b*(a-x(:,1)).^2-b*(a-x(:,2)).^2));
q0 = @(x)(f0(x,a,b)); % Initial scalar field [x,y] (Nxd) -> [NxNq]
CFL = 0.80;
V = [1;0]; % [Vx; Vy] propagation speed
CaseName='TestTransport2D';

SDCaseTr = SDCase_2D_Full_Transport(V,q0,CFL,Mesh,SDCell,CaseName);






%% Main part


order=1;

resol=30;

alpha=linspace(0,1,resol);
%alpha=linspace(0.6,1,resol);
%alpha=1;

beta=linspace(0,1,resol);
%beta=linspace(0.4,0.6,resol);

gamma=linspace(0,1,resol);
%gamma=linspace(0.5,0.7,resol);

delta=linspace(0,1,resol);



nbpts_sol=1;
config_sol=1;
base_sol=1;

nbpts_flux=8;
config_flux=2;
base_flux=2;




s=[-1,1,-1,1];

threshold = 10^900;
cmax=threshold;



%sprintf('%d sur %d',tb+(cas-min(lcas))*nb_basis(nbpts),nb_basis(nbpts)*length(lcas))
mask_Pf = Mask_polynom(nbpts_flux,base_flux);
mask_Ps = Mask_polynom(nbpts_sol,base_sol);



%%%
% Gestion of number of DoF
%%%
ti=length(alpha);
%ti=1;
tj=length(beta);
tj=1;
tk=length(gamma);
tk=1;
tl=length(delta);
tl=1;

al=zeros(0);
bl=zeros(0);
gl=zeros(0);
cl=zeros(0);
clm=zeros(0);

for i=5
    
    for j=1:tj
        sprintf('%d sur %d',j+(i-1)*length(alpha),length(alpha)*length(beta))
        for k=1:tk
            for l=1:tl
                
                param=[alpha(i),beta(j),gamma(k),delta(l)];
                
                al(end+1)=alpha(i);
                bl(end+1)=beta(j);
                gl(end+1)=gamma(k);
                
                
                
                xf = flux_point_distribution(nbpts_flux,config_flux,param,s);
                xs = [0,0];
                

                SDCell = SDCell_2D_Free(xs,xf,order,mask_Ps,mask_Pf);
                % !Rmk: run once part 1,2,3 of the script Main.m
                % (this will define 'Mesh','q0','CFL','V','CaseName')
                SDCaseTr = SDCase_linear_Transport_2D(V,q0,CFL,Mesh,SDCell,CaseName);
                %SDCaseTr = SDCaseTr.Iterate(ceil(Nx/CFL));
                SDCaseTr = SDCaseTr.Iterate(2);
                
                
                
                condm =max(SDCaseTr.Qxs);
                Cm(i,j,k,l) = condm;
                clm(end+1)=condm;
                
                cond = max(SDCell.getConditionNumber());
                C(i,j,k,l) = cond;
                cl(end+1)=cond;
                
                
  
                if  cmax>condm
                    
                    cmax=condm;
                    paramax=param;
                    tbmax=tb;
                end
            end
            
        end
    end
    
    
    
    
    
    
end
