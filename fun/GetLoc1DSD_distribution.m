function [xs,xfx,xfy] = GetLoc1DSD_distribution(xs_1d,xf_1d)
%GetLoc1DSD_distribution Compute a locally 1D SD points distribution from
%1D distribution.
% Inputs:
%   xs_1d (1xNs) 1D solution point distribution
%   xf_1d (1xNf) 1D flux point distribution
% Outputs:
%   xs (Ns*Nsx2) [xs,ys] 2D solution point distribution
%   xfx (Ns*Nfx2) [xfx,yfx] 2D x-flux point distribution
%   xfy (Ns*Nfx2) [xfy,yfy] 2D y-flux point distribution

    validateattributes(xs_1d,{'numeric'},{'2d','nrows',1});
    validateattributes(xf_1d,{'numeric'},{'2d','nrows',1});
    
        % Get xs from xs_1d
    a = reshape(kron(xs_1d,ones(size(xs_1d))),[length(xs_1d)^2,1]);
    b = reshape(kron(xs_1d',ones(size(xs_1d))),[length(xs_1d)^2,1]);
    xs = [a,b];
        % Get xfx & xfy from xf_1d
    a = reshape(kron(xs_1d,ones(size(xf_1d))),[length(xf_1d)*length(xs_1d),1]);
    b = reshape(kron(xs_1d',ones(size(xf_1d))),[length(xf_1d)*length(xs_1d),1]);
    c = reshape(kron(xf_1d,ones(size(xs_1d))),[length(xf_1d)*length(xs_1d),1]);
    d = reshape(kron(xf_1d',ones(size(xs_1d))),[length(xf_1d)*length(xs_1d),1]);
    xfy = [a,d];
    xfx = [d,a];
end

