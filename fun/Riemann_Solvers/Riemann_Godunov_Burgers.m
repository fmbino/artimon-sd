function F = Riemann_Godunov_Burgers(Ul, Ur)
%Riemann_Godunov_Burgers Exact Riemann solver for the scalar 1D Burger's 
%equation.
%
% Inputs:
%   Ul Left state velocity. (scalar)
%   Ur Right state velocity. (scalar)
% Outputs:
%   F Godunov flux (scalar)

F = (1/2)*max((min(Ur,0))^2,max(Ul,0)^2);

end