function F = Riemann_Roe_Euler( Ul, Ur, Fl, Fr, gamma)
%Riemnn_Rie_Euler Aproximate Riemann solver (Roe), for 1D Euler's 
%equations.
% Without entropy correction.
%
% Inputs:
%   Ur  (3x1) ([rho; rho*u; rho*E]) Right state  (conservative variables)
%   Ul  (3x1) Left state 
%   Fl  (3x1) ([rho*u; rho*u^2+p; rho*u*H]) Left state flux vector 
%   Fr  (3x1) Right state flux vector 
%   gamma (1x1) heat capacity ratio (common to both sides) 
% Outputs:
%   F   (3x1) Roe flux vector 
%
% !! Make sure that the given velocity <u> and the given total energy <E>
%are consistent. !!

    %-- Checking arguments
% size
% values (rho>0, gamma>0)

        %-- Computation of Roe's mean state
    RoeMeanSt = zeros(3,1); % Roe's mean state [rho, u, H]
    D = sqrt(Ur(1)/Ul(1));
    ul = Ul(2)/Ul(1);
    ur = Ur(2)/Ur(1);
    Hl = gamma*(Ul(3)/Ul(1))-(1/2)*(gamma-1)*(ul^2);
    Hr = gamma*(Ur(3)/Ur(1))-(1/2)*(gamma-1)*(ur^2);
    RoeMeanSt(1) = sqrt(Ul(1)*Ur(1));
    RoeMeanSt(2) = (ul+D*ur)/(1+D);
    RoeMeanSt(3) = (Hl+D*Hr)/(1+D);

        % speed of sound (mean state)
    a = (gamma-1)*(RoeMeanSt(3)-(1/2)*(RoeMeanSt(2))^2);
    a = sqrt(a);

        % flux jacobien eigenvalues
    DF_eign = [RoeMeanSt(2)-a; RoeMeanSt(2); RoeMeanSt(2)+a];

        % flux jacobian eigenvectors
    [R,invR] = getR(a,RoeMeanSt(1));

        %-- Computation of Roe's matrix <A>
    A = R*diag(abs(DF_eign))*invR;

        %-- Final Roe flux vector computation
    F = zeros(3,1);
    F = Fr + Fl - A*(Ur-Ul);
    F = F/2;

end

function [R,invR] = getR(a,rho)
%getR Returns R and R^-1. (Euler's equation.)
% This simply implements analytical expressions of these matrices.
% Inputs:
%   a speed of sound
%   rho density
% Outputs:
%   R   Flux jacobian eigenvectors matrix
%   invR Inverse of R

    R = zeros(3,3);
    invR = zeros(3,3);

    R(1,:) = [1, 1, 1];
    R(2,:) = [-a/rho, 0, a/rho];
    R(3,:) = [a^2, 0, a^2];

    invR(1,:) = [0, -rho/(2*a), 1/(2*a^2)];
    invR(2,:) = [1, 0, -1/(a^2)];
    invR(3,:) = [0, rho/(2*a), 1/(2*a^2)];

end
