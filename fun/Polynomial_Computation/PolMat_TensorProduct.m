function L = PolMat_TensorProduct(x,q,p,mean,Mask)
%PolMat_TensorProduct Returns the reconstruction matrix L(x,q) for the
%so-called "Tensor product" canonical polynomial basis, in d-dimensions 
%(1 or 2). Derivates and mean values can also obtained.
% Inputs:
%   x (Nxd) points coordinates. d = 1 (1D) or 2 (2D)
%   q (1xd) [qx,qy]
%           qx : maximum degree in X
%           qy : maximum degree in Y
%           -> prod(q+1) is the total number of polynomials in the basis.
% Optional inputs:
%   p (1x2) [px,py]
%           px : order of derivation in X
%           py : order of derivation in Y
%   mean (string) 
%           'mean': L (1xprod(q+1)) contains only the mean value of every 
% polynimial function over the [-1,1] (d=1) or [-1,1]x[1,1] (d=2) cell.
%   Mask (q(1)xq(2)^(d-1)) (boolean) (optional) index mask to select only some
%   polynomial functions. Contains either '1' (polynomial fun. included) or
%   '0' (polynomial fun. excluded). This enables the creation of custom
%   polynomial basis.
%   If M(i,j)==true, then X^(i-1)Y^(j-1) is in the basis.
%   If M(i,j)==false, then X^(i-1)Y(j-1) is NOT in the basis.
%   When M is used, the size of the output of L can be lower than
%   (Nxprod(q+1)), as they are fewer points in the basis.
% Output:
%   L (Nxprod(q+1)) is the matrice associated with the basis
%   and the given N points <x>. Sample outputs are given as example.  
% Example:
%       L = PolMat_TensorProduct(x,q) is the (Nxprod(q+1)) 
%       matrix:
%           [1,x,x.^2,...,x.^q] if x is (Nx1),
%       or, if q = [qx, qy] and x is (Nx2)
%           [1,...,X^qx,Y.*(1,...,X^qx),...,Y.^qy*(1,...,X^qx)]
%       L = PolMat_TensorProduct(x,q,[1,0]) is the x-derivative of the
%       previous matrix. Same for [0,1].


    % Check arguments
validateattributes(x,{'numeric'},{'2d','nonempty'});
N = size(x,1);
d = size(x,2);
validateattributes(q+1,{'numeric'},{'2d','positive','size',[1,d]});
validateattributes(d,{'numeric'},{'scalar','positive','nonzero','<=', 2});
if nargin>=4
    validateattributes(mean,{'char'},{'2d'});
else
    mean='';
end
    % Check mask if provided
if nargin>=5
    validateattributes(Mask,{'logical'},{'2d','size',q+1});
        % reshape to line format
    Mask = reshape(Mask,[1,prod(q+1)]);
else
    Mask = true(1,prod(q+1));
end
if nargin>=3
    validateattributes(p,{'numeric'},{'2d','>=',0,'size',[1,d]});
else
    p=[0,0]; % No derivation by default
end
L = zeros(N,sum(Mask));
L_tmp = zeros(prod(q+1)); % for the 2D case only

    for k=1:N % for each value of x_k
        X = SuccessivePower(x(k,1),q(1),p(1),mean);
        if d==1
            X=X(Mask);
            L(k,:)=X';
        elseif d==2
            Y = SuccessivePower(x(k,2),q(2),p(2),mean);
            L_tmp=kron(X,Y');
            L_tmp =reshape(L_tmp,[1,prod(q+1)]); 
            % Apply mask to retain only the desired polynomial function in 
            %the polynomial basis.
            L(k,:)=L_tmp(Mask);
        end
    end
    
    if strcmp(mean,'mean')
        L = L(1,:); % all lines are the same (mean values)
    end
end

function V = SuccessivePower(x,n,p,mean)
%SuccessivePower Returns the first n-sucessive powers of the scalar <x> in
%a column vector <V>.
% Inputs:
%   x (1x1)
%   n (1x1) max degree (>=0)
%   p (1x1) order of derivation
%   mean (string) (optional) mean value over [-1,1]
% Outputs:
%   V ((n+1)x1) [d^p/dx^p x^0; d^p/dx^p x^1; ...; d^p/dx^p x^n]
%           Or, iff mean=='mean', V contains the mean values:
%           [<d^p/dx^p x^0>; <d^p/dx^p x^1>; ...; <d^p/dx^p x^n>]

    validateattributes(x,{'numeric'},{'scalar'});
    validateattributes(n,{'numeric'},{'scalar','>=',0});
    validateattributes(p,{'numeric'},{'scalar','>=',0});
    
    a = -1; b = 1; % [a,b] cell
    V = zeros(n+1,1);
    mean = strcmp(mean,'mean');
    
    for i=1:(n+1)
            if(p<=(i-1))
                V(i) = factorial(i-1)/factorial(i-1-p);
                if mean
                    V(i) = (V(i)/(i-p))* (b^(i-p)-a^(i-p)); % int value
                    V(i) = V(i)/(b-a); % mean value
                else
                    V(i) = V(i) * x^(i-1-p);
                end
            else
                V(i)=0;
            end
    end
end