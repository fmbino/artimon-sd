function [Fx, Fy] = Burgers2D_Flux(Q)
%Burgers2D_Flux Returns the flux vectors for the 2D Burgers' equation.
% Vectorized : deals with N values of the state vector <Q>.
%
% Inputs:
%   Q   N values of the state vector (Nx2) [u1, v1; u2, v2; ...; uN, vN]
% Outputs:
%   Fx  N values of the horizontal flux vector (Nx2) [u^2/2, 0]
%   Fz  N values of the vertical flux vector (Nx2) [0, v^2/2]

Fx = zeros(size(Q,1),2);
Fy = zeros(size(Q,1),2);

Fx(:,1) = (1/2)*Q(:,1).^2;
Fy(:,2) = (1/2)*Q(:,2).^2;

end

