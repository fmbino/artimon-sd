function [Fx, Fy] = Euler2D_Flux(Q, gamma)
%Euler2D_Flux Returns the flux vectors for the 2D Euler's equations.
% Vectorized : deals with N values of the state vector <Q>.
%
% Inputs:
%   Q   N values of the state vector (Nx4) [rho, rho*u, rho*v, rho*E; ...]
% Outputs:
%   Fx  N values of the horizontal flux vector (Nx4)
% [rho*u, rho*u^2+p, rho*u*v, rho*u*H]
%   Fz  N values of the vertical flux vector (Nx4)
% [rho*v, rho*u*v, rho*v^2+p, rho*u*H]

Fx = zeros(size(Q,1),4);
Fy = zeros(size(Q,1),4);
p = zeros(size(Q,1),1);
rhoH = zeros(size(Q,1),1);

    % rho*u & rho*v
Fx(:,1) = Q(:,2);
Fy(:,1) = Q(:,3);

    % rho*u^2 + p & rho*v^2 + p 
p = Euler2D_GetPressure(Q,gamma);
Fx(:,2) = (Q(:,2).^2)./Q(:,1)+p;
Fy(:,3) = (Q(:,3).^2)./Q(:,1)+p;
    
    % rho*u*v*
Fy(:,2) = (Q(:,2).*Q(:,3))./(Q(:,1));
Fx(:,3) = Fy(:,2); 

    % rho*H*[u,v]
rhoH = Euler2D_GetRhoH(Q,gamma);
Fx(:,4) = (rhoH./Q(:,1)).*Q(:,2);
Fy(:,4) = (rhoH./Q(:,1)).*Q(:,3);

end

