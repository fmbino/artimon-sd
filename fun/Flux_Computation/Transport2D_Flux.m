function [Fx, Fy] = Transport2D_Flux(Q, V)
%Transport2D_Flux Returns the flux vectors for the 2D Transport equation.
% Vectorized : deals with N values of the state vector <Q>.
%
% Inputs:
%   Q   N values of the state vector (Nx2) [u1, v1; u2, v2; ...; uN, vN]
%   V   Constant velocity field (2x1) [Vx, Vy]
% Outputs:
%   Fx  N values of the horizontal flux vector (Nx2) [u^2/2, 0]
%   Fz  N values of the vertical flux vector (Nx2) [0, v^2/2]

Fx = zeros(size(Q,1),2);
Fy = zeros(size(Q,1),2);

Fx(:,1) = V(1)*Q(:,1);
Fy(:,2) = V(2)*Q(:,2);

end

