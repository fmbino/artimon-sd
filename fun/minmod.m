function m = minmod( a1,a2,a3 )
%minmod minmod function
% Inputs:
%   a1 (Nx1)
%   a2 (Nx1)
%   a3 (Nx1)
% Output:
%   m (Nx1)

%     validateattributes(a1,{'numeric'},{'2d','column',1});
%     validateattributes(a2,{'numeric'},{'2d','column',1});
%     validateattributes(a3,{'numeric'},{'2d','column',1});
%     if(~(size(a1,1)==size(a2,1)==size(a3,1)))
%         error('Dimension not consistent.');
%     end
    m = sign(a1).*(min(abs([a1,a2,a3])')').*((sign(a1).*(sign(a1)==sign(a2)))==sign(a3));

end

