function p = Euler2D_GetPressure( Q,gamma )
%Euler2D_GetPressure Returns the pressure, given a state vector <Q>.
%Assumptions: calorically perfect gas.
% Vectorized : deals with N values of the state vector <Q>.
%
% Inputs:
%   Q   N values of the state vector (Nx4) [rho, rho*u, rho*v, rho*E; ...]
%   gamma
% Outputs:
%   p  N values of the pressure (Nx1)

    % (gamma-1)*( rho*E  - 1/2 * rho * (u^2+v^2)
p = (gamma-1)*(Q(:,4) - (1/2)*((Q(:,2)).^2+(Q(:,3)).^2)./Q(:,1));

end

