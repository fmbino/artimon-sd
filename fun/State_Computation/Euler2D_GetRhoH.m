function rhoH = Euler2D_GetRhoH(Q,gamma)
%Euler2D_GetRhoH Returns the product density*(total enthalpy), given a 
%state vector <Q>.
% Assumptions: calorically perfect gas.
% Vectorized : deals with N values of the state vector <Q>.
%
% Inputs:
%   Q   N values of the state vector (Nx4) [rho, rho*u, rho*v, rho*E; ...]
%   gamma
% Outputs:
%   rhoH  N values of the product density*(total enthalpy) (Nx1)


rhoH = Q(:,4) + Euler2D_GetPressure(Q,gamma);

end

