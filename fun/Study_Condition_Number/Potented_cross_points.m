function [ P ] = Potented_cross_points( alpha,beta,x0,x1,y0,y1 )

% Gives the coordinates of points in a shape of a potented gross
%alphaand beta  are the parameters of the distribution
%alpha=0 -> four points at the center of the cell
%alpha=1 -> one point at each midle of the edges of the cell


xm=(x0+x1)/2;

l=(x1-x0)/2;


P=[xm+beta*l,xm+alpha*l;
   xm+alpha*l,xm+beta*l;
   xm-beta*l,xm-alpha*l;
   xm-alpha*l,xm-beta*l;
   xm+beta*l,xm-alpha*l;
   xm+alpha*l,xm-beta*l;
   xm-beta*l,xm+alpha*l;
   xm-alpha*l,xm+beta*l];


end