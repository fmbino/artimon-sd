function [ P ] = Center_points( x0,x1,y0,y1 )

% Gives the coordinates of a point at the center of the cell


xm=(x0+x1)/2;


P=[xm,xm];


end



