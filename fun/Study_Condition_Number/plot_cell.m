nbpts=13;
config_sol=5;
config_flux=5;
no=1
param=[ 1 0.5];


order=1;


s=[0,1,0,1];
s=[-1,1,-1,1];

mask_Pf = Mask_polynom(nbpts,no);
mask_Ps = Mask_polynom(nbpts,no);

%xf = sol_point_distribution(nbpts,config_sol,param,s);
%xs = [0,0];

xs = flux_point_distribution(nbpts,config_sol,param,s);
xf = flux_point_distribution(nbpts,config_sol,param,s);




SDCell = SDCell_2D_Free(xs,xf,order,mask_Ps,mask_Pf);

SDCell.Display