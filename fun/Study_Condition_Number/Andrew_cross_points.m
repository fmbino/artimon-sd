function [ P ] = Andrew_cross_points( alpha,x0,x1,y0,y1 )

% Gives the coordinates of points in a shape of a St Andrew gross
%alpha is the parameter of the distribution
%alpha=0 -> four points at the center of the cell
%alpha=1 -> one point at each corner of the cell


xm=(x0+x1)/2;

l=(x1-x0)/2;


P=[xm+alpha*l,xm+alpha*l;
   xm-alpha*l,xm-alpha*l;
   xm+alpha*l,xm-alpha*l;
   xm-alpha*l,xm+alpha*l];


end

