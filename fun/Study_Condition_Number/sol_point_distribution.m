function [ M ] = sol_point_distribution( pt,no,param,s )
% Return the coordinates of flux points

% pt is the number of points and therefore of elements in the basis
%no is the number of the chosen configuration

%M=zeros(pt,pt);

if pt==1
    
    M=[Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
    
    %%%
    % 4 pts
    %%%
elseif pt==4
    
    if no==1
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==2
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    end
    
    %%%
    % 5 pts
    %%%
    
elseif pt==5
    
    if no==1
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==2
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
        
    end
    
    %%%
    % 8 pts
    %%%
    
elseif pt==8
    
    if no==1
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==2
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==3
        
        M=[Potented_cross_points(param(1,1),param(1,2),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==4
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4))];
    end
    
    %%%
    % 9 pts
    %%%
    
elseif pt==9
    
    if no==1
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==2
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==3
        
        M=[Potented_cross_points(param(1,1),param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==4
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
    end
    
    %%%
    % 12 pts
    %%%
    
elseif pt==12
    
    if no==1
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==2
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==3
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==4
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==5
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,2),param(1,3),s(1,1),s(1,2),s(1,3),s(1,4))];
        
        
    elseif no==6
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,2),param(1,3),s(1,1),s(1,2),s(1,3),s(1,4))];
        
        
    end
    
    %%%
    % 13 pts
    %%%
    
elseif pt==13
    
    if no==1
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==2
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==3
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==4
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==5
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,2),param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
        
    elseif no==6
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,2),param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    end
    
    %%%
    % 16 pts
    %%%
    
elseif pt==16
    
    if no==1
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==2
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==3
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==4
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==5
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4))];
        
        
    elseif no==6
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,3),param(1,4),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==7
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,3),param(1,4),s(1,1),s(1,2),s(1,3),s(1,4))];
        
        
    elseif no==8
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,3),param(1,4),s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==9
        
        M=[Potented_cross_points(param(1,1),param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,3),param(1,4),s(1,1),s(1,2),s(1,3),s(1,4))];
        
        
        
        
    end
    
    %%%
    % 17 pts
    %%%
    
elseif pt==17
    
    
    if no==1
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==2
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==3
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==4
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==5
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,3),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,4),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
        
    elseif no==6
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Greek_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,3),param(1,4),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==7
        
        M=[Greek_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,3),param(1,4),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
        
    elseif no==8
        
        M=[Andrew_cross_points(param(1,1),s(1,1),s(1,2),s(1,3),s(1,4));...
            Andrew_cross_points(param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,3),param(1,4),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    elseif no==9
        
        M=[Potented_cross_points(param(1,1),param(1,2),s(1,1),s(1,2),s(1,3),s(1,4));...
            Potented_cross_points(param(1,3),param(1,4),s(1,1),s(1,2),s(1,3),s(1,4));...
            Center_points(s(1,1),s(1,2),s(1,3),s(1,4))];
        
    end
    
end

end

