function [ b ] = nb_basis( pts )
%For a given number of points pts,
%return the number of polynomial basis b

if pts==4
    
    b=1;
    
elseif pts==5
    
    b=2;
    
elseif pts==8
    
    b=3;
    
elseif pts==9
    
    b=4;
    
elseif pts==12
    
    b=4;
    
elseif pts==13
    
    b=3;
    
elseif pts==16
    
    b=1;
    
elseif pts==17
    
    b=4;
    
elseif pts==20
    
    b=12;
    
elseif pts==21
    
    b=1;
    
end

end

