function DuplicateFound = checkDuplicateLines( x )
%checkDuplicate Check for duplicate lines in input vector <x>.
% Input:
%   x (?x?)
% Output:
%   DuplicateFound (1x1,boolean)

validateattributes(x,{'numeric'},{'2d'});
N = size(x,1);
P = size(x,2);
DuplicateFound = false;

i = 1;
while(~DuplicateFound && i<N)
    for j=(i+1):N
        DuplicateFound = DuplicateFound || (sum(x(i,:)==x(j,:))==P) ;
    end
    i=i+1;
end

