function m = minmod_mod( a1,a2,a3,M)
%minmod_mod minmod function, with a min condition on a1.
% Inputs:
%   a1 (Nx1)
%   a2 (Nx1)
%   a3 (Nx1)
%   M (1x1)
% Output:
%   m (Nx1)

%     validateattributes(a1,{'numeric'},{'2d','column',1});
%     validateattributes(a2,{'numeric'},{'2d','column',1});
%     validateattributes(a3,{'numeric'},{'2d','column',1});
%     if(~(size(a1,1)==size(a2,1)==size(a3,1)))
%         error('Dimension not consistent.');
%     end
    if(abs(a1)<=M)
        m=a1;
    else
        m=minmod(a1,a2,a3);
    end

end
